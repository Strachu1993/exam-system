package com.exam.cron.start_exam;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Component
class StartExamCron {

    private final StartExamService startExamService;

    @Scheduled(cron="${app.cron.start.exam.value}")
    void checkCreatedExams() {
        startExamService.startSelectedExams();
    }
}
