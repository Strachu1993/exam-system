package com.exam.cron.start_exam;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode(exclude = "studentQuestion")
@ToString(exclude = "studentQuestion")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_answer")
class StudentAnswerEntity {

    @Id
    @GeneratedValue
    private UUID id;

    @JoinColumn(name = "fk_answer")
    @ManyToOne
    private AnswerEntity answer;

    @JoinColumn(name = "fk_student_question")
    @ManyToOne
    private StudentQuestionEntity studentQuestion;

    @Builder
    StudentAnswerEntity(final AnswerEntity answer, final StudentQuestionEntity studentQuestion) {
        this.answer = answer;
        this.studentQuestion = studentQuestion;
    }
}
