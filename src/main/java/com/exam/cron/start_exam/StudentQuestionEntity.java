package com.exam.cron.start_exam;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = {"studentAnswers", "studentScore"})
@ToString(exclude = {"studentAnswers", "studentScore"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_question")
class StudentQuestionEntity {

    @Id
    @GeneratedValue
    private UUID id;

    @JoinColumn(name = "fk_question")
    @ManyToOne
    private QuestionEntity question;

    @JoinColumn(name = "fk_student_score")
    @ManyToOne
    private StudentScoreEntity studentScore;

    @JoinColumn(name = "fk_student_question")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<StudentAnswerEntity> studentAnswers;

    @Builder
    StudentQuestionEntity(final QuestionEntity question, final StudentScoreEntity studentScore) {
        this.question = question;
        this.studentScore = studentScore;
        studentAnswers = new ArrayList<>();
    }

    void addStudentAnswer(final StudentAnswerEntity studentAnswer) {
        studentAnswers.add(studentAnswer);
    }
}


