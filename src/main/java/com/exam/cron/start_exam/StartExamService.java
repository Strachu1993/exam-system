package com.exam.cron.start_exam;

import com.exam.commons.enums.ExamStatus;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StartExamService {

    private final ExamRepository examRepository;

    @Transactional
    public void startSelectedExams() {
        final List<ExamEntity> exams = examRepository.findByStatus(ExamStatus.CREATED);
        for (final ExamEntity exam: exams) {
            if (exam.isShouldBeStart()) {
                exam.markAsInProgress();
                final List<QuestionEntity> questions = exam.getQuestions();
                final List<StudentScoreEntity> studentScores = exam.getStudentScores();

                for (StudentScoreEntity studentScore: studentScores) {
                    studentScore.markAsInProgress();
                    prepareStudentQuestions(questions, studentScore);
                }

                log.info("Exam '{}' has started.", exam.getId());
            }
        }
    }

    private void prepareStudentQuestions(final List<QuestionEntity> selectedQuestions, final StudentScoreEntity studentScore) {
        for (final QuestionEntity question: selectedQuestions) {
            final StudentQuestionEntity studentQuestion = createStudentQuestion(studentScore, question);
            studentScore.addStudentQuestion(studentQuestion);
            prepareStudentAnswers(studentQuestion);
        }
    }

    private void prepareStudentAnswers(final StudentQuestionEntity studentQuestion) {
        final List<AnswerEntity> answers = studentQuestion.getQuestion().getAnswers();
        for (final AnswerEntity answer: answers) {
            final StudentAnswerEntity studentAnswer = createStudentAnswer(studentQuestion, answer);
            studentQuestion.addStudentAnswer(studentAnswer);
        }
    }

    private StudentAnswerEntity createStudentAnswer(final StudentQuestionEntity studentQuestion, final AnswerEntity answer) {
        return StudentAnswerEntity.builder()
                .answer(answer)
                .studentQuestion(studentQuestion)
                .build();
    }

    private StudentQuestionEntity createStudentQuestion(final StudentScoreEntity studentScore, final QuestionEntity question) {
        return StudentQuestionEntity.builder()
                .question(question)
                .studentScore(studentScore)
                .build();
    }
}
