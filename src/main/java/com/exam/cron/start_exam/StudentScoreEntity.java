package com.exam.cron.start_exam;

import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "studentQuestions")
@ToString(exclude = "studentQuestions")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    @Setter(value = AccessLevel.NONE)
    private UUID id;

    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_exam")
    @ManyToOne
    private ExamEntity exam;

    @JoinColumn(name = "fk_student_score")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<StudentQuestionEntity> studentQuestions;

    void addStudentQuestion(final StudentQuestionEntity StudentQuestion) {
        studentQuestions.add(StudentQuestion);
    }

    void markAsInProgress() {
        status = StudentScoreStatus.IN_PROGRESS;
    }
}
