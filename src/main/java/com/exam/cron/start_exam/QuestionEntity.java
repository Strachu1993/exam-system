package com.exam.cron.start_exam;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = {"exams", "answers"})
@ToString(exclude = {"exams", "answers"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "question")
class QuestionEntity {

    @Id
    private UUID id;

    @ManyToMany(mappedBy = "questions")
    private List<ExamEntity> exams;

    @JoinColumn(name = "fk_question", insertable = false, updatable = false)
    @OneToMany
    private List<AnswerEntity> answers;
}
