package com.exam.cron.start_exam;

import com.exam.commons.enums.ExamStatus;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = {"questions", "studentScores"})
@ToString(exclude = {"questions", "studentScores"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    @Setter(value = AccessLevel.NONE)
    private UUID id;

    @Column(insertable = false, updatable = false)
    private LocalDateTime start;

    @Column(insertable = false, updatable = false)
    private LocalDateTime end;

    @Enumerated(EnumType.STRING)
    private ExamStatus status;

    @JoinColumn(name = "fk_exam")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<StudentScoreEntity> studentScores;

    @ManyToMany
    @JoinTable(name = "rel_question_exam",
            joinColumns = @JoinColumn(name = "fk_exam"),
            inverseJoinColumns = @JoinColumn(name = "fk_question"))
    private List<QuestionEntity> questions;

    boolean isShouldBeStart() {
        final LocalDateTime now = LocalDateTime.now();
        return now.isAfter(start) && now.isBefore(end);
    }

    void markAsInProgress() {
        status = ExamStatus.IN_PROGRESS;
    }
}
