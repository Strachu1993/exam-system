package com.exam.cron.finish_exam;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.feature.calculate_score.FinishScoreFacade;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class FinishExamService {

    private final ExamRepository examRepository;
    private final FinishScoreFacade finishScoreFacade;

    @Transactional
    public void finishSelectedExams() {
        final List<ExamEntity> exams = examRepository.findByStatus(ExamStatus.IN_PROGRESS);
        for (final ExamEntity exam: exams) {
            if (exam.isShouldBeFinish()) {
                exam.getStudentScores().forEach(x -> finishScoreFacade.calculate(x.getId()));
                exam.markAsFinish();
                log.info("Exam '{}' has ended.", exam.getId());
            }
        }
    }
}
