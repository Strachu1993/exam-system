package com.exam.cron.finish_exam;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Component
class FinishExamCron {

    private final FinishExamService examEndService;

    @Scheduled(cron = "${app.cron.finish.exam.value}")
    void checkInProgressExams() {
        examEndService.finishSelectedExams();
    }
}
