package com.exam.cron.finish_exam;

import com.exam.commons.enums.ExamStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
interface ExamRepository extends JpaRepository<ExamEntity, UUID> {

    List<ExamEntity> findByStatus(ExamStatus status);
}
