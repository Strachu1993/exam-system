package com.exam.commons.feature.calculate_score;

import com.exam.commons.enums.Rating;
import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(of = "id")
@ToString(exclude = "studentQuestions")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    private UUID id;

    private Integer correctQuestions;

    private Integer inCorrectQuestions;

    private Integer score;

    @Enumerated(EnumType.STRING)
    private Rating rating;

    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_student_score")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<StudentQuestionEntity> studentQuestions;

    boolean statusIsInProgress() {
        return status == StudentScoreStatus.IN_PROGRESS;
    }

    void finishStudentScore(final List<RatingEntity> ratings) {
        correctQuestions = (int) studentQuestions.stream().filter(StudentQuestionEntity::isAccept).count();
        inCorrectQuestions = (int) studentQuestions.stream().filter(x -> !x.isAccept()).count();
        score = (int) calculatePercentage(correctQuestions, studentQuestions.size());
        rating = findRating(ratings, score);
        markStatusAsFinish();
    }

    StudentExamFinishResponse toStudentExamFinishResponse() {
        return StudentExamFinishResponse.builder()
                .studentScoreId(id)
                .correctQuestions(correctQuestions)
                .inCorrectQuestions(inCorrectQuestions)
                .score(score)
                .rating(rating)
                .build();
    }

    private Rating findRating(final List<RatingEntity> ratings, final int score) {
        for (final RatingEntity rating: ratings) {
            final int minScope = rating.getMinScope();
            final int maxScope = rating.getMaxScope();
            if (minScope <= score && score <= maxScope) {
                return rating.getSymbol();
            }
        }

        return null;
    }

    private double calculatePercentage(final double obtained, final double total) {
        return obtained * 100 / total;
    }

    private void markStatusAsFinish() {
        status = StudentScoreStatus.FINISH;
    }
}
