package com.exam.commons.feature.calculate_score;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode(of = "id")
@ToString(exclude = "studentQuestion")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_answer")
class StudentAnswerEntity {

    @Id
    private UUID id;

    private boolean selected;

    private boolean correct;

    @JoinColumn(name = "fk_answer")
    @ManyToOne
    private AnswerEntity answer;

    @JoinColumn(name = "fk_student_question")
    @ManyToOne
    private StudentQuestionEntity studentQuestion;

    void checkAnswer() {
        correct = selected == answer.isCorrect() && answer.isCorrect();
        answer = null;
    }
}
