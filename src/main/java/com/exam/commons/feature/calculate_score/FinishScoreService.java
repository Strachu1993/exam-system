package com.exam.commons.feature.calculate_score;

import com.exam.commons.exception.custom.not_found.StudentScoreNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class FinishScoreService implements FinishScoreFacade {

    private final StudentScoreRepository studentScoreRepository;
    private final RatingReadRepository settingsReadRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StudentExamFinishResponse calculate(final UUID studentScoreId) {
        final List<RatingEntity> ratings = settingsReadRepository.findAll();
        final StudentScoreEntity studentScore = findStudentScore(studentScoreId);
        if (studentScore.statusIsInProgress()) {
            final List<StudentQuestionEntity> studentQuestions = studentScore.getStudentQuestions();
            checkQuestion(studentQuestions);
            studentScore.finishStudentScore(ratings);
            log.info("Student score with id '{}' was calculated.", studentScoreId);
        }

        return studentScore.toStudentExamFinishResponse();
    }

    private void checkQuestion(final List<StudentQuestionEntity> studentQuestions) {
        for (final StudentQuestionEntity studentQuestion : studentQuestions) {
            final List<StudentAnswerEntity> studentAnswers = studentQuestion.getStudentAnswers();
            checkAnswers(studentAnswers);
            final long goodAnswersCount = countGoodAnswers(studentAnswers);
            final boolean accept = goodAnswersCount == studentQuestion.getQuestion().getCorrectAnswers();
            studentQuestion.checkQuestion(accept);
        }
    }

    private long countGoodAnswers(final List<StudentAnswerEntity> studentAnswers) {
        return studentAnswers.stream()
                .filter(StudentAnswerEntity::isCorrect)
                .count();
    }

    private void checkAnswers(final List<StudentAnswerEntity> studentAnswers) {
        studentAnswers.forEach(StudentAnswerEntity::checkAnswer);
    }

    private StudentScoreEntity findStudentScore(final UUID studentScoreId) {
        return studentScoreRepository.findById(studentScoreId).orElseThrow(() -> new StudentScoreNotFoundException(studentScoreId));
    }
}
