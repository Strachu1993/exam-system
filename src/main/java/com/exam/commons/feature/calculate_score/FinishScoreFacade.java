package com.exam.commons.feature.calculate_score;

import java.util.UUID;

@FunctionalInterface
public interface FinishScoreFacade {

    StudentExamFinishResponse calculate(final UUID studentScoreId);
}
