package com.exam.commons.feature.calculate_score;

import org.springframework.data.repository.Repository;

import java.util.List;

@org.springframework.stereotype.Repository
interface RatingReadRepository extends Repository<RatingEntity, String> {

    List<RatingEntity> findAll();
}
