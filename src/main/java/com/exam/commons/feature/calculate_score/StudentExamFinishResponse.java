package com.exam.commons.feature.calculate_score;

import com.exam.commons.enums.Rating;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class StudentExamFinishResponse {

    private UUID studentScoreId;

    private Integer correctQuestions;

    private Integer inCorrectQuestions;

    private Integer score;

    private Rating rating;
}
