package com.exam.commons.feature.character_generator;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class CharacterGenerator {

    @Value("#{'${account.available.characters}'.split('')}")
    private char[] availableChars;

    private final Random random;

    public String generateText() {
        final int length = random.nextInt(3)+4;
        final StringBuilder result = new StringBuilder();

        for(int i=0 ; i<length ; i++){
            final int randomIndex = random.nextInt(availableChars.length);
            result.append(availableChars[randomIndex]);
        }

        return result.toString();
    }
}
