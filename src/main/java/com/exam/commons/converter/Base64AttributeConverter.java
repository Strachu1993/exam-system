package com.exam.commons.converter;

import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import java.util.Base64;

@Component
public class Base64AttributeConverter implements AttributeConverter<String, String> {

    @Override
    public String convertToDatabaseColumn(final String attribute) {
        return Base64.getEncoder().encodeToString(attribute.getBytes());
    }

    @Override
    public String convertToEntityAttribute(final String dbData) {
            final byte[] decodedBytes = Base64.getDecoder().decode(dbData);
            return new String(decodedBytes);
    }
}