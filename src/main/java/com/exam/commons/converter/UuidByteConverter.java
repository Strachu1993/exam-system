package com.exam.commons.converter;

import lombok.experimental.UtilityClass;

import java.nio.ByteBuffer;
import java.util.UUID;

@UtilityClass
public class UuidByteConverter {

    public byte[] convertToByteArray(final UUID attribute) {
        final ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
        byteBuffer.putLong(attribute.getMostSignificantBits());
        byteBuffer.putLong(attribute.getLeastSignificantBits());

        return byteBuffer.array();
    }

    public UUID convertToUuid(final byte[] dbData) {
        final ByteBuffer byteBuffer = ByteBuffer.wrap(dbData);
        final long high = byteBuffer.getLong();
        final long low = byteBuffer.getLong();

        return new UUID(high, low);
    }
}