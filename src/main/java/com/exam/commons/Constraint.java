package com.exam.commons;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constraint {

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Login {
        public static final int MAX_LOGIN_LENGTH = 60;
        public static final int MIN_LOGIN_LENGTH = 3;
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class StudentGroup {
        public static final int MAX_NAME_LENGTH = 40;
        public static final int MIN_NAME_LENGTH = 1;
        public static final String YEAR_REGEXP = "\\d{4}";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class StudentAccount {
        public static final int MAX_NAME_LENGTH = 100;
        public static final int MIN_NAME_LENGTH = 1;
        public static final int MAX_SURNAME_LENGTH = 100;
        public static final int MIN_SURNAME_LENGTH = 1;
        public static final int ALBUM_NUMBER_LENGTH = 6;
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Subject {
        public static final int MAX_NAME_LENGTH = 100;
        public static final int MIN_NAME_LENGTH = 1;
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Question {
        public static final int MAX_QUESTION_LENGTH = 300;
        public static final int MIN_QUESTION_LENGTH = 1;
        public static final int MAX_CODE_LENGTH = 1000;
        public static final int MAX_CODE_SYNTAX_SIZE = 100;
        public static final int MAX_ANSWER_LENGTH = 300;
        public static final int MIN_ANSWER_LENGTH = 1;
        public static final int MIN_ANSWER_SIZE = 2;
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Exam {
        public static final int MAX_DESCRIPTION_LENGTH = 200;
    }
}
