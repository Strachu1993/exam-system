package com.exam.commons.exception.custom.error;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class GeneratePdfException extends MvcExceptionAbstract {

    private static final String MESSAGE = "PDF could not be generated correctly.";

    public GeneratePdfException(final Exception exception) {
        super(MESSAGE, exception);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    public String getType() {
        return "generate-pdf";
    }
}
