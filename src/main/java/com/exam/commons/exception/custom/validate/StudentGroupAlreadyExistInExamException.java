package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class StudentGroupAlreadyExistInExamException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Exam with id '%s' already have student from group with id '%s'.";

    public StudentGroupAlreadyExistInExamException(final UUID examId, final UUID groupId) {
        super(String.format(MESSAGE, examId, groupId));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "students-group-already-exist-exam";
    }
}
