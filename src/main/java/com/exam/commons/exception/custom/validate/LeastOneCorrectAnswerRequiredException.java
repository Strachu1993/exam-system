package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class LeastOneCorrectAnswerRequiredException extends MvcExceptionAbstract {

    private static final String LEAST_ONE_CORRECT_ANSWER = "At least one correct answer required.";

    public LeastOneCorrectAnswerRequiredException() {
        super(LEAST_ONE_CORRECT_ANSWER);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "least-one-correct-answer-required";
    }
}
