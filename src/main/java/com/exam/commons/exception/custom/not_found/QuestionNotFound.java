package com.exam.commons.exception.custom.not_found;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class QuestionNotFound extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Cannot found question with id '%s'.";

    public QuestionNotFound(final UUID questionId) {
        super(String.format(NOT_FOUND, questionId.toString()));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "question-not-found";
    }
}
