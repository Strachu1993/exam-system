package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class NotEnoughQuestionsException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Not enough questions for a subject '%s', you ask for '%d' but there are '%d'";

    public NotEnoughQuestionsException(final String subjectName, final int required, final int exist) {
        super(String.format(MESSAGE, subjectName, required, exist));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getType() {
        return "not-enough-questions";
    }
}
