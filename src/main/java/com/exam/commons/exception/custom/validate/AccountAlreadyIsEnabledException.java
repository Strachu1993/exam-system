package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class AccountAlreadyIsEnabledException extends MvcExceptionAbstract {

    private static final String ALREADY_IS_ENABLED = "Account with id '%s' already is enabled.";

    public AccountAlreadyIsEnabledException(final UUID accountId) {
        super(String.format(ALREADY_IS_ENABLED, accountId));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "account-is-already-enabled";
    }
}
