package com.exam.commons.exception.custom.not_found;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class StudentDetailsNotFoundException extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Cannot found student details with id '%s'.";

    public StudentDetailsNotFoundException(final UUID studentDetailsId) {
        super(String.format(NOT_FOUND, studentDetailsId.toString()));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "student-details-not-found";
    }
}
