package com.exam.commons.exception.custom.not_found;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class StudentScoreNotFoundException extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Cannot found student score with id '%s'.";

    public StudentScoreNotFoundException(final UUID studentScoreId) {
        super(String.format(NOT_FOUND, studentScoreId.toString()));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "student-score-not-found";
    }
}
