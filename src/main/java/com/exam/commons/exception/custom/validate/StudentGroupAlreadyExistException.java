package com.exam.commons.exception.custom.validate;

import com.exam.commons.enums.Subgroup;
import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class StudentGroupAlreadyExistException extends MvcExceptionAbstract {

    public StudentGroupAlreadyExistException(final String name, final String year, final Subgroup subgroup) {
        super("Student group with data 'name = " + name + "', 'year = " + year + "', 'subgroup = " + subgroup + "' already exist.");
    }

    public StudentGroupAlreadyExistException(final UUID studentGroupId, final String name, final String year, final Subgroup subgroup) {
        super("Student group with data 'id = " + studentGroupId + "', 'name = " + name + "', 'year = " + year + "', 'subgroup = " + subgroup + "' already exist.");
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getType() {
        return "student-group-already-exist";
    }
}
