package com.exam.commons.exception.custom.validate;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class ExamDeleteException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Exam with id '%s' was not removed because it is in '%s' status.";

    public ExamDeleteException(final UUID examId, final ExamStatus status) {
        super(String.format(MESSAGE, examId, status));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "exam-delete";
    }
}
