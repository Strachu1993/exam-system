package com.exam.commons.exception.custom.jpa;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class JpaBreakUniqueException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Data already exists '%s'.";

    public JpaBreakUniqueException(final Exception ex, final String duplicateValue) {
        super(String.format(MESSAGE, duplicateValue) + " | " +  ex);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getType() {
        return "unique-data-exception";
    }
}
