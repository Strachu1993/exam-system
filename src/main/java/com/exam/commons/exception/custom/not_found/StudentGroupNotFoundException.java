package com.exam.commons.exception.custom.not_found;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class StudentGroupNotFoundException extends MvcExceptionAbstract {

    public StudentGroupNotFoundException(final UUID studentGroupId) {
        super("Cannot found student group with id '" + studentGroupId.toString() + "'.");
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "student-group-not-found";
    }
}
