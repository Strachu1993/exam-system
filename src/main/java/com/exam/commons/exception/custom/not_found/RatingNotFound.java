package com.exam.commons.exception.custom.not_found;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class RatingNotFound extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Cannot found rating with id '%s'.";

    public RatingNotFound(final UUID ratingId) {
        super(String.format(NOT_FOUND, ratingId.toString()));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "rating-not-found";
    }
}
