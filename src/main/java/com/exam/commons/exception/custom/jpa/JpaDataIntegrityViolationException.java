package com.exam.commons.exception.custom.jpa;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class JpaDataIntegrityViolationException extends MvcExceptionAbstract {

    private static final String MESSAGE = "The row has references to other tables.";

    public JpaDataIntegrityViolationException(final Exception ex) {
        super(MESSAGE + " | " +  ex);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "relation-exception";
    }
}
