package com.exam.commons.exception.custom.not_found;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class ExamNotFound extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Cannot found exam with id '%s'.";

    public ExamNotFound(final UUID examId) {
        super(String.format(NOT_FOUND, examId.toString()));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "exam-not-found";
    }
}
