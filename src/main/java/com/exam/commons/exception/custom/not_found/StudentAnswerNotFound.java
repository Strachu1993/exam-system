package com.exam.commons.exception.custom.not_found;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class StudentAnswerNotFound extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Cannot found student answer";

    public StudentAnswerNotFound() {
        super(NOT_FOUND);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "student-answer-not-found";
    }
}
