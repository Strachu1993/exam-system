package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class AlbumNumberIsIncorrectException extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Album number is incorrect '%s'. Example: '124356'";

    public AlbumNumberIsIncorrectException(final String albumNumber) {
        super(String.format(NOT_FOUND, albumNumber));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "album-number-is-incorrect";
    }
}
