package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class AccessResourceDeniedException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Access resource denied.";

    public AccessResourceDeniedException() {
        super(MESSAGE);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getType() {
        return "access-resource-denied-enabled";
    }
}
