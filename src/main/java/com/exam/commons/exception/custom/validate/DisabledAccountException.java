package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class DisabledAccountException extends MvcExceptionAbstract {

    private static final String DISABLE_ACCOUNT = "Account with id '%s' is disable.";

    public DisabledAccountException(final UUID accountId) {
        super(String.format(DISABLE_ACCOUNT, accountId));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.UNAUTHORIZED;
    }

    @Override
    public String getType() {
        return "account-is-disable";
    }
}
