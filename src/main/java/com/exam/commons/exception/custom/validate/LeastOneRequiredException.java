package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class LeastOneRequiredException extends MvcExceptionAbstract {

    private static final String MESSAGE = "The last row cannot be deleted. Type: '%s', Id: '%s'.";

    public LeastOneRequiredException(final String type, final UUID referenceId) {
        super(String.format(MESSAGE, type, referenceId));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "least-one-required";
    }
}
