package com.exam.commons.exception.custom.general;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class StudentException extends MvcExceptionAbstract {

    public StudentException(final String message, final Exception ex) {
        super(message, ex);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "student-exception";
    }
}
