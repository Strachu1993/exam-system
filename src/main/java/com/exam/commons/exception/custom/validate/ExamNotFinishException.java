package com.exam.commons.exception.custom.validate;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class ExamNotFinishException extends MvcExceptionAbstract {

    private static final String MESSAGE = "The exam with ID '%s' has not finished yet. The current status is %s.";

    public ExamNotFinishException(final UUID examId, final ExamStatus status) {
        super(String.format(MESSAGE, examId, status));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "exam-not-finished-yet";
    }
}
