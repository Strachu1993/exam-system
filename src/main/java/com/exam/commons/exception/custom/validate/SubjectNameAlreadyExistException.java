package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class SubjectNameAlreadyExistException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Name '%s' already exist in subject table.";

    public SubjectNameAlreadyExistException(final String subjectName) {
        super(String.format(MESSAGE, subjectName));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "subject-name-already-exist";
    }
}
