package com.exam.commons.exception.custom.validate;

import com.exam.commons.enums.Rating;
import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class RatingScopeException extends MvcExceptionAbstract {

    private static final String WRONG_MAX_VALUE = "The largest value should have been 100, but it was '%s'.";
    private static final String WRONG_MIN_VALUE = "The smallest value should have been 0, but it was %s'.";
    private static final String WRONG_SCOPE = "The difference between '%s' max and '%s' min should be 1, but it was '%s'";

    private RatingScopeException(final String message, final int value) {
        super(String.format(message, value));
    }

    public RatingScopeException(final Rating first, final Rating seconds, final int difference) {
        super(String.format(WRONG_SCOPE, first, seconds, difference));
    }

    public static void wrongMinValue(final int minValue) {
        throw new RatingScopeException(WRONG_MIN_VALUE, minValue);
    }

    public static void wrongMaxValue(final int maxValue) {
        throw new RatingScopeException(WRONG_MAX_VALUE, maxValue);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "rating-scope-exception";
    }
}
