package com.exam.commons.exception.custom.validate;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class QuestionsFromSubjectAlreadyExistInExamException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Exam with id '%s' already have questions from subject with id '%s'.";

    public QuestionsFromSubjectAlreadyExistInExamException(final UUID examId, final String subjectName) {
        super(String.format(MESSAGE, examId, subjectName));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "question-subject-already-exist-exam";
    }
}
