package com.exam.commons.exception.custom.not_found;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class SubjectNotFoundException extends MvcExceptionAbstract {

    private static final String NOT_FOUND_ID = "Cannot found subject with id '%s'.";
    private static final String NOT_FOUND_NAME = "Cannot found subject by name '%s'.";

    public SubjectNotFoundException(final UUID subjectId) {
        super(String.format(NOT_FOUND_ID, subjectId.toString()));
    }

    public SubjectNotFoundException(final String subjectName) {
        super(String.format(NOT_FOUND_NAME, subjectName));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "subject-not-found";
    }
}
