package com.exam.commons.exception.custom.not_found;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class AccountNotFoundException extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Cannot found account with id '%s'.";
    private static final String NOT_FOUND_ROLE = "Cannot found account with id '%s'. With role '%s'.";

    public AccountNotFoundException(final UUID accountId) {
        super(String.format(NOT_FOUND, accountId.toString()));
    }

    public AccountNotFoundException(final UUID accountId, final RoleName role) {
        super(String.format(NOT_FOUND_ROLE, accountId.toString(), role));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "account-not-found";
    }
}
