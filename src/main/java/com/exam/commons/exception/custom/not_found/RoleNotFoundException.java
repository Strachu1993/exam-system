package com.exam.commons.exception.custom.not_found;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

public class RoleNotFoundException extends MvcExceptionAbstract {

    private static final String NOT_FOUND = "Cannot found role with name '%s'.";

    public RoleNotFoundException(final RoleName roleName) {
        super(String.format(NOT_FOUND, roleName));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    public String getType() {
        return "role-not-found";
    }
}
