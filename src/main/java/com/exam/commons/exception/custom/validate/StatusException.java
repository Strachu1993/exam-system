package com.exam.commons.exception.custom.validate;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.enums.StudentScoreStatus;
import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class StatusException extends MvcExceptionAbstract {

    private static final String EXAM_MESSAGE = "Exam with id '%s' should have '%s' status, but have '%s'.";
    private static final String SCORE_MESSAGE = "Student score with id '%s' should have '%s' statuses, but have '%s'.";

    public StatusException(final UUID examId, final ExamStatus required, final ExamStatus current) {
        super(String.format(EXAM_MESSAGE, examId, required, current));
    }

    public StatusException(final UUID studentScoreId, final StudentScoreStatus current, final StudentScoreStatus...required) {
        super(String.format(SCORE_MESSAGE, studentScoreId, prepareValues(required), current));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "status-exception";
    }
}
