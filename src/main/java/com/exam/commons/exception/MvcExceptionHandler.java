package com.exam.commons.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@ControllerAdvice
public class MvcExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MvcExceptionAbstract.class)
    public final ResponseEntity<Optional<Map<String, Object>>> handleMvcAbstractException(final MvcExceptionAbstract ex, final WebRequest request) {
        final String requestPath = ((ServletWebRequest) request).getRequest().getRequestURI();
        final HttpStatus httpStatus = ex.getHttpStatus();
        final String type = ex.getType();
        final String message = ex.getMessage();

        final Map<String, Object> result = new HashMap<>();
        result.put("message", message);
        result.put("http-status", httpStatus);
        result.put("exception-type", type);
        result.put("request-path", requestPath);

        return ResponseEntity.status(httpStatus).body(Optional.of(result));
    }
}
