package com.exam.commons.exception;

import org.springframework.http.HttpStatus;

import java.util.Arrays;

public abstract class MvcExceptionAbstract extends RuntimeException {

    protected MvcExceptionAbstract(final String errorMessage) {
        super(errorMessage);
    }

    protected MvcExceptionAbstract(final String errorMessage, final Exception exception) {
        super(errorMessage, exception);
    }

    public abstract HttpStatus getHttpStatus();

    public abstract String getType();

    public static String prepareValues(final Enum<?>... values) {
        final String[] strings = Arrays.stream(values).map(Enum::name).toArray(String[]::new);
        return prepareValues(strings);
    }

    public static String prepareValues(final String... values) {
        final StringBuilder resultBuilder = new StringBuilder();
        for (final String value : values) {
            resultBuilder.append(value).append(", ");
        }

        final String result = resultBuilder.toString();
        return result.substring(0, result.length() - 2);
    }
}
