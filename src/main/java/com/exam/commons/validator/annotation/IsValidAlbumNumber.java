package com.exam.commons.validator.annotation;

import com.exam.commons.validator.impl.AlbumNumberValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = AlbumNumberValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IsValidAlbumNumber {

    String message() default "Invalid student album number. Example: '230123' or '746253'";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
