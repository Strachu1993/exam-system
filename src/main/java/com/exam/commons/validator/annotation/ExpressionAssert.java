package com.exam.commons.validator.annotation;

import com.exam.commons.validator.impl.ExpressionAssertValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = ExpressionAssertValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExpressionAssert {

    String message() default "Expression must evaluate to true";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String value();

    /*
    *@ExpressionAssert.List({
    *    @ExpressionAssert(value = "6 + 6 == 12", message = "It is not 12."),
    *    @ExpressionAssert(value = "object.equals(secondObject)", message = "Objects are not equals."),
    *})
    */
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {
        ExpressionAssert[] value();
    }
}
