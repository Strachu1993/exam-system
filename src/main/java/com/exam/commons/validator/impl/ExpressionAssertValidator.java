package com.exam.commons.validator.impl;

import com.exam.commons.validator.annotation.ExpressionAssert;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ExpressionAssertValidator implements ConstraintValidator<ExpressionAssert, Object> {

    private Expression expression;

    public void initialize(final ExpressionAssert annotation) {
        final ExpressionParser parser = new SpelExpressionParser();
        expression = parser.parseExpression(annotation.value());
    }

    public boolean isValid(final Object target, final ConstraintValidatorContext context) {
        return expression.getValue(target, Boolean.class);
    }
}
