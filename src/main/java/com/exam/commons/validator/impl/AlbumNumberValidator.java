package com.exam.commons.validator.impl;

import com.exam.commons.Constraint;
import com.exam.commons.exception.custom.validate.AlbumNumberIsIncorrectException;
import com.exam.commons.validator.annotation.IsValidAlbumNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AlbumNumberValidator implements ConstraintValidator<IsValidAlbumNumber, String> {

    @Override
    public boolean isValid(final String albumNumber, final ConstraintValidatorContext context) {
        boolean isCorrect = albumNumber.matches("\\d{" + Constraint.StudentAccount.ALBUM_NUMBER_LENGTH + "}");
        if (!isCorrect) {
            throw new AlbumNumberIsIncorrectException(albumNumber);
        }

        return true;
    }
}
