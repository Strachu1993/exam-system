package com.exam.commons.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum Rating {

    DBD("bardzo dobry", "5.0"),
    DB_PLUS("dobry plus", "4.5"),
    DB("dobry", "4.0"),
    DST_PLUS("dostateczny plus", "3.5"),
    DST("dostateczny", "3.0"),
    NDST("niedostateczny", "2.0");

    private final String realName;
    private final String rate;
}
