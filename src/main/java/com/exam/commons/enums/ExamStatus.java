package com.exam.commons.enums;

public enum ExamStatus {

    CREATED, IN_PROGRESS, FINISH
}
