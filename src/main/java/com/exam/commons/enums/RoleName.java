package com.exam.commons.enums;

public enum RoleName {

	ROLE_STUDENT, ROLE_ADMIN
}
