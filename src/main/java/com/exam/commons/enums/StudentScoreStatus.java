package com.exam.commons.enums;

public enum StudentScoreStatus {

    CREATED, IN_PROGRESS, FINISH
}