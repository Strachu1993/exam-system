package com.exam.config.properties;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "http.access.control")
public class HttpProperty {

    private final String allowMethods;
    private final String allowOrigin;
    private final String allowHeaders;
    private final String maxAge;
}
