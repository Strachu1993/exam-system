package com.exam.config.properties;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "api.url.path")
public class ApiProperties {

    private final String admin;
    private final String student;
    private final String anonymous;
    private final String favicon;
}
