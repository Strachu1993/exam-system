package com.exam.config.properties;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "jwt")
public class JwtProperties {

    private final String secretPassword;
    private final String expireHours;
    private final String authHeader;
    private final String tokenType;
    private final int expirationTimeInMs;
}
