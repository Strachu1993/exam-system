package com.exam.config.h2;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UuidGenerator {

    public static UUID uuidGenerateV4() {
        return UUID.randomUUID();
    }
}