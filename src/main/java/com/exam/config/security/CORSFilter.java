package com.exam.config.security;

import com.exam.config.properties.HttpProperty;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CORSFilter implements Filter {

    private final HttpProperty httpProperty;

    @Override
    public void init(final FilterConfig fc) throws ServletException {}

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain chain) throws IOException, ServletException {
        final HttpServletResponse response = (HttpServletResponse) resp;
        final HttpServletRequest request = (HttpServletRequest) req;
        response.setHeader("Access-Control-Allow-Origin", httpProperty.getAllowOrigin());
        response.setHeader("Access-Control-Allow-Methods", httpProperty.getAllowMethods());
        response.setHeader("Access-Control-Max-Age", httpProperty.getMaxAge());
        response.setHeader("Access-Control-Allow-Headers", httpProperty.getAllowHeaders());

        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            chain.doFilter(req, resp);
        }
    }

    @Override
    public void destroy() {}
}