package com.exam.config.security;

import org.springframework.security.core.Authentication;

import java.util.HashMap;

@FunctionalInterface
public interface JwtTokenProviderFacade {

    HashMap<String, String> generateToken(final Authentication authentication);
}
