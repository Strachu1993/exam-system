package com.exam.config.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface AccountRepository extends CrudRepository<AccountEntity, UUID> {

    Optional<AccountEntity> findByLogin(final String username);
}
