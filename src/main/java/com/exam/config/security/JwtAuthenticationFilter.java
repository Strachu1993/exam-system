package com.exam.config.security;

import com.exam.config.properties.ApiProperties;
import com.exam.config.properties.JwtProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Slf4j
class JwtAuthenticationFilter extends BasicAuthenticationFilter {

    private final JwtTokenProvider tokenProvider;
    private final CustomUserDetailsService customUserDetailsService;
    private final ApiProperties apiProperties;
    private final JwtProperties jwtProperties;
    private final String jwtPrefixTokenType;

    JwtAuthenticationFilter(final AuthenticationManager authenticationManager, final JwtTokenProvider tokenProvider, final CustomUserDetailsService customUserDetailsService, final JwtProperties jwtProperties, final ApiProperties apiProperties) {
        super(authenticationManager);
        this.tokenProvider = tokenProvider;
        this.customUserDetailsService = customUserDetailsService;
        this.apiProperties = apiProperties;
        this.jwtProperties = jwtProperties;
        jwtPrefixTokenType = jwtProperties.getTokenType() + " ";
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) throws ServletException, IOException {
        final String contextPath = request.getRequestURI();
        if (contextPath.startsWith(apiProperties.getAnonymous()) || apiProperties.getFavicon().equals(contextPath)) {
            filterChain.doFilter(request, response);
            return;
        }

        final String jwt = getJwtFromRequest(request);
        consumeJwt(request, jwt);
        filterChain.doFilter(request, response);
    }

    private void consumeJwt(final HttpServletRequest request, final String jwt) {
        tokenProvider.validateToken(jwt);
        final UUID userId = tokenProvider.getUserIdFromJWT(jwt);
        final UserDetails userDetails = customUserDetailsService.loadUserById(userId);
        final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private String getJwtFromRequest(final HttpServletRequest request) {
        final String bearerToken = request.getHeader(jwtProperties.getAuthHeader());
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(jwtPrefixTokenType)) {
            return bearerToken.substring(jwtPrefixTokenType.length());
        }
        return null;
    }
}
