package com.exam.config.security;

import com.exam.commons.exception.custom.validate.JwtTokenExceptionException;
import com.exam.config.properties.JwtProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Component
class JwtTokenProvider implements JwtTokenProviderFacade {

    private final JwtProperties jwtProperties;
    private final String encodedJwtSecret;

    JwtTokenProvider(final JwtProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
        encodedJwtSecret = generateEncodedSecret();
    }

    @Override
    public HashMap<String, String> generateToken(final Authentication authentication) {
        final UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        final Date now = new Date();
        final Date expirationDate = new Date(now.getTime() + jwtProperties.getExpirationTimeInMs());

        final String roles = userPrincipal.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(", "));

        final String token = Jwts.builder()
                .setSubject(userPrincipal.getId().toString())
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, encodedJwtSecret)
                .compact();

        return getLoginResult(roles, token);
    }

    UUID getUserIdFromJWT(final String token) {
        final Claims claims = Jwts.parser()
                .setSigningKey(encodedJwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return UUID.fromString(claims.getSubject());
    }

    void validateToken(final String authToken) throws JwtTokenExceptionException {
        try {
            Jwts.parser().setSigningKey(encodedJwtSecret).parseClaimsJws(authToken);
        } catch (final SignatureException ex) {
            throw new JwtTokenExceptionException("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            throw new JwtTokenExceptionException("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            throw new JwtTokenExceptionException("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            throw new JwtTokenExceptionException("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            throw new JwtTokenExceptionException("JWT claims string is empty.");
        }
    }

    private HashMap<String, String> getLoginResult(final String roles, final String token) {
        final HashMap<String, String> jwtResponse = new HashMap<>();
        jwtResponse.put("accessToken", token);
        jwtResponse.put("tokenType", jwtProperties.getTokenType());
        jwtResponse.put("roles", roles);
        return jwtResponse;
    }

    private String generateEncodedSecret() {
        final String secretPassword = jwtProperties.getSecretPassword();
        if (StringUtils.isEmpty(secretPassword)) {
            throw new IllegalArgumentException("JWT secret cannot be null or empty.");
        }
        return Base64.getEncoder().encodeToString(secretPassword.getBytes());
    }
}
