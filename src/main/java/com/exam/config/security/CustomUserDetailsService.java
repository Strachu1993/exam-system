package com.exam.config.security;

import com.exam.commons.enums.RoleName;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class CustomUserDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) throws UsernameNotFoundException {
        final AccountEntity accountEntity = accountRepository.findByLogin(login).orElseThrow(
                () -> new UsernameNotFoundException("Account not found with login: " + login));

		final UserPrincipal userPrincipal = UserPrincipal.create(accountEntity);
		disableAccountIfStudent(accountEntity);
		return userPrincipal;
    }

    @Transactional
    public UserDetails loadUserById(final UUID id) {
        final AccountEntity accountEntity = accountRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("Account not found with id : " + id));

        return UserPrincipal.update(accountEntity);
    }

	private void disableAccountIfStudent(final AccountEntity account) {
		if (account.isEnabled()) {
			final boolean isStudent = account.getRoles().stream()
					.map(RoleEntity::getName).anyMatch(roleName -> roleName == RoleName.ROLE_STUDENT);

			if (isStudent) {
				account.disableAccount();
				accountRepository.save(account);
				log.info("Student account with id {} has been disabled.", account.getId());
			}
		}
	}
}
