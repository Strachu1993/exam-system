package com.exam.config.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class UserPrincipal implements UserDetails {

    private UUID id;
    private UUID studentDetailsId;
    private String username;
    @JsonIgnore
    private String password;
    private Collection<? extends GrantedAuthority> authorities;
    private boolean enabled;

    static UserPrincipal create(final AccountEntity account) {
        final List<GrantedAuthority> authorities = getGrantedAuthorities(account);
        return new UserPrincipal(account.getId(), account.getStudentDetailsId(), account.getLogin(), account.getPassword(), authorities, account.isEnabled());
    }

    static UserPrincipal update(final AccountEntity account) {
        final List<GrantedAuthority> authorities = getGrantedAuthorities(account);
        return new UserPrincipal(account.getId(), account.getStudentDetailsId(), account.getLogin(), account.getPassword(), authorities, true);
    }

    private static List<GrantedAuthority> getGrantedAuthorities(AccountEntity account) {
        return account.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
