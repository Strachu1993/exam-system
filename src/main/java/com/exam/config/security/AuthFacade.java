package com.exam.config.security;

import com.exam.commons.exception.custom.validate.AccessResourceDeniedException;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.UUID;

@UtilityClass
public class AuthFacade {

    public UUID getId() {
        return getPrincipal().getId();
    }

    public void checkStudentAccessToResource(final UUID studentDetailsId) {
        final UUID studentAuthId = getStudentAuthId();
        if (!studentAuthId.equals(studentDetailsId)) {
            throw new AccessResourceDeniedException();
        }
    }

    public UUID getStudentAuthId() {
        return getPrincipal().getStudentDetailsId();
    }

    private UserPrincipal getPrincipal() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (UserPrincipal) authentication.getPrincipal();
    }
}
