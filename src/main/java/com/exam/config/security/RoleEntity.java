package com.exam.config.security;

import com.exam.commons.enums.RoleName;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode
@ToString
@Getter(value = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "role")
class RoleEntity {

    @Id
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(insertable = false, updatable = false)
    private RoleName name;
}
