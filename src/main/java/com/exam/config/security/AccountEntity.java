package com.exam.config.security;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "account")
class AccountEntity {

	@Id
    @Setter(value = AccessLevel.NONE)
    private UUID id;

    @Column(unique = true, insertable = false, updatable = false)
    private String login;

    @Column(insertable = false, updatable = false)
    private String password;

    @Column
    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "rel_role_account",
            joinColumns = @JoinColumn(name = "fk_account"),
            inverseJoinColumns = @JoinColumn(name = "fk_role"))
    private Set<RoleEntity> roles = new HashSet<>();

    @OneToOne(mappedBy = "account")
    private StudentDetailsEntity studentDetails;

    void disableAccount() {
        enabled = false;
    }

    UUID getStudentDetailsId() {
        if (studentDetails != null) {
            return studentDetails.getId();
        }

        return null;
    }
}
