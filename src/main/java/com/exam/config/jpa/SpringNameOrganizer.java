package com.exam.config.jpa;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Slf4j
@UtilityClass
public final class SpringNameOrganizer {

    private final static String MAIN_PACKAGE = "com.exam";

    public void modifyJpaObjects() {
        final InterfaceScanningComponentProvider interfaceProvider = new InterfaceScanningComponentProvider();
        final ClassPathScanningCandidateComponentProvider classProvider = new ClassPathScanningCandidateComponentProvider(false);

        final List<Class<?>> entityClasses = findComponentsWithAnnotation(Entity.class, classProvider);
        final List<Class<?>> repositoryClasses = findComponentsWithAnnotation(Repository.class, interfaceProvider);

        changeEntitiesObjectParameters(entityClasses);
        changeRepositoryObjectParameters(repositoryClasses);
    }

    private void changeRepositoryObjectParameters(final List<Class<?>> repositoryClasses) {
        for (int i = 0; i < repositoryClasses.size(); i++) {
            final Class<?> clazz = repositoryClasses.get(i);
            final Repository repositoryAnnotation = clazz.getAnnotation(Repository.class);
            changeAnnotationValue(repositoryAnnotation, "value", clazz.getCanonicalName());
        }
    }

    private void changeEntitiesObjectParameters(final List<Class<?>> entityClasses) {
        for (int i = 0; i < entityClasses.size(); i++) {
            final Class<?> clazz = entityClasses.get(i);
            final Entity entityAnnotation = clazz.getAnnotation(Entity.class);
            changeAnnotationValue(entityAnnotation, "name", clazz.getCanonicalName());
        }
    }

    @SuppressWarnings("unchecked")
    private void changeAnnotationValue(final Annotation annotation, final String key, final Object newValue) {
        final Object handler = Proxy.getInvocationHandler(annotation);
        Field field;
        try {
            field = handler.getClass().getDeclaredField("memberValues");
        } catch (NoSuchFieldException | SecurityException e) {
            throw new IllegalStateException(e);
        }
        field.setAccessible(true);
        final Map<String, Object> memberValues;
        try {
            memberValues = (Map<String, Object>) field.get(handler);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        final Object oldValue = memberValues.get(key);
        if (oldValue == null || oldValue.getClass() != newValue.getClass()) {
            throw new IllegalArgumentException();
        }
        memberValues.put(key, newValue);
    }

    private List<Class<?>> findComponentsWithAnnotation(final Class<? extends Annotation> annotation, final ClassPathScanningCandidateComponentProvider provider) {
        final List<Class<?>> result = new LinkedList<>();
        provider.addIncludeFilter(new AnnotationTypeFilter(annotation));

        for (final BeanDefinition beanDefinition : provider.findCandidateComponents(MAIN_PACKAGE)) {
            try {
                result.add(Class.forName(beanDefinition.getBeanClassName()));
            } catch (ClassNotFoundException e) {
                log.error("Error during scanning custom JPA classes");
                System.exit(-1);
            }
        }
        return result;
    }

    private static class InterfaceScanningComponentProvider extends ClassPathScanningCandidateComponentProvider {

        InterfaceScanningComponentProvider() {
            super(false);
        }

        @Override
        protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
            return super.isCandidateComponent(beanDefinition) || beanDefinition.getMetadata().isInterface();
        }
    }
}
