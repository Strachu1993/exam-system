package com.exam;

import com.exam.config.jpa.SpringNameOrganizer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
class EXaminatorApplication {

    public static void main(String[] args) {
        SpringNameOrganizer.modifyJpaObjects();
        SpringApplication.run(EXaminatorApplication.class, args);
    }
}