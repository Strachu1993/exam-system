package com.exam.feature.noAuth.login;

import com.exam.commons.Constraint;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class LoginRequest {

    @Size(min = Constraint.Login.MIN_LOGIN_LENGTH, max = Constraint.Login.MAX_LOGIN_LENGTH)
    private String login;

    @NotBlank
    private String password;
}
