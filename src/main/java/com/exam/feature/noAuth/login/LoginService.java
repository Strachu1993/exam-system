package com.exam.feature.noAuth.login;

import com.exam.config.security.JwtTokenProviderFacade;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class LoginService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProviderFacade tokenProvider;

    ResponseEntity<HashMap<String, String>> login(final LoginRequest loginRequest) {
        final String login = loginRequest.getLogin();
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login, loginRequest.getPassword()));
        final ResponseEntity<HashMap<String, String>> loginData = ResponseEntity.ok(tokenProvider.generateToken(authentication));
        log.info("An account named '{}' has been logged in.", login);
        return loginData;
    }
}
