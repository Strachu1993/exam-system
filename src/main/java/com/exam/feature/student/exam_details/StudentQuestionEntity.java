package com.exam.feature.student.exam_details;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(of = "id")
@ToString(exclude = "studentAnswers")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_question")
class StudentQuestionEntity {

    @Id
    private UUID id;

    @JoinColumn(name = "fk_question")
    @ManyToOne
    private QuestionEntity question;

    @JoinColumn(name = "fk_student_score")
    @ManyToOne
    private StudentScoreEntity studentScore;

    @JoinColumn(name = "fk_student_question")
    @OneToMany
    private List<StudentAnswerEntity> studentAnswers;
}


