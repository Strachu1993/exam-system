package com.exam.feature.student.exam_details;

import com.exam.commons.exception.custom.not_found.StudentScoreNotFoundException;
import com.exam.config.security.AuthFacade;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentExamDetailsService {

    private final StudentScoreReadRepository studentScoreRepository;

    StudentExamDetailsResponse getStudentExamDetails(final UUID studentScoreId) {
        final StudentScoreEntity studentScore = findStudentScore(studentScoreId);
        final UUID studentDetailsId = studentScore.getStudentDetails().getId();
        AuthFacade.checkStudentAccessToResource(studentDetailsId);
        studentScore.checkStudentScoreStatus();
        final List<StudentQuestionEntity> studentQuestions = studentScore.getStudentQuestions();
        final List<StudentExamDetailsResponse.StudentQuestionResponse> studentQuestionResponses = prepareStudentQuestionDetails(studentQuestions);

        return prepareStudentExamDetails(studentScore.getExam(), studentScoreId, studentQuestionResponses);
    }

    private StudentExamDetailsResponse prepareStudentExamDetails(final ExamEntity exam, final UUID studentScoreId, final List<StudentExamDetailsResponse.StudentQuestionResponse> studentQuestionResponses) {
        return StudentExamDetailsResponse.builder()
                .start(exam.getStart())
                .end(exam.getEnd())
                .studentScoreId(studentScoreId)
                .studentQuestionResponses(studentQuestionResponses)
                .build();
    }

    private List<StudentExamDetailsResponse.StudentQuestionResponse> prepareStudentQuestionDetails(final List<StudentQuestionEntity> studentQuestions) {
        final List<StudentExamDetailsResponse.StudentQuestionResponse> result = new ArrayList<>();
        for(StudentQuestionEntity studentQuestion: studentQuestions) {
            final List<StudentAnswerEntity> studentAnswers = studentQuestion.getStudentAnswers();
            final List<StudentExamDetailsResponse.StudentAnswerResponse> studentAnswerResponses = prepareStudentAnswersDetails(studentAnswers);
            result.add(StudentExamDetailsResponse.StudentQuestionResponse.of(studentQuestion, studentAnswerResponses));
        }

        return result.stream().sorted(Comparator.comparing(StudentExamDetailsResponse.StudentQuestionResponse::hashCode)).collect(Collectors.toList());
    }

    private List<StudentExamDetailsResponse.StudentAnswerResponse> prepareStudentAnswersDetails(final List<StudentAnswerEntity> studentAnswers) {
        final List<StudentExamDetailsResponse.StudentAnswerResponse> result = new ArrayList<>();
        for(StudentAnswerEntity studentAnswer: studentAnswers) {
            result.add(StudentExamDetailsResponse.StudentAnswerResponse.of(studentAnswer));
        }

        return result.stream().sorted(Comparator.comparing(StudentExamDetailsResponse.StudentAnswerResponse::hashCode)).collect(Collectors.toList());
    }

    private StudentScoreEntity findStudentScore(UUID studentScoreId) {
        return studentScoreRepository.findById(studentScoreId).orElseThrow(() -> new StudentScoreNotFoundException(studentScoreId));
    }
}
