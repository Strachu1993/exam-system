package com.exam.feature.student.exam_details;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Getter(AccessLevel.PACKAGE)
@EqualsAndHashCode
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "answer")
class AnswerEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String answer;
}
