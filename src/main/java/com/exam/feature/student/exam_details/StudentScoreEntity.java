package com.exam.feature.student.exam_details;

import com.exam.commons.enums.StudentScoreStatus;
import com.exam.commons.exception.custom.validate.StatusException;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(of = "id")
@ToString(exclude = {"studentDetails", "studentQuestions"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_student_details")
    @ManyToOne
    private StudentDetailsEntity studentDetails;

    @JoinColumn(name = "fk_exam")
    @ManyToOne
    private ExamEntity exam;

    @JoinColumn(name = "fk_student_score")
    @OneToMany
    private List<StudentQuestionEntity> studentQuestions;

    void checkStudentScoreStatus() {
        if (status != StudentScoreStatus.IN_PROGRESS) {
            throw new StatusException(id, status, StudentScoreStatus.IN_PROGRESS);
        }
    }
}
