package com.exam.feature.student.exam_details;

import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface StudentScoreReadRepository extends Repository<StudentScoreEntity, UUID> {

    Optional<StudentScoreEntity> findById(UUID id);
}
