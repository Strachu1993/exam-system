package com.exam.feature.student.exam_details;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class StudentExamDetailsResponse {

    private LocalDateTime start;
    private LocalDateTime end;
    private UUID studentScoreId;
    private List<StudentQuestionResponse> studentQuestionResponses;

    @Builder
    @Getter
    @EqualsAndHashCode(of = "studentQuestionId")
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class StudentQuestionResponse {
        private UUID studentQuestionId;
        private String question;
        private String code;
        private String codeSyntax;
        private short correctAnswers;
        private byte[] image;
        private List<StudentAnswerResponse> answers;

        static StudentQuestionResponse of(final StudentQuestionEntity studentQuestion, final List<StudentExamDetailsResponse.StudentAnswerResponse> studentAnswerResponses) {
            final QuestionEntity question = studentQuestion.getQuestion();
            return StudentQuestionResponse.builder()
                    .studentQuestionId(studentQuestion.getId())
                    .question(question.getQuestion())
                    .code(question.getCode())
                    .codeSyntax(question.getCodeSyntax())
                    .correctAnswers(question.getCorrectAnswers())
                    .image(question.getImage())
                    .answers(studentAnswerResponses)
                    .build();
        }
    }

    @Builder
    @Getter
    @EqualsAndHashCode(of = "studentAnswerId")
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class StudentAnswerResponse {
        private UUID studentAnswerId;
        private String answer;
        private boolean selected;

        static StudentAnswerResponse of(final StudentAnswerEntity studentAnswer) {
            final String answer = studentAnswer.getAnswer().getAnswer();
            return StudentAnswerResponse.builder()
                    .studentAnswerId(studentAnswer.getId())
                    .answer(answer)
                    .selected(studentAnswer.isSelected())
                    .build();
        }
    }
}
