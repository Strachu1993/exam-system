package com.exam.feature.student.exam_details;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "answers")
@ToString(exclude = "answers")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "question")
class QuestionEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String question;

    @Column(insertable = false, updatable = false)
    private String code;

    @Column(insertable = false, updatable = false)
    private String codeSyntax;

    @Column(insertable = false, updatable = false)
    private short correctAnswers;

    @Column(insertable = false, updatable = false)
    @Basic
    private byte[] image;

    @JoinColumn(name = "fk_question", insertable = false, updatable = false)
    @OneToMany
    private List<AnswerEntity> answers;
}
