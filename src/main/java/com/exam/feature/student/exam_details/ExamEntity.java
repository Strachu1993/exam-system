package com.exam.feature.student.exam_details;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.exception.custom.validate.StatusException;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(of = "id")
@ToString(exclude = "studentScores")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private LocalDateTime start;

    @Column(insertable = false, updatable = false)
    private LocalDateTime end;

    @Enumerated(EnumType.STRING)
    @Column(insertable = false, updatable = false)
    private ExamStatus status;

    @JoinColumn(name = "fk_exam")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<StudentScoreEntity> studentScores;

    void checkStatusIsInProgress() {
        if (status != ExamStatus.IN_PROGRESS) {
            throw new StatusException(id, ExamStatus.IN_PROGRESS, status);
        }
    }
}


