package com.exam.feature.student.exam_details;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.student}/exam")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentExamDetailsEndpoint {

    private final StudentExamDetailsService studentExamDetailsService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{studentScoreId}", produces = MediaType.APPLICATION_JSON_VALUE)
    StudentExamDetailsResponse getStudentExams(@PathVariable final UUID studentScoreId) {
        return studentExamDetailsService.getStudentExamDetails(studentScoreId);
    }
}
