package com.exam.feature.student.exams;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class StudentExamResponse {

    private LocalDateTime start;

    private LocalDateTime end;

    private ExamStatus examStatus;

    private String description;

    @Setter(AccessLevel.PACKAGE)
    private UUID studentScoreId;

    @Setter(AccessLevel.PACKAGE)
    private StudentScoreStatus scoreStatus;
}
