package com.exam.feature.student.exams;

import com.exam.commons.exception.custom.not_found.StudentDetailsNotFoundException;
import com.exam.config.security.AuthFacade;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentExamService {

    private final StudentDetailsReadRepository studentDetailsRepository;

    List<StudentExamResponse> getStudentExams() {
        final UUID studentDetailsId = AuthFacade.getStudentAuthId();
        final StudentDetailsEntity studentDetailsEntity = findStudentDetailsId(studentDetailsId);

        final List<StudentExamResponse> result = new ArrayList<>();
        studentDetailsEntity.getStudentScores().forEach(x -> {
            final ExamEntity exam = x.getExam();
            final StudentExamResponse studentExamResponse = exam.toStudentExamResponse();
            studentExamResponse.setStudentScoreId(x.getId());
            studentExamResponse.setScoreStatus(x.getStatus());
            result.add(studentExamResponse);
        });

        return result;
    }

    private StudentDetailsEntity findStudentDetailsId(final UUID studentDetailsId) {
        return studentDetailsRepository.findById(studentDetailsId).orElseThrow(() -> new StudentDetailsNotFoundException(studentDetailsId));
    }
}
