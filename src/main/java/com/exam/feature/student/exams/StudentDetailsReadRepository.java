package com.exam.feature.student.exams;

import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface StudentDetailsReadRepository extends Repository<StudentDetailsEntity, UUID> {

    Optional<StudentDetailsEntity> findById(UUID id);
}
