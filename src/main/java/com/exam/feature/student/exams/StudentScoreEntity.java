package com.exam.feature.student.exams;

import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode(exclude = {"studentDetails", "exam"})
@ToString(exclude = {"studentDetails", "exam"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    private UUID id;

    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_exam")
    @ManyToOne
    private ExamEntity exam;

    @JoinColumn(name = "fk_student_details")
    @ManyToOne
    private StudentDetailsEntity studentDetails;
}
