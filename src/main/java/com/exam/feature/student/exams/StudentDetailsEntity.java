package com.exam.feature.student.exams;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "studentScores")
@ToString(exclude = "studentScores")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_details")
class StudentDetailsEntity {

    @Id
    private UUID id;

    @OneToMany(mappedBy = "studentDetails")
    private List<StudentScoreEntity> studentScores;
}
