package com.exam.feature.student.exams;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "${api.url.path.student}/exams")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentExamEndpoint {

    private final StudentExamService studentExamService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<StudentExamResponse> getStudentExams() {
        return studentExamService.getStudentExams();
    }
}
