package com.exam.feature.student.exam_vote;

import com.exam.commons.exception.custom.not_found.StudentAnswerNotFound;
import com.exam.commons.exception.custom.not_found.StudentQuestionNotFound;
import com.exam.commons.exception.custom.not_found.StudentScoreNotFoundException;
import com.exam.config.security.AuthFacade;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentExamVoteService {

    private final StudentScoreRepository studentScoreRepository;

    @Transactional
    public void vote(final UUID studentScoreId, final List<StudentSelectionRequest> studentSelections) {
        final StudentScoreEntity studentScoreEntity = findStudentScoreById(studentScoreId);
        final UUID studentDetailsId = studentScoreEntity.getStudentDetails().getId();
        AuthFacade.checkStudentAccessToResource(studentDetailsId);
        studentScoreEntity.checkStudentScoreStatus();
        final List<StudentQuestionEntity> studentQuestionEntities = studentScoreEntity.getStudentQuestions();
        insertStudentVote(studentSelections, studentQuestionEntities);
    }

    private void insertStudentVote(final List<StudentSelectionRequest> studentSelections, final List<StudentQuestionEntity> studentQuestionEntities) {
        for(final StudentSelectionRequest studentSelection: studentSelections) {
            final UUID selectedStudentQuestionId = studentSelection.getStudentQuestionId();
            final StudentQuestionEntity studentQuestionEntity = studentQuestionEntities.stream()
                    .filter(x -> x.getId().equals(selectedStudentQuestionId))
                    .map(Optional::ofNullable)
                    .findFirst()
                    .flatMap(Function.identity())
                    .orElseThrow(StudentQuestionNotFound::new);

            final List<StudentSelectionRequest.Answer> selectedAnswers = studentSelection.getAnswers();
            for (StudentSelectionRequest.Answer selectedAnswer: selectedAnswers) {
                final StudentAnswerEntity studentAnswerEntity = studentQuestionEntity.getStudentAnswers().stream()
                        .filter(x -> x.getId().equals(selectedAnswer.getStudentAnswerId()))
                        .map(Optional::ofNullable)
                        .findFirst()
                        .flatMap(Function.identity())
                        .orElseThrow(StudentAnswerNotFound::new);

                studentAnswerEntity.setSelected(selectedAnswer.isChecked());
            }
        }
    }

    private StudentScoreEntity findStudentScoreById(final UUID studentScoreId) {
        return studentScoreRepository.findById(studentScoreId).orElseThrow(() -> new StudentScoreNotFoundException(studentScoreId));
    }
}
