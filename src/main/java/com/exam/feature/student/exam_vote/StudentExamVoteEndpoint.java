package com.exam.feature.student.exam_vote;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.student}/exam")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentExamVoteEndpoint {

    private final StudentExamVoteService studentExamVoteService;

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping(value = "/{studentScoreId}/vote", produces = MediaType.APPLICATION_JSON_VALUE)
    void vote(@PathVariable final UUID studentScoreId, @RequestBody final List<StudentSelectionRequest> studentSelections) {
        studentExamVoteService.vote(studentScoreId, studentSelections);
    }
}
