package com.exam.feature.student.exam_vote;

import com.exam.commons.enums.StudentScoreStatus;
import com.exam.commons.exception.custom.validate.StatusException;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "studentDetails")
@ToString(exclude = "studentDetails")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    private UUID id;

    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_student_details")
    @ManyToOne
    private StudentDetailsEntity studentDetails;

    @JoinColumn(name = "fk_student_score")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<StudentQuestionEntity> studentQuestions;

    void checkStudentScoreStatus() {
        if (status != StudentScoreStatus.IN_PROGRESS) {
            throw new StatusException(id, status, StudentScoreStatus.IN_PROGRESS);
        }
    }
}
