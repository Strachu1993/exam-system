package com.exam.feature.student.exam_vote;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_answer")
class StudentAnswerEntity {

    @Id
    private UUID id;

    @Setter(value = AccessLevel.PACKAGE)
    private Boolean selected;

    @JoinColumn(name = "fk_student_question")
    @ManyToOne
    private StudentQuestionEntity studentQuestion;
}
