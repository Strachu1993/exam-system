package com.exam.feature.student.exam_finish;

import com.exam.commons.exception.custom.not_found.StudentScoreNotFoundException;
import com.exam.commons.feature.calculate_score.FinishScoreFacade;
import com.exam.commons.feature.calculate_score.StudentExamFinishResponse;
import com.exam.config.security.AuthFacade;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentExamFinishService {

    private final StudentScoreRepository studentScoreRepository;
    private final FinishScoreFacade finishScoreFacade;

    @Transactional
    public StudentExamFinishResponse finishExam(final UUID studentScoreId) {
        final StudentScoreEntity studentScoreEntity = findStudentScoreById(studentScoreId);
        final UUID studentDetailsId = studentScoreEntity.getStudentDetails().getId();
        AuthFacade.checkStudentAccessToResource(studentDetailsId);
        studentScoreEntity.checkStudentScoreStatus();
        return finishScoreFacade.calculate(studentScoreId);
    }

    private StudentScoreEntity findStudentScoreById(final UUID studentScoreId) {
        return studentScoreRepository.findById(studentScoreId).orElseThrow(() -> new StudentScoreNotFoundException(studentScoreId));
    }
}
