package com.exam.feature.student.exam_finish;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface StudentScoreRepository extends JpaRepository<StudentScoreEntity, UUID> {

}
