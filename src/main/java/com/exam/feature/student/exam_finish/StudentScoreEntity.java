package com.exam.feature.student.exam_finish;

import com.exam.commons.enums.StudentScoreStatus;
import com.exam.commons.exception.custom.validate.StatusException;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode(of = "id")
@ToString(exclude = "studentDetails")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    private UUID id;

    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_student_details")
    @ManyToOne
    private StudentDetailsEntity studentDetails;

    void checkStudentScoreStatus() {
        if (status == StudentScoreStatus.CREATED) {
            throw new StatusException(id, status, StudentScoreStatus.IN_PROGRESS, StudentScoreStatus.FINISH);
        }
    }
}
