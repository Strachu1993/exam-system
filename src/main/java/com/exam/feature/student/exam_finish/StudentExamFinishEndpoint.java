package com.exam.feature.student.exam_finish;

import com.exam.commons.feature.calculate_score.StudentExamFinishResponse;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.student}/exam")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentExamFinishEndpoint {

    private final StudentExamFinishService studentExamFinishService;

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping(value = "/{studentScoreId}/finish", produces = MediaType.APPLICATION_JSON_VALUE)
    StudentExamFinishResponse finishExam(@PathVariable final UUID studentScoreId) {
        return studentExamFinishService.finishExam(studentScoreId);
    }
}
