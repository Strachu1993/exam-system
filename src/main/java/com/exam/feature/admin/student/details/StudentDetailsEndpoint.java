package com.exam.feature.admin.student.details;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/students")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentDetailsEndpoint {

    private final StudentDetailsService studentDetailsService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{studentAccountId}", produces = MediaType.APPLICATION_JSON_VALUE)
    StudentDetailsResponse getStudentDetails(@PathVariable final UUID studentAccountId) {
        return studentDetailsService.getStudentDetails(studentAccountId);
    }
}
