package com.exam.feature.admin.student.create;

import com.exam.commons.enums.RoleName;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface RoleReadRepository extends Repository<RoleEntity, UUID> {

    Optional<RoleEntity> findByName(final RoleName roleName);
}


