package com.exam.feature.admin.student.create;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.custom.not_found.RoleNotFoundException;
import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import com.exam.commons.feature.character_generator.CharacterGenerator;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class CreateStudentService {

    private final StudentGroupReadRepository studentGroupRepository;
    private final RoleReadRepository roleRepository;
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;
    private final CharacterGenerator characterGenerator;

    @Transactional
    public void createStudent(final CreateStudentRequest request) {
        final String newAlbumNumber = request.getAlbumNumber();
        validateAlbumNumber(newAlbumNumber);
        final String newName = request.getName();
        final String newSurname = request.getSurname();
        final UUID newGroupId = request.getGroupId();

        final String generatedLogin = characterGenerator.generateText();
        final String generatedPassword = characterGenerator.generateText();
        final String encodePassword = passwordEncoder.encode(generatedPassword);
        final Set<RoleEntity> studentRoleEntity = findStudentRole();
        final StudentGroupEntity studentGroup = findStudentGroupEntity(newGroupId);

        final StudentDetailsEntity studentDetails = createStudentDetailsEntity(newName, newSurname, generatedPassword, studentGroup, newAlbumNumber);
        final AccountEntity studentAccount = createAccountEntity(generatedLogin, encodePassword, studentRoleEntity, studentDetails);
        studentDetails.setAccount(studentAccount);

        accountRepository.save(studentAccount);
        log.info("Student account has been added.");
    }

    private void validateAlbumNumber(final String albumNumber) {
        final boolean albumNumberIsExist = accountRepository.existsByStudentDetailsAlbumNumber(albumNumber);
        if (albumNumberIsExist) {
            throw new AlbumNumberAlreadyExistsException(albumNumber);
        }
    }

    private StudentGroupEntity findStudentGroupEntity(final UUID studentGroupId) {
        return studentGroupRepository.findById(studentGroupId)
                .orElseThrow(() -> new StudentGroupNotFoundException(studentGroupId));
    }

    private Set<RoleEntity> findStudentRole() {
        final RoleName studentRole = RoleName.ROLE_STUDENT;
        final RoleEntity studentRoleEntity = roleRepository.findByName(studentRole).orElseThrow(() -> new RoleNotFoundException(studentRole));
        return Set.of(studentRoleEntity);
    }

    private static StudentDetailsEntity createStudentDetailsEntity(final String newName, final String newSurname, final String password, final StudentGroupEntity studentGroup, final String albumNumber) {
        return StudentDetailsEntity.builder()
                .name(newName)
                .surname(newSurname)
                .password(password)
                .studentGroup(studentGroup)
                .albumNumber(albumNumber)
                .build();
    }

    private static AccountEntity createAccountEntity(final String login, final String password, final Set<RoleEntity> studentEntity, final StudentDetailsEntity studentDetails) {
        return AccountEntity.builder()
                .enabled(true)
                .login(login)
                .password(password)
                .roles(studentEntity)
                .studentDetails(studentDetails)
                .build();
    }
}
