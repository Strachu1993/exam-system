package com.exam.feature.admin.student.score_details;

import com.exam.commons.enums.Rating;
import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private Integer correctQuestions;

    @Column(insertable = false, updatable = false)
    private Integer inCorrectQuestions;

    @Column(insertable = false, updatable = false)
    private Integer score;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private Rating rating;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_exam")
    @ManyToOne
    private ExamEntity exam;

    ScoreDetailsResponse toScoreDetailsResponse() {
        return ScoreDetailsResponse.builder()
                .examId(exam.getId())
                .start(exam.getStart())
                .end(exam.getEnd())
                .description(exam.getDescription())
                .examStatus(exam.getStatus())
                .scoreId(id)
                .correctQuestions(correctQuestions)
                .inCorrectQuestions(inCorrectQuestions)
                .score(score)
                .rating(rating)
                .scoreStatus(status)
                .build();
    }
}
