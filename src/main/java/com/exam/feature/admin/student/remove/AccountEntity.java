package com.exam.feature.admin.student.remove;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "account")
class AccountEntity {

    @Id
    private UUID id;

    @OneToOne(mappedBy = "account", cascade = CascadeType.REMOVE)
    private StudentDetailsEntity studentDetails;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "rel_role_account",
            joinColumns = @JoinColumn(name = "fk_account"),
            inverseJoinColumns = @JoinColumn(name = "fk_role"))
    private Set<RoleEntity> roles;

    void validateIsCanBeRemove() {
        final boolean hasStartedExam = studentDetails.getStudentScores().stream()
                .anyMatch(StudentScoreEntity::isInProgress);
        if (hasStartedExam) {
            throw new StudentHasStartedExamException(id);
        }
    }
}
