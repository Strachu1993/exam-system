package com.exam.feature.admin.student.score_details;

import com.exam.commons.exception.custom.not_found.StudentScoreNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class ScoreDetailsService {

    private final StudentScoreReadRepository studentScoreReadRepository;

    ScoreDetailsResponse getStudentScoreDetails(final UUID scoreId) {
        return findStudentScore(scoreId).toScoreDetailsResponse();
    }

    private StudentScoreEntity findStudentScore(final UUID studentScoreId) {
        return studentScoreReadRepository.findById(studentScoreId).orElseThrow(() -> new StudentScoreNotFoundException(studentScoreId));
    }
}
