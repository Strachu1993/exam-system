package com.exam.feature.admin.student.remove;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/students")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class RemoveStudentEndpoint {

    private final RemoveStudentService removeStudentService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    void removeStudent(@PathVariable("id") final UUID studentAccountId) {
        removeStudentService.removeStudent(studentAccountId);
    }
}
