package com.exam.feature.admin.student.score_details;

import com.exam.commons.enums.ExamStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(exclude = "studentScores")
@ToString(exclude = "studentScores")
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private LocalDateTime start;

    @Column(insertable = false, updatable = false)
    private LocalDateTime end;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private ExamStatus status;

    @Column(insertable = false, updatable = false)
    private String description;

    @OneToMany(mappedBy = "exam")
    private List<StudentScoreEntity> studentScores;
}
