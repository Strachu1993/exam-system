package com.exam.feature.admin.student.create;

import com.exam.commons.Constraint;
import com.exam.commons.validator.annotation.IsValidAlbumNumber;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Size;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
class CreateStudentRequest {

    @Size(min = Constraint.StudentAccount.MIN_NAME_LENGTH, max = Constraint.StudentAccount.MAX_NAME_LENGTH)
    private final String name;

    @Size(min = Constraint.StudentAccount.MIN_SURNAME_LENGTH, max = Constraint.StudentAccount.MAX_SURNAME_LENGTH)
    private final String surname;

    private final UUID groupId;

    @IsValidAlbumNumber
    private final String albumNumber;
}
