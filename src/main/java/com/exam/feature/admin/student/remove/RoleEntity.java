package com.exam.feature.admin.student.remove;

import com.exam.commons.enums.RoleName;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(exclude = "accounts")
@ToString(exclude = "accounts")
@Entity
@Table(name = "role")
class RoleEntity {

    @Id
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column
    private RoleName name;

    @ManyToMany(mappedBy = "roles")
    private Set<AccountEntity> accounts;
}
