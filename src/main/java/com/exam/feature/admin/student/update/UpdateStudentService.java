package com.exam.feature.admin.student.update;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.custom.jpa.JpaBreakUniqueException;
import com.exam.commons.exception.custom.not_found.AccountNotFoundException;
import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class UpdateStudentService {

    private final StudentGroupRepository studentGroupRepository;
    private final AccountRepository accountRepository;

    void updateStudent(final UUID studentAccountId, final UpdateStudentRequest request) {
        final String newName = request.getName();
        final String newSurname = request.getSurname();
        final String newAlbumNumber = request.getAlbumNumber();
        final StudentGroupEntity newStudentGroup = findStudentGroupEntity(request.getGroupId());

        final AccountEntity studentAccount = findAccount(studentAccountId);
        final StudentDetailsEntity studentDetails = studentAccount.getStudentDetails();
        studentDetails.setName(newName);
        studentDetails.setSurname(newSurname);
        studentDetails.setAlbumNumber(newAlbumNumber);
        studentDetails.setStudentGroup(newStudentGroup);

        saveStudentAccount(studentAccount, newAlbumNumber);
        log.info("Student account with id '{}' has been updated.", studentAccountId);
    }

    private AccountEntity findAccount(final UUID studentAccountId) {
        return accountRepository.findByIdAndRolesName(studentAccountId, RoleName.ROLE_STUDENT)
                .orElseThrow(() -> new AccountNotFoundException(studentAccountId, RoleName.ROLE_STUDENT));
    }

    private StudentGroupEntity findStudentGroupEntity(final UUID studentGroupId) {
        return studentGroupRepository.findById(studentGroupId)
                .orElseThrow(() -> new StudentGroupNotFoundException(studentGroupId));
    }

    private void saveStudentAccount(final AccountEntity studentAccount, final String albumNumber) {
        try {
            accountRepository.save(studentAccount);
        } catch (final DataIntegrityViolationException ex) {
            throw new JpaBreakUniqueException(ex, albumNumber);
        }
    }
}
