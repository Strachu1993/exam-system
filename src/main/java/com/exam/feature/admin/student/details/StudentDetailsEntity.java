package com.exam.feature.admin.student.details;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(exclude = "account")
@ToString(exclude = "account")
@Entity
@Table(name = "student_details")
class StudentDetailsEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String albumNumber;

    @Column(insertable = false, updatable = false)
    private String name;

    @Column(insertable = false, updatable = false)
    private String surname;

    @JoinColumn(name = "fk_account")
    @OneToOne
    private AccountEntity account;

    @OneToMany(mappedBy = "studentDetails", fetch = FetchType.EAGER)
    private List<StudentScoreEntity> studentScores;

    @JoinColumn(name = "fk_group")
    @ManyToOne
    private StudentGroupEntity studentGroup;
}
