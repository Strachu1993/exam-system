package com.exam.feature.admin.student.details;

import com.exam.commons.enums.Rating;
import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(exclude = {"studentDetails", "exam"})
@ToString(exclude = {"studentDetails", "exam"})
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private Integer score;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private Rating rating;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_exam")
    @ManyToOne
    private ExamEntity exam;

    @JoinColumn(name = "fk_student_details")
    @ManyToOne
    private StudentDetailsEntity studentDetails;

    StudentDetailsResponse.ExamResponse toExamResponse() {
        return StudentDetailsResponse.ExamResponse.builder()
                .scoreId(id)
                .rating(rating)
                .scoreStatus(status)
                .examId(exam.getId())
                .description(exam.getDescription())
                .start(exam.getStart())
                .end(exam.getEnd())
                .build();
    }
}
