package com.exam.feature.admin.student.remove;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.custom.general.StudentException;
import com.exam.commons.exception.custom.not_found.AccountNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class RemoveStudentService {

    private final AccountRepository accountRepository;

    @Transactional
    public void removeStudent(final UUID studentAccountId) {
        final AccountEntity studentAccount = findAccount(studentAccountId);
        studentAccount.validateIsCanBeRemove();
        try {
            accountRepository.delete(studentAccount);
            log.info("Student account with id '{}' has been removed.", studentAccountId);
        } catch (final Exception ex) {
            final String errorMessage = String.format("Something was wrong during delete student account with id '%s'.", studentAccountId);
            throw new StudentException(errorMessage, ex);
        }
    }

    private AccountEntity findAccount(final UUID studentAccountId) {
        return accountRepository.findByIdAndRolesName(studentAccountId, RoleName.ROLE_STUDENT)
                .orElseThrow(() -> new AccountNotFoundException(studentAccountId, RoleName.ROLE_STUDENT));
    }
}
