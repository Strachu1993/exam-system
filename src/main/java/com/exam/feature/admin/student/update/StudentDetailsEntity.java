package com.exam.feature.admin.student.update;

import com.exam.commons.validator.annotation.IsValidAlbumNumber;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

@Setter(AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(exclude = "account")
@ToString(exclude = "account")
@Entity
@Table(name = "student_details")
class StudentDetailsEntity {

    @Id
    @Setter(AccessLevel.NONE)
    private UUID id;

    @IsValidAlbumNumber
    @Column
    private String albumNumber;

    @Column
    private String name;

    @Column
    private String surname;

    @JoinColumn(name = "fk_account")
    @OneToOne
    private AccountEntity account;

    @JoinColumn(name = "fk_group")
    @ManyToOne
    private StudentGroupEntity studentGroup;
}
