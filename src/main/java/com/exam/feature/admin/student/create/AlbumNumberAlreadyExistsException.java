package com.exam.feature.admin.student.create;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

class AlbumNumberAlreadyExistsException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Album number '%s' already exists.";

    AlbumNumberAlreadyExistsException(final String albumNumber) {
        super(String.format(MESSAGE, albumNumber));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getType() {
        return "album-number-already-exists";
    }
}
