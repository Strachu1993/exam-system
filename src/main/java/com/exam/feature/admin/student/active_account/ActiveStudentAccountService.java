package com.exam.feature.admin.student.active_account;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.custom.not_found.AccountNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class ActiveStudentAccountService {

    private final AccountRepository accountRepository;

    @Transactional
    public void activeStudentAccount(final UUID studentAccountId) {
        final AccountEntity studentAccount = accountRepository.findByIdAndRolesName(studentAccountId, RoleName.ROLE_STUDENT)
                .orElseThrow(() -> new AccountNotFoundException(studentAccountId, RoleName.ROLE_STUDENT));

        studentAccount.enableStudentAccount();
        accountRepository.save(studentAccount);
        log.info("Student account with id '{}' was activated.", studentAccountId);
    }
}
