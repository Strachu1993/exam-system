package com.exam.feature.admin.student.remove;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(exclude = "studentScore")
@ToString(exclude = "studentScore")
@Entity
@Table(name = "student_question")
class StudentQuestionEntity {

    @Id
    private UUID id;

    @JoinColumn(name = "fk_student_score")
    @ManyToOne
    private StudentScoreEntity studentScore;

    @OneToMany(mappedBy = "studentQuestion", cascade = CascadeType.REMOVE)
    private Set<StudentAnswerEntity> studentAnswers;
}
