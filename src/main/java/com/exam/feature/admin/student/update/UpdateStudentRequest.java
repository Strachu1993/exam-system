package com.exam.feature.admin.student.update;

import com.exam.commons.Constraint;
import com.exam.commons.validator.annotation.IsValidAlbumNumber;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
class UpdateStudentRequest {

    @NotBlank
    @Size(min = Constraint.StudentAccount.MIN_NAME_LENGTH, max = Constraint.StudentAccount.MAX_NAME_LENGTH)
    private final String name;

    @NotBlank
    @Size(min = Constraint.StudentAccount.MIN_SURNAME_LENGTH, max = Constraint.StudentAccount.MAX_SURNAME_LENGTH)
    private final String surname;

    @NotNull
    private final UUID groupId;

    @IsValidAlbumNumber
    private final String albumNumber;
}
