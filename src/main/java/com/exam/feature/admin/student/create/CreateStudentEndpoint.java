package com.exam.feature.admin.student.create;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "${api.url.path.admin}/students")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class CreateStudentEndpoint {

    private final CreateStudentService createStudentService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void createStudent(@RequestBody @Valid final CreateStudentRequest request) {
        createStudentService.createStudent(request);
    }
}
