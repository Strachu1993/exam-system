package com.exam.feature.admin.student.score_details;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.enums.Rating;
import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
class ScoreDetailsResponse {

    private final UUID examId;
    private final LocalDateTime start;
    private final LocalDateTime end;
    private final String description;
    private final ExamStatus examStatus;
    private final UUID scoreId;
    private final Integer correctQuestions;
    private final Integer inCorrectQuestions;
    private final Integer score;
    private final Rating rating;
    private final StudentScoreStatus scoreStatus;
}
