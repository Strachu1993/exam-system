package com.exam.feature.admin.student.active_account;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/students")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class ActiveStudentAccountEndpoint {

    private final ActiveStudentAccountService activeStudentAccountService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(value = "/{studentAccountId}/active")
    void activeStudentAccount(@PathVariable("studentAccountId") final UUID studentAccountId) {
        activeStudentAccountService.activeStudentAccount(studentAccountId);
    }
}
