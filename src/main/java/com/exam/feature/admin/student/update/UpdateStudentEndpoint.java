package com.exam.feature.admin.student.update;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/students")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class UpdateStudentEndpoint {

    private final UpdateStudentService updateStudentService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    void updateStudent(@PathVariable("id") final UUID studentAccountId, @RequestBody @Valid final UpdateStudentRequest request) {
        updateStudentService.updateStudent(studentAccountId, request);
    }
}
