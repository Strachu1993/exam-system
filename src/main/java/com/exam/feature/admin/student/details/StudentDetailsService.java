package com.exam.feature.admin.student.details;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.custom.not_found.AccountNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentDetailsService {

    private final AccountReadRepository accountReadRepository;

    StudentDetailsResponse getStudentDetails(final UUID studentAccountId) {
        return findStudentAccount(studentAccountId).toStudentDetailsResponse();
    }

    private AccountEntity findStudentAccount(final UUID studentAccountId) {
        return accountReadRepository.findByIdAndRolesName(studentAccountId, RoleName.ROLE_STUDENT)
                .orElseThrow(() -> new AccountNotFoundException(studentAccountId, RoleName.ROLE_STUDENT));
    }
}
