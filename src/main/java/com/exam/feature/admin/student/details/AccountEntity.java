package com.exam.feature.admin.student.details;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Builder(access = AccessLevel.PACKAGE)
@Getter(value = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "account")
class AccountEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "rel_role_account",
            joinColumns = @JoinColumn(name = "fk_account"),
            inverseJoinColumns = @JoinColumn(name = "fk_role"))
    private Set<RoleEntity> roles;

    @OneToOne(mappedBy = "account")
    private StudentDetailsEntity studentDetails;

    StudentDetailsResponse toStudentDetailsResponse() {
        final StudentDetailsResponse.StudentGroupResponse groupResponse = studentDetails.getStudentGroup().toStudentGroupResponse();
        final List<StudentDetailsResponse.ExamResponse> examResponses = studentDetails.getStudentScores().stream()
                .map(StudentScoreEntity::toExamResponse)
                .collect(Collectors.toList());

        return StudentDetailsResponse.builder()
                .accountId(id)
                .enabled(enabled)
                .studentDetailsId(studentDetails.getId())
                .albumNumber(studentDetails.getAlbumNumber())
                .name(studentDetails.getName())
                .surname(studentDetails.getSurname())
                .group(groupResponse)
                .exams(examResponses)
                .build();
    }
}
