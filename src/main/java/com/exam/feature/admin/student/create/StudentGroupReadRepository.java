package com.exam.feature.admin.student.create;

import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface StudentGroupReadRepository extends Repository<StudentGroupEntity, UUID> {

    Optional<StudentGroupEntity> findById(UUID id);
}
