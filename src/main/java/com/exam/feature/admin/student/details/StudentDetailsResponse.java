package com.exam.feature.admin.student.details;

import com.exam.commons.enums.Rating;
import com.exam.commons.enums.StudentScoreStatus;
import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
class StudentDetailsResponse {

    private final UUID accountId;
    private final boolean enabled;
    private final UUID studentDetailsId;
    private final String name;
    private final String surname;
    private final String albumNumber;
    private final StudentGroupResponse group;
    private final List<ExamResponse> exams;

    @Builder(access = AccessLevel.PACKAGE)
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    @Getter
    @EqualsAndHashCode
    @ToString
    static class StudentGroupResponse {
        private final UUID id;
        private final String name;
        private final String year;
        private final Subgroup subgroup;
    }

    @Builder(access = AccessLevel.PACKAGE)
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    @Getter
    @EqualsAndHashCode
    @ToString
    static class ExamResponse {
        private final UUID examId;
        private final LocalDateTime start;
        private final LocalDateTime end;
        private final String description;
        private final UUID scoreId;
        private final Rating rating;
        private final StudentScoreStatus scoreStatus;
    }
}
