package com.exam.feature.admin.student.remove;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

class StudentHasStartedExamException extends MvcExceptionAbstract {

    private static final String MESSAGE = "Student with id '%s' has at least one exam started.";

    StudentHasStartedExamException(final UUID studentAccountId) {
        super(String.format(MESSAGE, studentAccountId));
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "student-has-least-one-exam-started";
    }
}
