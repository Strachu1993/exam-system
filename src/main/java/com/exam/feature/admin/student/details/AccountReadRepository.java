package com.exam.feature.admin.student.details;

import com.exam.commons.enums.RoleName;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface AccountReadRepository extends Repository<AccountEntity, UUID> {

    Optional<AccountEntity> findByIdAndRolesName(final UUID id, final RoleName role);
}


