package com.exam.feature.admin.student.create;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface AccountRepository extends JpaRepository<AccountEntity, UUID> {

    boolean existsByStudentDetailsAlbumNumber(String albumNumber);
}