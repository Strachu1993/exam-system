package com.exam.feature.admin.setting.system.rating.list;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.Repository;

import java.util.List;

@org.springframework.stereotype.Repository
interface RatingReadRepository extends Repository<RatingEntity, String> {

    List<RatingEntity> findAll(Sort sort);
}
