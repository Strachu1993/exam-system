package com.exam.feature.admin.setting.system.rating.list;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class RatingListService {

    private final RatingReadRepository ratingRepository;

    List<RatingResponse> getExamRatingSettings() {
        final Sort sort = Sort.by(
                Sort.Order.desc("position"));
        return ratingRepository.findAll(sort).stream()
                .map(RatingResponse::of)
                .collect(Collectors.toList());
    }
}
