package com.exam.feature.admin.setting.system.rating.update;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "${api.url.path.admin}/system/setting/ratings/update")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class RatingUpdateEndpoint {

    private final RatingUpdateService ratingChangeService;

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void getStudentDetails(@RequestBody final List<RatingRequest> ratingRequests) {
        ratingChangeService.updateRatings(ratingRequests);
    }
}
