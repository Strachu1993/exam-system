package com.exam.feature.admin.setting.system.rating.list;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "${api.url.path.admin}/system/setting/ratings")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class RatingListEndpoint {

    private final RatingListService ratingChangeService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<RatingResponse> getExamRatingSettings() {
        return ratingChangeService.getExamRatingSettings();
    }
}
