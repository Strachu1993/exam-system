package com.exam.feature.admin.setting.system.rating.update;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface RatingRepository extends JpaRepository<RatingEntity, String> {

}

