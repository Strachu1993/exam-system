package com.exam.feature.admin.setting.system.rating.update;

import com.exam.commons.enums.Rating;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "rating")
class RatingEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    @Enumerated(value = EnumType.STRING)
    private Rating symbol;

    private int maxScope;

    private int minScope;

    @Column(insertable = false, updatable = false)
    private int position;

    void update(final RatingRequest ratingRequest) {
        this.symbol = ratingRequest.getSymbol();
        this.maxScope = ratingRequest.getMaxScope();
        this.minScope = ratingRequest.getMinScope();
    }
}