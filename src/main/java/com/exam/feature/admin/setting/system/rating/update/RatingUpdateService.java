package com.exam.feature.admin.setting.system.rating.update;

import com.exam.commons.exception.custom.not_found.RatingNotFound;
import com.exam.commons.exception.custom.validate.RatingScopeException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class RatingUpdateService {

    private final RatingRepository ratingRepository;

    @Transactional
    public void updateRatings(final List<RatingRequest> ratingRequests) {
        final List<RatingEntity> ratingEntities = ratingRepository.findAll();

        for (final RatingRequest ratingRequest : ratingRequests) {
            final RatingEntity ratingEntity = filterById(ratingEntities, ratingRequest.getId());
            ratingEntity.update(ratingRequest);
        }

        validateRatingScores(ratingEntities);
        log.info("Setting rating was changed.");
    }

    void validateRatingScores(final List<RatingEntity> ratingEntities) {
        final List<RatingEntity> ratings = ratingEntities.stream()
                .sorted(Comparator.comparingInt(RatingEntity::getPosition))
                .collect(Collectors.toList());

        final int ratingsSize = ratingEntities.size();
        validateTheSmallestValue(ratings);
        validateTheGreatestVaue(ratings, ratingsSize);
        validateRanges(ratings, ratingsSize);
    }

    private void validateRanges(List<RatingEntity> ratings, int ratingsSize) {
        for (int i=0 ; i<ratingsSize - 1 ; i++) {
            final RatingEntity r1 = ratings.get(i);
            final RatingEntity r2 = ratings.get(i + 1);

            if (r1.getMaxScope() + 1 != r2.getMinScope()) {
                final int difference = r1.getMaxScope() - r2.getMinScope();
                throw new RatingScopeException(r1.getSymbol(), r2.getSymbol(), difference);
            }
        }
    }

    private void validateTheGreatestVaue(List<RatingEntity> ratings, int ratingsSize) {
        final int maxValue = ratings.get(ratingsSize - 1).getMaxScope();
        if (maxValue != 100) {
            RatingScopeException.wrongMaxValue(maxValue);
        }
    }

    private void validateTheSmallestValue(List<RatingEntity> ratings) {
        final int minValue = ratings.get(0).getMinScope();
        if (minValue != 0) {
            RatingScopeException.wrongMinValue(minValue);
        }
    }

    private RatingEntity filterById(final List<RatingEntity> ratingEntities, final UUID ratingId) {
        return ratingEntities.stream()
                .filter(x -> x.getId().equals(ratingId))
                .findFirst()
                .orElseThrow(() -> new RatingNotFound(ratingId));
    }
}
