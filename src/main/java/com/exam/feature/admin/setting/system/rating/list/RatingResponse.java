package com.exam.feature.admin.setting.system.rating.list;

import com.exam.commons.enums.Rating;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@ToString
class RatingResponse {

    private UUID id;
    private Rating symbol;
    private int maxScope;
    private int minScope;

    static RatingResponse of(final RatingEntity ratingEntity) {
        return RatingResponse.builder()
                .id(ratingEntity.getId())
                .symbol(ratingEntity.getSymbol())
                .maxScope(ratingEntity.getMaxScope())
                .minScope(ratingEntity.getMinScope())
                .build();
    }
}
