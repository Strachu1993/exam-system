package com.exam.feature.admin.question.subject_data;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Builder
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "question")
class QuestionEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String question;

    @Column(insertable = false, updatable = false)
    private String code;

    @Column(insertable = false, updatable = false)
    private String codeSyntax;

    @Column(insertable = false, updatable = false)
    private short correctAnswers;

    @Basic
    @Column(insertable = false, updatable = false)
    private byte[] image;

    @JoinColumn(name = "fk_subject")
    @ManyToOne
    private SubjectEntity subject;

    @JoinColumn(name = "fk_question")
    @OneToMany(fetch = FetchType.EAGER)
    private List<AnswerEntity> answers;

    QuestionDetailsResponse toQuestionDetailsResponse() {
        final List<QuestionDetailsResponse.AnswerDetailsResponse> answerResponse = answers.stream()
                .map(AnswerEntity::toAnswerEntity)
                .collect(Collectors.toList());

        return QuestionDetailsResponse.builder()
                .id(id)
                .subjectName(subject.getName())
                .question(question)
                .code(code)
                .codeSyntax(codeSyntax)
                .correctAnswers(correctAnswers)
                .totalAnswers(answers.size())
                .answers(answerResponse)
                .image(image)
                .build();
    }
}
