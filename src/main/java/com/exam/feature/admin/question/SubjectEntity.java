package com.exam.feature.admin.question;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "subject")
class SubjectEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String name;

    @OneToMany(mappedBy = "subject")
    private List<QuestionEntity> questions;
}