package com.exam.feature.admin.question;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class QuestionResponse {

    private UUID id;
    private String question;
    private boolean hasCode;
    private int totalAnswers;
    private int correctAnswers;

    static QuestionResponse of(final QuestionEntity question) {
        final boolean hasCode = question.getCode() != null;
        final int totalAnswersCount = question.getAnswers().size();

        return QuestionResponse.builder()
                .id(question.getId())
                .question(question.getQuestion())
                .hasCode(hasCode)
                .totalAnswers(totalAnswersCount)
                .correctAnswers(question.getCorrectAnswers())
                .build();
    }
}
