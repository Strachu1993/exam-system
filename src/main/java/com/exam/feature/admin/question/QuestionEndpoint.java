package com.exam.feature.admin.question;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/question")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class QuestionEndpoint {

    private final QuestionService questionService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    void add(@RequestPart @Valid final QuestionRequest questionRequest, @RequestPart(required = false) final MultipartFile image) throws IOException {
        questionService.add(questionRequest, image);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(value = "/update/{id}")
    void update(@PathVariable("id") final UUID questionId, @RequestPart @Valid final QuestionRequest questionRequest, @RequestPart(required = false) final MultipartFile image) throws IOException {
        questionService.update(questionId, questionRequest, image);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable("id") final UUID questionId) {
        questionService.delete(questionId);
    }
}
