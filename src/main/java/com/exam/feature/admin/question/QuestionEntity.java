package com.exam.feature.admin.question;

import com.exam.commons.exception.custom.validate.LeastOneCorrectAnswerRequiredException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Builder
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "question")
class QuestionEntity {

    @Id
    @Setter(value = AccessLevel.NONE)
    private UUID id;

    private String question;

    private String code;

    private String codeSyntax;

    @Setter(value = AccessLevel.NONE)
    private short correctAnswers;

    @Basic
    private byte[] image;

    @Setter(value = AccessLevel.NONE)
    @JoinColumn(name = "fk_question")
    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    private List<AnswerEntity> answers;

    @JoinColumn(name = "fk_subject")
    @ManyToOne
    private SubjectEntity subject;

    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        calculateCorrectAnswers();
    }

    void cleanAndSetAnswers(final List<AnswerEntity> answers) {
        this.answers.clear();
        this.answers.addAll(answers);
        calculateCorrectAnswers();
    }

    void calculateCorrectAnswers() {
        final long correctAnswers = answers.stream()
                .filter(AnswerEntity::isCorrect)
                .count();

        if (correctAnswers < 1) {
            throw new LeastOneCorrectAnswerRequiredException();
        }

        this.correctAnswers = (short) correctAnswers;
    }
}
