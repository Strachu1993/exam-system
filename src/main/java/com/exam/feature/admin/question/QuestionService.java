package com.exam.feature.admin.question;

import com.exam.commons.exception.custom.jpa.JpaDataIntegrityViolationException;
import com.exam.commons.exception.custom.not_found.QuestionNotFound;
import com.exam.commons.exception.custom.not_found.SubjectNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class QuestionService {

    private final QuestionRepository questionRepository;
    private final SubjectRepository subjectRepository;

    @Transactional
    public void add(final QuestionRequest questionRequest, final MultipartFile image) throws IOException {
        final byte[] imageBytes = image != null ? image.getBytes() : null;
        final QuestionEntity newQuestion = mapToEntity(questionRequest, imageBytes);
        questionRepository.save(newQuestion);
        log.info("Question has been added.");
    }

    @Transactional
    public void update(final UUID questionId, final QuestionRequest questionRequest, final MultipartFile image) throws IOException {
        final byte[] imageBytes = image != null ? image.getBytes() : null;
        final QuestionEntity question = findQuestionEntity(questionId);
        final SubjectEntity subject = findSubjectEntity(questionRequest.getSubjectId());
        final List<AnswerEntity> answers = prepareAnswers(questionRequest.getAnswers());

        question.setQuestion(questionRequest.getQuestion());
        question.setCode(questionRequest.getCode());
        question.setSubject(subject);
        question.setImage(imageBytes);
        question.setCodeSyntax(questionRequest.getCodeSyntax());
        question.cleanAndSetAnswers(answers);

        questionRepository.save(question);
        log.info("Question with id '{}' has been updated.", questionId);
    }

    @Transactional
    public void delete(final UUID questionId) {
        try {
            final QuestionEntity question = findQuestionEntity(questionId);
            questionRepository.delete(question);
            log.info("Question with id '{}' has been removed.", questionId);
        } catch (final DataIntegrityViolationException ex) {
            throw new JpaDataIntegrityViolationException(ex);
        }
    }

    private QuestionEntity mapToEntity(final QuestionRequest questionRequest, final byte[] imageBytes) {
        final UUID subjectId = questionRequest.getSubjectId();
        final SubjectEntity subjectEntity = findSubjectEntity(subjectId);
        final List<AnswerEntity> answers = prepareAnswers(questionRequest.getAnswers());

        return QuestionEntity.builder()
                .question(questionRequest.getQuestion())
                .code(questionRequest.getCode())
                .codeSyntax(questionRequest.getCodeSyntax())
                .answers(answers)
                .subject(subjectEntity)
                .image(imageBytes)
                .build();
    }

    private List<AnswerEntity> prepareAnswers(final List<QuestionRequest.AnswerRequest> answersRequest) {
        return answersRequest.stream()
                .map(this::mapToEntity)
                .collect(Collectors.toList());
    }

    private AnswerEntity mapToEntity(final QuestionRequest.AnswerRequest answerRequest) {
        return AnswerEntity.builder()
                .answer(answerRequest.getAnswer())
                .correct(answerRequest.isCorrect())
                .build();
    }

    private SubjectEntity findSubjectEntity(final UUID subjectId) {
        return subjectRepository.findById(subjectId)
                .orElseThrow(() -> new SubjectNotFoundException(subjectId));
    }

    private QuestionEntity findQuestionEntity(final UUID questionId) {
        return questionRepository.findById(questionId)
                .orElseThrow(() -> new QuestionNotFound(questionId));
    }
}
