package com.exam.feature.admin.question.subject_data;

import com.exam.commons.exception.custom.not_found.QuestionNotFound;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class QuestionDetailsService {

    private final QuestionReadRepository questionReadRepository;

    QuestionDetailsResponse getQuestionDetails(final UUID questionId) {
        return findQuestionEntity(questionId).toQuestionDetailsResponse();
    }

    private QuestionEntity findQuestionEntity(final UUID questionId) {
        return questionReadRepository.findById(questionId)
                .orElseThrow(() -> new QuestionNotFound(questionId));
    }
}
