package com.exam.feature.admin.question.subject_data;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/question")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class QuestionDetailsEndpoint {

    private final QuestionDetailsService subjectDataService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/details/{questionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    QuestionDetailsResponse getQuestionDetails(@PathVariable final UUID questionId) {
        return subjectDataService.getQuestionDetails(questionId);
    }
}
