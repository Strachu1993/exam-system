package com.exam.feature.admin.question.subject_data;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Builder
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "answer")
class AnswerEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String answer;

    @Column(insertable = false, updatable = false)
    private boolean correct;

    QuestionDetailsResponse.AnswerDetailsResponse toAnswerEntity() {
        return QuestionDetailsResponse.AnswerDetailsResponse.builder()
                .answer(answer)
                .correct(correct)
                .build();
    }
}
