package com.exam.feature.admin.question.subject_data;

import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface QuestionReadRepository extends Repository<QuestionEntity, UUID> {

    Optional<QuestionEntity> findById(UUID questionId);
}
