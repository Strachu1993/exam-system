package com.exam.feature.admin.question;

import com.exam.commons.Constraint;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class QuestionRequest {

    private UUID subjectId;

    @Size(min = Constraint.Question.MIN_QUESTION_LENGTH, max = Constraint.Question.MAX_QUESTION_LENGTH)
    private String question;

    @Size(max = Constraint.Question.MAX_CODE_LENGTH)
    private String code;

    @Size(max = Constraint.Question.MAX_CODE_SYNTAX_SIZE)
    private String codeSyntax;

    @Size(min = Constraint.Question.MIN_ANSWER_SIZE)
    private List<AnswerRequest> answers;

    @Builder
    @Getter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class AnswerRequest {

        private UUID id;

        @Size(min = Constraint.Question.MIN_ANSWER_LENGTH, max = Constraint.Question.MAX_ANSWER_LENGTH)
        private String answer;

        private boolean correct;
    }
}
