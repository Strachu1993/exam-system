package com.exam.feature.admin.question.subject_data;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class QuestionDetailsResponse {

    private UUID id;
    private String subjectName;
    private String question;
    private String code;
    private String codeSyntax;
    private int totalAnswers;
    private int correctAnswers;
    private byte[] image;
    private List<AnswerDetailsResponse> answers;

    @Builder
    @Getter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class AnswerDetailsResponse {
        private String answer;
        private boolean correct;
    }
}
