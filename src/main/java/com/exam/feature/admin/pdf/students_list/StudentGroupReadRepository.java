package com.exam.feature.admin.pdf.students_list;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface StudentGroupReadRepository extends Repository<StudentGroupEntity, UUID> {

    List<StudentGroupEntity> findByIdIn(List groupIds, Sort sort);
}
