package com.exam.feature.admin.pdf.exam_score;

import com.exam.commons.enums.Rating;
import com.exam.commons.exception.custom.error.GeneratePdfException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

@Service
class GeneratePdfExamScoreService {

    private static final String FONT_PATCH = "font/arialuni.ttf";
    private static final String DOCUMENT_METADATA_NAME = "PDF with exam scores";
    private static final String HEADER_PAGE = "Wynik egzaminu";
    private static final String SEPARATE_LINE = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";

    private Font HEADER_FONT;
    private Font SCORE_FONT;
    private Font SCORE_HEADER_FONT;

    {
        try {
            final BaseFont BASE_FONT = BaseFont.createFont(FONT_PATCH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            HEADER_FONT = new Font(BASE_FONT, 14, Font.BOLD, BaseColor.BLACK);
            SCORE_HEADER_FONT = new Font(BASE_FONT, 10, Font.BOLD, BaseColor.BLACK);
            SCORE_FONT = new Font(BASE_FONT, 10, Font.NORMAL);
        } catch (final Exception e) {
            throw new GeneratePdfException(e);
        }
    }

    ByteArrayInputStream createPdf(final ExamScoreDto examScoreDto) {
        final Document document;
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            document = new Document();
            PdfWriter.getInstance(document, output);
            document.open();
            addMetaData(document);
            addExamContent(document, examScoreDto);
            document.close();
        } catch (final Exception e) {
            throw new GeneratePdfException(e);
        }

        return new ByteArrayInputStream(output.toByteArray());
    }

    private void addExamContent(final Document document, final ExamScoreDto examScoreDto) throws DocumentException {
        final Paragraph header = new Paragraph(HEADER_PAGE, HEADER_FONT);
        header.setAlignment(Element.ALIGN_CENTER);
        document.add(header);
        final Map<String, List<ExamScoreDto.StudentScoreDto>> studentsScores = examScoreDto.getStudentsScores();

        for (final Map.Entry<String, List<ExamScoreDto.StudentScoreDto>> entry : studentsScores.entrySet()) {
            final String key = entry.getKey();
            final List<ExamScoreDto.StudentScoreDto> mapValue = entry.getValue();
            document.add(Chunk.NEWLINE);

            final Paragraph groupHeader = new Paragraph(key, SCORE_HEADER_FONT);
            groupHeader.setAlignment(Element.ALIGN_CENTER);
            document.add(groupHeader);

            for(ExamScoreDto.StudentScoreDto studentDto: mapValue) {
                addLine(document);
                final Paragraph studentParagraph = new Paragraph();
                studentParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
                final Rating rating = studentDto.getRating();
                getScoreRow(studentParagraph, "Album: ", studentDto.getAlbumNumber());
                getScoreRow(studentParagraph, ", Imię: ", studentDto.getName());
                getScoreRow(studentParagraph, ", Nazwisko: ", studentDto.getSurname());
                getScoreRow(studentParagraph, ", Ocena: ", rating.getRealName() + "(" + rating.getRate() + ")");
                getScoreRow(studentParagraph, ", Wynik: ", studentDto.getScore() + "%");
                document.add(studentParagraph);
            }
            addLine(document);
        }
    }

    private void getScoreRow(final Paragraph paragraph, final String header, final String value) {
        final Chunk albumHeader = new Chunk(header, SCORE_HEADER_FONT);
        final Chunk albumValue = new Chunk(value, SCORE_FONT);
        paragraph.add(albumHeader);
        paragraph.add(albumValue);
    }

    private void addMetaData(final Document document) {
        document.addTitle(DOCUMENT_METADATA_NAME);
    }

    private void addLine(final Document document) throws DocumentException {
        document.add(new Paragraph(SEPARATE_LINE));
    }
}
