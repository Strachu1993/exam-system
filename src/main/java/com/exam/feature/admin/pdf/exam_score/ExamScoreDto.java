package com.exam.feature.admin.pdf.exam_score;

import com.exam.commons.enums.Rating;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@ToString
@EqualsAndHashCode
@Getter(AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class ExamScoreDto {

    private UUID examId;
    private LocalDateTime start;
    private LocalDateTime end;
    private String description;
    private Map<String, List<StudentScoreDto>> studentsScores;

    static ExamScoreDto of(final ExamEntity exam) {
        final Map<String, List<StudentScoreDto>> studentsScores = exam.getStudentScores().stream()
                .map(StudentScoreDto::of)
                .collect(Collectors.groupingBy(StudentScoreDto::getGroupFullName,
                        Collectors.mapping(x -> x,
                                Collectors.toList())));

        return ExamScoreDto.builder()
                .examId(exam.getId())
                .start(exam.getStart())
                .end(exam.getEnd())
                .description(exam.getDescription())
                .studentsScores(studentsScores)
                .build();
    }

    @ToString
    @EqualsAndHashCode
    @Getter(AccessLevel.PACKAGE)
    @Builder(access = AccessLevel.PACKAGE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    static class StudentScoreDto {

        private String albumNumber;
        private String surname;
        private String name;
        private String groupFullName;
        private Rating rating;
        private Integer score;

        static StudentScoreDto of(final StudentScoreEntity studentScore) {
            final StudentDetailsEntity studentDetails = studentScore.getStudentDetails();

            return StudentScoreDto.builder()
                    .albumNumber(studentDetails.getAlbumNumber())
                    .name(studentDetails.getName())
                    .surname(studentDetails.getSurname())
                    .groupFullName(studentDetails.getFullGroupName())
                    .rating(studentScore.getRating())
                    .score(studentScore.getScore())
                    .build();
        }
    }
}

