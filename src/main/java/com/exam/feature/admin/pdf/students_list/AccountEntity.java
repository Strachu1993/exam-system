package com.exam.feature.admin.pdf.students_list;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "account")
class AccountEntity {

	@Id
    @Setter(value = AccessLevel.NONE)
    private UUID id;

    @Column(unique = true, insertable = false, updatable = false)
    private String login;

    @OneToOne(mappedBy = "account")
    private StudentDetailsEntity studentDetails;
}
