package com.exam.feature.admin.pdf.students_list;

import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "studentDetails")
@ToString(exclude = "studentDetails")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_group")
class StudentGroupEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(insertable = false, updatable = false)
    private Subgroup subgroup;

    @JoinColumn(name = "fk_group")
    @OneToMany(fetch = FetchType.EAGER)
    private List<StudentDetailsEntity> studentDetails;
}
