package com.exam.feature.admin.pdf.students_list;

import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ToString
@EqualsAndHashCode
@Getter(AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class StudentDto {

    private String albumNumber;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String groupFullName;

    static Map<String, List<StudentDto>> of(final StudentGroupEntity studentGroup) {
        final Map<String, List<StudentDto>> result = new HashMap<>();
        final List<StudentDto> resultList = new ArrayList<>();
        final String groupFullName = getFullGroupName(studentGroup);

        final List<StudentDetailsEntity> studentDetails = studentGroup.getStudentDetails();
        for (StudentDetailsEntity student: studentDetails) {
            final StudentDto studentDto = prepareStudentDto(groupFullName, student);
            resultList.add(studentDto);
        }
        result.put(groupFullName, resultList);

        return result;
    }

    static StudentDto of(final StudentDetailsEntity studentDetails) {
        final String groupFullName = getFullGroupName(studentDetails.getStudentGroup());

        return prepareStudentDto(groupFullName, studentDetails);
    }

    private static StudentDto prepareStudentDto(final String groupFullName, final StudentDetailsEntity student) {
        final String login = student.getAccount().getLogin();

        return StudentDto.builder()
                .groupFullName(groupFullName)
                .albumNumber(student.getAlbumNumber())
                .name(student.getName())
                .login(login)
                .password(student.getPassword())
                .surname(student.getSurname())
                .build();
    }

    private static String getFullGroupName(final StudentGroupEntity studentGroup) {
        final String groupName = studentGroup.getName();
        final Subgroup subgroup = studentGroup.getSubgroup();

        return groupName + " " + subgroup;
    }
}
