package com.exam.feature.admin.pdf.students_list;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/pdf")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentsPdfEndpoint {

    private final StudentsListPdfService studentsListPdfService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/group/students", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> generateStudentPdfByStudentGroup(@RequestBody final List<UUID> studentsGroupIds) {
        final ByteArrayInputStream resultPdf = studentsListPdfService.generatePdf(studentsGroupIds);
        return getPdfFIle(resultPdf, "StudentsFromGroup");
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/exam/{examId}/students", produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> generateStudentPdfByExam(@PathVariable final UUID examId) {
        final ByteArrayInputStream resultPdf = studentsListPdfService.generatePdf(examId);
        final String fileName = "StudentsFromExam_" + examId;
        return getPdfFIle(resultPdf, fileName);
    }

    private ResponseEntity<InputStreamResource> getPdfFIle(final ByteArrayInputStream bis, final String fileName) {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=" + fileName + ".pdf");

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
