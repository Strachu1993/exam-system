package com.exam.feature.admin.pdf.exam_score;

import com.exam.commons.exception.custom.not_found.ExamNotFound;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.UUID;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class ExamScorePdfService {

    private final ExamReadRepository examReadRepository;
    private final GeneratePdfExamScoreService pdfService;

    ByteArrayInputStream generatePdf(final UUID examId) {
        final ExamEntity exam = findExamEntity(examId);
        exam.validateExamStatus();
        final ExamScoreDto examScoreDto = ExamScoreDto.of(exam);

        return pdfService.createPdf(examScoreDto);
    }

    private ExamEntity findExamEntity(UUID examId) {
        return examReadRepository.findById(examId).orElseThrow(() -> new ExamNotFound(examId));
    }
}
