package com.exam.feature.admin.pdf.students_list;

import com.exam.commons.exception.custom.not_found.ExamNotFound;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentsListPdfService {

    private final ExamReadRepository examReadRepository;
    private final StudentGroupReadRepository studentGroupRepository;
    private final GeneratePdfStudentsService pdfService;

    ByteArrayInputStream generatePdf(final List<UUID> groupIds) {
        final Map<String, List<StudentDto>> students = findStudentsByGroups(groupIds);
        return pdfService.createPdf(students);
    }

    ByteArrayInputStream generatePdf(final UUID examId) {
        final Map<String, List<StudentDto>> students = findStudentsFromExam(examId);
        return pdfService.createPdf(students);
    }

    private Map<String, List<StudentDto>> findStudentsFromExam(final UUID examId) {
        final ExamEntity exam = examReadRepository.findById(examId).orElseThrow(() -> new ExamNotFound(examId));
        return exam.getStudentScores().stream()
                .map(StudentScoreEntity::getStudentDetails)
                .map(StudentDto::of)
                .collect(Collectors.groupingBy(StudentDto::getGroupFullName,
                        Collectors.mapping(x -> x,
                                Collectors.toList())));
    }

    private Map<String, List<StudentDto>> findStudentsByGroups(final List<UUID> groupIds) {
        final Sort sort = Sort.by(
                Sort.Order.desc("name"),
                Sort.Order.desc("subgroup"));
        final List<StudentGroupEntity> studentList = studentGroupRepository.findByIdIn(groupIds, sort);

        final Map<String, List<StudentDto>> result = new HashMap<>();
        for (StudentGroupEntity group : studentList) {
            final Map<String, List<StudentDto>> studentFromGroup = StudentDto.of(group);
            result.putAll(studentFromGroup);
        }

        return result;
    }
}
