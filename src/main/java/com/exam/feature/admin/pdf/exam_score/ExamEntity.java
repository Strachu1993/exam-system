package com.exam.feature.admin.pdf.exam_score;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.exception.custom.validate.ExamNotFinishException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    private UUID id;

    @Column
    private LocalDateTime start;

    @Column
    private LocalDateTime end;

    @Column
    private String description;

    @Enumerated(EnumType.STRING)
    private ExamStatus status;

    @JoinColumn(name = "fk_exam")
    @OneToMany
    private List<StudentScoreEntity> studentScores;

    void validateExamStatus() {
        if (status != ExamStatus.FINISH) {
            throw new ExamNotFinishException(id, status);
        }
    }
}
