package com.exam.feature.admin.pdf.exam_score;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/pdf")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class ExamScorePdfEndpoint {

    private final ExamScorePdfService examScorePdfService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/exam/{examId}/scores", produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> generateExamScorePdf(@PathVariable final UUID examId) {
        final ByteArrayInputStream resultPdf = examScorePdfService.generatePdf(examId);
        final String fileName = "ExamScores_" + examId;
        return getPdfFIle(resultPdf, fileName);
    }

    private ResponseEntity<InputStreamResource> getPdfFIle(final ByteArrayInputStream bis, final String fileName) {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=" + fileName + ".pdf");

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
