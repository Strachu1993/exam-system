package com.exam.feature.admin.pdf.exam_score;

import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface ExamReadRepository extends Repository<ExamEntity, UUID> {

    Optional<ExamEntity> findById(UUID examId);
}
