package com.exam.feature.admin.pdf.students_list;

import com.exam.commons.exception.custom.error.GeneratePdfException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

@Service
class GeneratePdfStudentsService {

    private static final String FONT_PATCH = "font/arialuni.ttf";
    private static final String DOCUMENT_METADATA_NAME = "PDF with student";
    private static final String HEADER_PAGE = "Lita studentów";
    private static final String SEPARATE_LINE = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";

    private Font HEADER_FONT;
    private Font STUDENT_FONT;
    private Font STUDENT_HEADER_FONT;

    {
        try {
            final BaseFont BASE_FONT = BaseFont.createFont(FONT_PATCH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            HEADER_FONT = new Font(BASE_FONT, 14, Font.BOLD, BaseColor.BLACK);
            STUDENT_HEADER_FONT = new Font(BASE_FONT, 10, Font.BOLD, BaseColor.BLACK);
            STUDENT_FONT = new Font(BASE_FONT, 10, Font.NORMAL);
        } catch (final Exception e) {
            throw new GeneratePdfException(e);
        }
    }

    ByteArrayInputStream createPdf(final Map<String, List<StudentDto>> students) {
        final Document document;
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            document = new Document();
            PdfWriter.getInstance(document, output);
            document.open();
            addMetaData(document);
            addStudentContent(document, students);
            document.close();
        } catch (final Exception e) {
            throw new GeneratePdfException(e);
        }

        return new ByteArrayInputStream(output.toByteArray());
    }

    private void addStudentContent(final Document document, final Map<String, List<StudentDto>> students) throws DocumentException {
        final Paragraph header = new Paragraph(HEADER_PAGE, HEADER_FONT);
        header.setAlignment(Element.ALIGN_CENTER);
        document.add(header);

        for (final Map.Entry<String, List<StudentDto>> entry : students.entrySet()) {
            final String key = entry.getKey();
            final List<StudentDto> mapValue = entry.getValue();
            document.add(Chunk.NEWLINE);

            final Paragraph groupHeader = new Paragraph(key, STUDENT_HEADER_FONT);
            groupHeader.setAlignment(Element.ALIGN_CENTER);
            document.add(groupHeader);

            for(StudentDto studentDto: mapValue) {
                addLine(document);
                final Paragraph studentParagraph = new Paragraph();
                studentParagraph.setAlignment(Element.ALIGN_JUSTIFIED);
                getStudentRow(studentParagraph, "Album: ", studentDto.getAlbumNumber());
                getStudentRow(studentParagraph, ", Imię: ", studentDto.getName());
                getStudentRow(studentParagraph, ", Nazwisko: ", studentDto.getSurname());
                getStudentRow(studentParagraph, ", Login: ", studentDto.getLogin());
                getStudentRow(studentParagraph, ", Hasło: ", studentDto.getPassword());
                getStudentRow(studentParagraph, ", Grupa: ", studentDto.getGroupFullName());
                document.add(studentParagraph);
            }
            addLine(document);
        }
    }

    private void getStudentRow(final Paragraph paragraph, final String header, final String value) {
        final Chunk albumHeader = new Chunk(header, STUDENT_HEADER_FONT);
        final Chunk albumValue = new Chunk(value, STUDENT_FONT);
        paragraph.add(albumHeader);
        paragraph.add(albumValue);
    }

    private void addMetaData(final Document document) {
        document.addTitle(DOCUMENT_METADATA_NAME);
    }

    private void addLine(final Document document) throws DocumentException {
        document.add(new Paragraph(SEPARATE_LINE));
    }
}
