package com.exam.feature.admin.pdf.students_list;

import com.exam.commons.converter.Base64AttributeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode(exclude = "studentGroup")
@ToString(exclude = "studentGroup")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_details")
class StudentDetailsEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String albumNumber;

    @Column(insertable = false, updatable = false)
    private String name;

    @Column(insertable = false, updatable = false)
    private String surname;

    @Convert(converter = Base64AttributeConverter.class)
    @Column(insertable = false, updatable = false)
    private String password;

    @JoinColumn(name = "fk_account")
    @OneToOne
    private AccountEntity account;

    @JoinColumn(name = "fk_group")
    @ManyToOne
    private StudentGroupEntity studentGroup;
}
