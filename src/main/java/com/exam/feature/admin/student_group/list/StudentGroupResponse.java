package com.exam.feature.admin.student_group.list;

import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@ToString
class StudentGroupResponse {

    private final UUID id;
    private final String name;
    private final String year;
    private final Subgroup subgroup;
    private final int studentSize;

    static StudentGroupResponse of(final StudentGroupEntity group) {
        final int studentSize = group.getStudentDetails().size();

        return StudentGroupResponse.builder()
                .id(group.getId())
                .name(group.getName())
                .year(group.getYear())
                .subgroup(group.getSubgroup())
                .studentSize(studentSize)
                .build();
    }
}
