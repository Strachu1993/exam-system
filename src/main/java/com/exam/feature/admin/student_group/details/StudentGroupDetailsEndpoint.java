package com.exam.feature.admin.student_group.details;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/student-groups")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentGroupDetailsEndpoint {

    private final StudentGroupDetailsService studentGroupDetailsService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{studentGroupId}", produces = MediaType.APPLICATION_JSON_VALUE)
    StudentGroupDetailsResponse getStudentGroupDetails(@PathVariable final UUID studentGroupId) {
        return studentGroupDetailsService.getStudentGroupDetails(studentGroupId);
    }
}
