package com.exam.feature.admin.student_group.details;

import com.exam.commons.enums.Subgroup;
import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentGroupDetailsService implements StudentGroupDetailsFacade {

    private final StudentGroupRepository studentGroupRepository;

    @Override
    public UUID getStudentGroupDetails(final String name, final String year, final Subgroup subgroup) {
        return studentGroupRepository.findByNameAndYearAndSubgroup(name, year, subgroup)
                .map(StudentGroupEntity::getId).orElse(null);
    }

    StudentGroupDetailsResponse getStudentGroupDetails(final UUID studentGroupId) {
        return findStudentGroupEntity(studentGroupId).toStudentGroupDetailsResponse();
    }

    private StudentGroupEntity findStudentGroupEntity(final UUID studentGroupId) {
        return studentGroupRepository.findById(studentGroupId)
                .orElseThrow(() -> new StudentGroupNotFoundException(studentGroupId));
    }
}
