package com.exam.feature.admin.student_group.details;

import com.exam.commons.enums.Subgroup;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface StudentGroupRepository extends Repository<StudentGroupEntity, UUID> {

    Optional<StudentGroupEntity> findById(UUID studentGroupId);

    Optional<StudentGroupEntity> findByNameAndYearAndSubgroup(final String name, final String year, final Subgroup subgroup);
}
