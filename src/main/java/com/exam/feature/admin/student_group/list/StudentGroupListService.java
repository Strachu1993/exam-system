package com.exam.feature.admin.student_group.list;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentGroupListService {

    private final StudentGroupRepository studentGroupRepository;

    List<StudentGroupResponse> findAll() {
        final Sort sort = Sort.by(
                Sort.Order.desc("year"),
                Sort.Order.desc("name"),
                Sort.Order.desc("subgroup"));
        return studentGroupRepository.findAll(sort).stream()
                .map(StudentGroupResponse::of)
                .collect(Collectors.toList());
    }
}
