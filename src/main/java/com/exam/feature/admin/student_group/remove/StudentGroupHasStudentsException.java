package com.exam.feature.admin.student_group.remove;

import com.exam.commons.exception.MvcExceptionAbstract;
import org.springframework.http.HttpStatus;

import java.util.UUID;

class StudentGroupHasStudentsException extends MvcExceptionAbstract {

    StudentGroupHasStudentsException(final UUID studentGroupId, final int studentSize) {
        super("Student group with id '" + studentGroupId + "' has '" + studentSize + "' students.");
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    @Override
    public String getType() {
        return "student-group-has-student";
    }
}
