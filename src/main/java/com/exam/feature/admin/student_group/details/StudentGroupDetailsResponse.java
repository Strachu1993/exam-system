package com.exam.feature.admin.student_group.details;

import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class StudentGroupDetailsResponse {

    private UUID id;
    private String name;
    private String year;
    private Subgroup subgroup;
    private List<StudentDetailsResponse> students;

    @Builder
    @Getter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class StudentDetailsResponse {
        private UUID studentDetailsId;
        private UUID accountId;
        private boolean enabled;
        private String albumNumber;
        private String name;
        private String surname;
    }
}
