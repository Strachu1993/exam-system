package com.exam.feature.admin.student_group.create;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "${api.url.path.admin}/student-groups")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentGroupCreateEndpoint {

    private final StudentGroupCreateService studentGroupCreateService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void create(@RequestBody @Valid final StudentGroupCreateRequest request) {
        studentGroupCreateService.create(request);
    }
}
