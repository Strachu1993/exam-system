package com.exam.feature.admin.student_group.remove;

import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentGroupRemoveService {

    private final StudentGroupRepository studentGroupRepository;

    @Transactional
    public void remove(final UUID studentGroupId) {
        final StudentGroupEntity studentGroupEntity = findStudentGroupEntity(studentGroupId);
        studentGroupEntity.validateStudentSize();
        studentGroupRepository.deleteById(studentGroupId);
        log.info("Student group with id '{}' has been removed.", studentGroupId);
    }

    private StudentGroupEntity findStudentGroupEntity(final UUID studentGroupId) {
        return studentGroupRepository.findById(studentGroupId)
                .orElseThrow(() -> new StudentGroupNotFoundException(studentGroupId));
    }
}
