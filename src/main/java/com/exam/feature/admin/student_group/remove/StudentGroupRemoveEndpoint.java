package com.exam.feature.admin.student_group.remove;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/student-groups")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentGroupRemoveEndpoint {

    private final StudentGroupRemoveService studentGroupRemoveService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{groupId}")
    void remove(@PathVariable final UUID groupId) {
        studentGroupRemoveService.remove(groupId);
    }
}
