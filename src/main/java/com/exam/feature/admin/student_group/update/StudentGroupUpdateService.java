package com.exam.feature.admin.student_group.update;

import com.exam.commons.enums.Subgroup;
import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import com.exam.commons.exception.custom.validate.StudentGroupAlreadyExistException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentGroupUpdateService {

    private final StudentGroupRepository studentGroupRepository;

    @Transactional
    public void update(final UUID studentGroupId, final StudentGroupUpdateRequest studentGroupRequest) {
        final String newName = studentGroupRequest.getName();
        final String newYear = studentGroupRequest.getYear();
        final Subgroup newSubgroup = studentGroupRequest.getSubgroup();
        validateRequest(studentGroupId, newName, newYear, newSubgroup);

        final StudentGroupEntity studentGroupEntity = findStudentGroupEntity(studentGroupId);
        studentGroupEntity.setName(newName);
        studentGroupEntity.setYear(newYear);
        studentGroupEntity.setSubgroup(newSubgroup);

        studentGroupRepository.save(studentGroupEntity);
        log.info("Student group with id '{}' has been updated.", studentGroupId);
    }

    private void validateRequest(final UUID studentGroupId, final String name, final String year, final Subgroup subgroup) {
        final int alreadyExistStudentGroupDataCount = studentGroupRepository.countByIdNotAndNameAndYearAndSubgroup(studentGroupId, name, year, subgroup);
        if (alreadyExistStudentGroupDataCount > 0) {
            throw new StudentGroupAlreadyExistException(studentGroupId, name, year, subgroup);
        }
    }

    private StudentGroupEntity findStudentGroupEntity(final UUID studentGroupId) {
        return studentGroupRepository.findById(studentGroupId)
                .orElseThrow(() -> new StudentGroupNotFoundException(studentGroupId));
    }
}
