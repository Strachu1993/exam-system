package com.exam.feature.admin.student_group.update;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/student-groups")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class StudentGroupUpdateEndpoint {

    private final StudentGroupUpdateService studentGroupUpdateService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(value = "/{groupId}")
    void update(@PathVariable final UUID groupId, @RequestBody @Valid final StudentGroupUpdateRequest studentGroupRequest) {
        studentGroupUpdateService.update(groupId, studentGroupRequest);
    }
}
