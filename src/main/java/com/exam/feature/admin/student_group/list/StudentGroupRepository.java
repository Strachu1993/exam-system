package com.exam.feature.admin.student_group.list;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface StudentGroupRepository extends Repository<StudentGroupEntity, UUID> {

    List<StudentGroupEntity> findAll(Sort sort);
}
