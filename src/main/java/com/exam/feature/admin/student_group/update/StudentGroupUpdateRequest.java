package com.exam.feature.admin.student_group.update;

import com.exam.commons.Constraint;
import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Builder(access = AccessLevel.PACKAGE)
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@ToString
class StudentGroupUpdateRequest {

    @Size(min = Constraint.StudentGroup.MIN_NAME_LENGTH, max = Constraint.StudentGroup.MAX_NAME_LENGTH)
    private final String name;

    @Pattern(regexp = Constraint.StudentGroup.YEAR_REGEXP)
    private final String year;

    @NotNull
    private final Subgroup subgroup;
}
