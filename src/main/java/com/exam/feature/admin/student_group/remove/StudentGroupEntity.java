package com.exam.feature.admin.student_group.remove;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Set;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@EqualsAndHashCode(exclude = "studentDetails")
@ToString(exclude = "studentDetails")
@Entity
@Table(name = "student_group", uniqueConstraints = {
        @UniqueConstraint(name = "unique_group", columnNames = {"name", "year", "subgroup"})
})
class StudentGroupEntity {

    @Id
    private UUID id;

    @JoinColumn(name = "fk_group", insertable = false, updatable = false)
    @OneToMany
    private Set<StudentDetailsEntity> studentDetails;

    void validateStudentSize() {
        if (!studentDetails.isEmpty()) {
            throw new StudentGroupHasStudentsException(id, studentDetails.size());
        }
    }
}
