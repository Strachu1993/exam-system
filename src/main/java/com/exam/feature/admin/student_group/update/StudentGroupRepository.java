package com.exam.feature.admin.student_group.update;

import com.exam.commons.enums.Subgroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface StudentGroupRepository extends JpaRepository<StudentGroupEntity, UUID> {

    int countByIdNotAndNameAndYearAndSubgroup(UUID id, String name, String year, Subgroup subgroup);
}
