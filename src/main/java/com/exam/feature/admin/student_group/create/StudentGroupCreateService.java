package com.exam.feature.admin.student_group.create;

import com.exam.commons.enums.Subgroup;
import com.exam.commons.exception.custom.validate.StudentGroupAlreadyExistException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class StudentGroupCreateService {

    private final StudentGroupRepository studentGroupRepository;

    @Transactional
    public void create(final StudentGroupCreateRequest request) {
        final String newName = request.getName();
        final String newYear = request.getYear();
        final Subgroup newSubgroup = request.getSubgroup();
        validateRequest(newName, newYear, newSubgroup);

        final StudentGroupEntity studentGroupEntity = prepareStudentGroupEntity(newName, newYear, newSubgroup);
        final StudentGroupEntity savedStudentGroupEntity = studentGroupRepository.save(studentGroupEntity);
        log.info("Student group with data 'id = {}', 'name = {}', 'year = {}', 'subgroup = {}' has been added.", savedStudentGroupEntity.getId(), newName, newYear, newSubgroup);
    }

    private void validateRequest(final String name, final String year, final Subgroup subgroup) {
        final int alreadyExistStudentGroupDataCount = studentGroupRepository.countByNameAndYearAndSubgroup(name, year, subgroup);
        if (alreadyExistStudentGroupDataCount > 0) {
            throw new StudentGroupAlreadyExistException(name, year, subgroup);
        }
    }

    private static StudentGroupEntity prepareStudentGroupEntity(final String newName, final String newYear, final Subgroup newSubgroup) {
        return StudentGroupEntity.builder()
                .name(newName)
                .year(newYear)
                .subgroup(newSubgroup)
                .build();
    }
}
