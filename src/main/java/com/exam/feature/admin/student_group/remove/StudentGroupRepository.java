package com.exam.feature.admin.student_group.remove;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface StudentGroupRepository extends JpaRepository<StudentGroupEntity, UUID> {
}
