package com.exam.feature.admin.student_group.details;

import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@EqualsAndHashCode(exclude = "studentDetails")
@ToString(exclude = "studentDetails")
@Entity
@Table(name = "student_group")
class StudentGroupEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String name;

    @Column(insertable = false, updatable = false)
    private String year;

    @Enumerated(EnumType.STRING)
    @Column(insertable = false, updatable = false)
    private Subgroup subgroup;

    @JoinColumn(name = "fk_group", insertable = false, updatable = false)
    @OneToMany
    private List<StudentDetailsEntity> studentDetails;

    StudentGroupDetailsResponse toStudentGroupDetailsResponse() {
        final List<StudentGroupDetailsResponse.StudentDetailsResponse> students = studentDetails.stream()
                .map(StudentDetailsEntity::toStudentDetailsResponse)
                .collect(Collectors.toList());

        return StudentGroupDetailsResponse.builder()
                .id(id)
                .name(name)
                .year(year)
                .subgroup(subgroup)
                .students(students)
                .build();
    }
}
