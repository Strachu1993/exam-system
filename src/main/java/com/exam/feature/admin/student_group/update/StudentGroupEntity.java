package com.exam.feature.admin.student_group.update;

import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "student_group", uniqueConstraints = {
        @UniqueConstraint(name = "unique_group", columnNames = {"name", "year", "subgroup"})
})
class StudentGroupEntity {

    @Id
    @Setter(value = AccessLevel.NONE)
    private UUID id;

    @Column
    private String name;

    @Column
    private String year;

    @Enumerated(EnumType.STRING)
    @Column
    private Subgroup subgroup;
}
