package com.exam.feature.admin.student_group.details;

import com.exam.commons.enums.Subgroup;

import java.util.UUID;

public interface StudentGroupDetailsFacade {

    UUID getStudentGroupDetails(final String name, final String year, final Subgroup subgroup);
}
