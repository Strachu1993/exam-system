package com.exam.feature.admin.exam.details;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/exam")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class ExamDetailsEndpoint {

    private final ExamDetailsService examService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{examId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ExamDetailsResponse getExamDetails(@PathVariable final UUID examId) {
        return examService.getExamDetails(examId);
    }
}
