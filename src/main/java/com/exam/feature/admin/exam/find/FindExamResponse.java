package com.exam.feature.admin.exam.find;

import com.exam.commons.enums.ExamStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class FindExamResponse {

    private UUID examId;
    private LocalDateTime start;
    private LocalDateTime end;
    private ExamStatus examStatus;
    private int questionSize;
    private long studentGroupsSize;
    private String description;

    static FindExamResponse of(final ExamEntity exam) {
        final int questionSize = exam.getQuestions().size();
        final long studentGroupsSize = exam.getStudentScores().stream()
                .map(StudentScoreEntity::getStudentDetails)
                .map(StudentDetailsEntity::getStudentGroup)
                .distinct().count();

        return FindExamResponse.builder()
                .examId(exam.getId())
                .start(exam.getStart())
                .end(exam.getEnd())
                .examStatus(exam.getStatus())
                .questionSize(questionSize)
                .studentGroupsSize(studentGroupsSize)
                .description(exam.getDescription())
                .build();
    }
}
