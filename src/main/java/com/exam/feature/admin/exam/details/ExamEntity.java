package com.exam.feature.admin.exam.details;

import com.exam.commons.enums.ExamStatus;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "questions")
@ToString(exclude = "questions")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private LocalDateTime start;

    @Column(insertable = false, updatable = false)
    private LocalDateTime end;

    @Enumerated(EnumType.STRING)
    @Column(insertable = false, updatable = false)
    private ExamStatus status;

    @JoinColumn(name = "fk_exam")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<StudentScoreEntity> studentScores;

    @Column(insertable = false, updatable = false)
    private String description;

    @ManyToMany
    @JoinTable(name = "rel_question_exam",
            joinColumns = @JoinColumn(name = "fk_exam"),
            inverseJoinColumns = @JoinColumn(name = "fk_question"))
    private List<QuestionEntity> questions;
}
