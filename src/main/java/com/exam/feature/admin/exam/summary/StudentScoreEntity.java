package com.exam.feature.admin.exam.summary;

import com.exam.commons.enums.Rating;
import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode(of = "id")
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private Integer score;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private Rating rating;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_exam")
    @ManyToOne
    private ExamEntity exam;

    @JoinColumn(name = "fk_student_details")
    @ManyToOne
    private StudentDetailsEntity studentDetails;

    ExamSummaryResponse.ScoreResponse toScoreResponse() {
        return ExamSummaryResponse.ScoreResponse.builder()
                .id(id)
                .scoreStatus(status)
                .rating(rating)
                .score(score)
                .student(studentDetails.toStudentResponse())
                .build();
    }
}
