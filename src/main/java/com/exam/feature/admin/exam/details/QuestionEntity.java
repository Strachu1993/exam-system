package com.exam.feature.admin.exam.details;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "exams")
@ToString(exclude = "exams")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "question")
class QuestionEntity {

    @Id
    private UUID id;

    @JoinColumn(name = "fk_subject", insertable = false, updatable = false)
    @ManyToOne
    private SubjectEntity subject;

    @ManyToMany(mappedBy = "questions")
    private List<ExamEntity> exams;
}
