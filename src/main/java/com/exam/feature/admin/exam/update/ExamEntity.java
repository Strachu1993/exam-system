package com.exam.feature.admin.exam.update;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.exception.custom.validate.ExamModificationException;
import com.exam.commons.exception.custom.validate.QuestionsFromSubjectAlreadyExistInExamException;
import com.exam.commons.exception.custom.validate.StudentGroupAlreadyExistInExamException;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = {"questions", "studentScores"})
@ToString(exclude = {"questions", "studentScores"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    private UUID id;

    @Column(insertable = false)
    @Enumerated(EnumType.STRING)
    private ExamStatus status;

    @Column(insertable = false)
    private LocalDateTime start;

    @Column(insertable = false)
    private LocalDateTime end;

    @Column(insertable = false)
    private String description;

    @OneToMany(mappedBy = "exam", cascade = CascadeType.PERSIST)
    private List<StudentScoreEntity> studentScores;

    @ManyToMany
    @JoinTable(name = "rel_question_exam",
            joinColumns = @JoinColumn(name = "fk_exam"),
            inverseJoinColumns = @JoinColumn(name = "fk_question"))
    private List<QuestionEntity> questions;

    void updateExam(final UpdateExamRequest updateExamRequest) {
        this.start = updateExamRequest.getStart();
        this.end = updateExamRequest.getEnd();
        this.description = updateExamRequest.getDescription();
    }

    void addStudentScore(final StudentScoreEntity studentScore) {
        studentScores.add(studentScore);
    }

    void addQuestions(final List<QuestionEntity> questions) {
        this.questions.addAll(questions);
    }

    void validateExamStatusForModification() {
        if (status != ExamStatus.CREATED) {
            throw new ExamModificationException(id, status);
        }
    }

    void validateStudentsFromGroupAlreadyExist(final UUID groupId) {
        final boolean hasStudents = studentScores.stream()
                .map(StudentScoreEntity::getStudentDetails)
                .map(StudentDetailsEntity::getStudentGroup)
                .map(StudentGroupEntity::getId)
                .anyMatch(x -> x.equals(groupId));

        if (hasStudents) {
            throw new StudentGroupAlreadyExistInExamException(id, groupId);
        }
    }

    void validateQuestionsFromSubjectAlreadyExist(final String subjectName) {
        final boolean hasQuestions = questions.stream()
                .map(QuestionEntity::getSubject)
                .map(SubjectEntity::getName)
                .anyMatch(x -> x.equals(subjectName));

        if (hasQuestions) {
            throw new QuestionsFromSubjectAlreadyExistInExamException(id, subjectName);
        }
    }
}


