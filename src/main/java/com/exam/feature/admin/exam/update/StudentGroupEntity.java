package com.exam.feature.admin.exam.update;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "studentDetails")
@ToString(exclude = "studentDetails")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_group")
class StudentGroupEntity {

    @Id
    private UUID id;

    @OneToMany(mappedBy = "studentGroup", fetch = FetchType.EAGER)
    private List<StudentDetailsEntity> studentDetails;
}

