package com.exam.feature.admin.exam.remove;

import com.exam.commons.exception.custom.not_found.ExamNotFound;
import com.exam.commons.exception.custom.validate.LeastOneRequiredException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class RemoveExamService {

    private final ExamRepository examRepository;

    @Transactional
    public void removeExam(final UUID examId) {
        final ExamEntity exam = findExamById(examId);
        exam.validateExamStatusForDelete();
        examRepository.deleteById(examId);
        log.info("Exam with id '{}' has been removed.", examId);
    }

    @Transactional
    public void removeGroupFromExam(final UUID examId, final UUID groupId) {
        final ExamEntity exam = findExamById(examId);
        exam.validateExamStatusForModification();
        removeStudentGroupFromExam(groupId, exam);
        validateLeastOneRequired(exam.getStudentScores().size(), "students group", examId);
        log.info("Students from group '{}' were removed from the exam with id '{}'", groupId, examId);
    }

    @Transactional
    public void removeQuestionsBySubjectFromExam(final UUID examId, final UUID subjectId) {
        final ExamEntity exam = findExamById(examId);
        exam.validateExamStatusForModification();
        final List<QuestionEntity> questions = findQuestionsBySubjectId(subjectId, exam);
        exam.removeQuestions(questions);
        validateLeastOneRequired(exam.getQuestions().size(), "subject", examId);
        log.info("Questions from subject '{}' were removed from the exam with id '{}'", subjectId, examId);
    }

    private void removeStudentGroupFromExam(final UUID groupId, final ExamEntity exam) {
        final List<StudentScoreEntity> studentScores = exam.getStudentScores().stream()
                .filter(x -> x.getStudentDetails().getStudentGroup().getId().equals(groupId))
                .collect(Collectors.toList());
        exam.removeStudentScores(studentScores);
    }

    private void validateLeastOneRequired(final int size, final String type, final UUID examId) {
        if(size <= 0) {
            throw new LeastOneRequiredException(type, examId);
        }
    }

    private List<QuestionEntity> findQuestionsBySubjectId(final UUID subjectId, final ExamEntity exam) {
        return exam.getQuestions().stream()
                    .filter(x -> x.getSubject().getId().equals(subjectId))
                    .collect(Collectors.toList());
    }

    private ExamEntity findExamById(final UUID examId) {
        return examRepository.findById(examId).orElseThrow(() -> new ExamNotFound(examId));
    }
}
