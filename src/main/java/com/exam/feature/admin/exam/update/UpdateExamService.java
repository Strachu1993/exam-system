package com.exam.feature.admin.exam.update;

import com.exam.commons.exception.custom.not_found.ExamNotFound;
import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import com.exam.commons.exception.custom.not_found.SubjectNotFoundException;
import com.exam.commons.exception.custom.validate.NotEnoughQuestionsException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class UpdateExamService {

    private final ExamRepository examRepository;
    private final StudentGroupReadRepository studentGroupRepository;
    private final SubjectReadRepository subjectRepository;
    private final Random random;

    @Transactional
    public void updateExam(final UUID examId, final UpdateExamRequest updateExamRequest) {
        final ExamEntity exam = findExamById(examId);
        exam.validateExamStatusForModification();
        exam.updateExam(updateExamRequest);
        log.info("Exam with id '{}' has been updated.", examId);
    }

    @Transactional
    public void addStudentsToExamByGroup(final UUID examId, final UUID groupId) {
        final ExamEntity exam = findExamById(examId);
        exam.validateExamStatusForModification();
        exam.validateStudentsFromGroupAlreadyExist(groupId);
        updateExamStudentByGroup(exam, groupId);
        log.info("Exam with id '{}' has been updated by student from groups with id '{}'.", examId, groupId);
    }

    @Transactional
    public void addQuestionsToExamBySubject(final UUID examId, final UpdateExamQuestionsRequest updateExamRequest) {
        final ExamEntity exam = findExamById(examId);
        exam.validateExamStatusForModification();
        final String subjectName = updateExamRequest.getSubjectName();
        exam.validateQuestionsFromSubjectAlreadyExist(subjectName);
        updateExamQuestionBySubject(exam, updateExamRequest);
        log.info("Exam with id '{}' has been updated by questions from subject with name '{}'.", examId, subjectName);
    }

    private void updateExamQuestionBySubject(final ExamEntity exam, final UpdateExamQuestionsRequest updateExamRequest) {
        final String subjectName = updateExamRequest.getSubjectName();
        final int numberOfQuestions = updateExamRequest.getNumberOfQuestions();
        final List<QuestionEntity> questions = findSubjectByName(subjectName).getQuestions();
        checkRequiredQuestionSize(subjectName, numberOfQuestions, questions.size());
        selectQuestions(numberOfQuestions, questions);
        exam.addQuestions(questions);
    }

    private void selectQuestions(int numberOfQuestions, List<QuestionEntity> questions) {
        while (questions.size() > numberOfQuestions) {
            final int position = random.nextInt(questions.size());
            questions.remove(position);
        }
    }

    private void checkRequiredQuestionSize(final String subjectName, final int requiredNumberOfQuestions, final int questionSize) {
        if (questionSize < requiredNumberOfQuestions) {
            throw new NotEnoughQuestionsException(subjectName, requiredNumberOfQuestions, questionSize);
        }
    }

    private void updateExamStudentByGroup(final ExamEntity exam, final UUID groupId) {
        final List<StudentDetailsEntity> students = findStudentByGroupId(groupId).getStudentDetails();
        for (final StudentDetailsEntity student: students) {
            final StudentScoreEntity studentScore = createStudentScore(exam, student);
            exam.addStudentScore(studentScore);
        }
    }

    private StudentScoreEntity createStudentScore(final ExamEntity exam, final StudentDetailsEntity student) {
        return StudentScoreEntity.builder()
                .studentDetails(student)
                .exam(exam)
                .build();
    }

    private ExamEntity findExamById(final UUID examId) {
        return examRepository.findById(examId).orElseThrow(() -> new ExamNotFound(examId));
    }

    private StudentGroupEntity findStudentByGroupId(final UUID groupId) {
        return studentGroupRepository.findById(groupId).orElseThrow(() -> new StudentGroupNotFoundException(groupId));
    }

    private SubjectEntity findSubjectByName(final String subjectName) {
        return subjectRepository.findByName(subjectName).orElseThrow(() -> new SubjectNotFoundException(subjectName));
    }
}
