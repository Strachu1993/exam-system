package com.exam.feature.admin.exam.update;

import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface SubjectReadRepository extends Repository<SubjectEntity, UUID> {

    Optional<SubjectEntity> findByName(String subjectName);
}
