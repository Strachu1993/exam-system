package com.exam.feature.admin.exam.summary;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.enums.Rating;
import com.exam.commons.enums.StudentScoreStatus;
import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class ExamSummaryResponse {

    private UUID id;
    private LocalDateTime start;
    private LocalDateTime end;
    private String description;
    private ExamStatus examStatus;
    private List<ScoreResponse> scores;

    @Builder
    @Getter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class ScoreResponse {
        private UUID id;
        private StudentScoreStatus scoreStatus;
        private Integer score;
        private Rating rating;
        private StudentResponse student;
    }

    @Builder
    @Getter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class StudentResponse {
        private String numberAlbum;
        private String name;
        private String surname;
        private GroupResponse group;
    }

    @Builder
    @Getter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class GroupResponse {
        private UUID id;
        private String name;
        private Subgroup subgroup;
        private String year;
    }
}
