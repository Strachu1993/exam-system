package com.exam.feature.admin.exam.summary;

import com.exam.commons.enums.ExamStatus;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@EqualsAndHashCode(exclude = "studentScores")
@ToString(exclude = "studentScores")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private LocalDateTime start;

    @Column(insertable = false, updatable = false)
    private LocalDateTime end;

    @Column(insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private ExamStatus status;

    @Column(insertable = false, updatable = false)
    private String description;

    @OneToMany(mappedBy = "exam")
    private List<StudentScoreEntity> studentScores;

    ExamSummaryResponse toExamSummaryResponse() {
        final List<ExamSummaryResponse.ScoreResponse> scoreResponses = studentScores.stream()
                .map(StudentScoreEntity::toScoreResponse)
                .collect(Collectors.toList());

        return ExamSummaryResponse.builder()
                .id(id)
                .start(start)
                .end(end)
                .description(description)
                .scores(scoreResponses)
                .examStatus(status)
                .build();
    }
}
