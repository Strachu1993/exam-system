package com.exam.feature.admin.exam.summary;

import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface ExamReadRepository extends Repository<ExamEntity, UUID> {

    Optional<ExamEntity> findById(UUID studentScoreId);
}
