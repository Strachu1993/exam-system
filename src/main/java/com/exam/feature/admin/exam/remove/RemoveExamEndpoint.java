package com.exam.feature.admin.exam.remove;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/exam/delete")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class RemoveExamEndpoint {

    private final RemoveExamService removeExamService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/{examId}")
    void removeExam(@PathVariable final UUID examId) {
        removeExamService.removeExam(examId);
    }


    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(value = "/{examId}/group/{groupId}")
    void removeGroupFromExam(@PathVariable final UUID examId, @PathVariable final UUID groupId) {
        removeExamService.removeGroupFromExam(examId, groupId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(value = "/{examId}/subject/{subjectId}")
    void removeQuestionsBySubjectFromExam(@PathVariable final UUID examId, @PathVariable final UUID subjectId) {
        removeExamService.removeQuestionsBySubjectFromExam(examId, subjectId);
    }
}
