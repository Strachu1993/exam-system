package com.exam.feature.admin.exam.create;

import com.exam.commons.enums.Subgroup;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "studentDetails")
@ToString(exclude = "studentDetails")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_group")
class StudentGroupEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String name;

    @Column(insertable = false, updatable = false)
    private String year;

    @Enumerated(EnumType.STRING)
    @Column(insertable = false, updatable = false)
    private Subgroup subgroup;

    @JoinColumn(name = "fk_group")
    @OneToMany(fetch = FetchType.EAGER)
    private List<StudentDetailsEntity> studentDetails;
}

