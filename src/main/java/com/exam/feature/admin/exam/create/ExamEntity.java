package com.exam.feature.admin.exam.create;

import com.exam.commons.enums.ExamStatus;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "questions")
@ToString(exclude = "questions")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    @GeneratedValue
    @Setter(value = AccessLevel.NONE)
    private UUID id;

    @Column
    private LocalDateTime start;

    @Column
    private LocalDateTime end;

    @Enumerated(EnumType.STRING)
    @Column
    private ExamStatus status;

    @Column
    private String description;

    @JoinColumn(name = "fk_exam")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<StudentScoreEntity> studentScores;

    @ManyToMany
    @JoinTable(name = "rel_question_exam",
            joinColumns = @JoinColumn(name = "fk_exam"),
            inverseJoinColumns = @JoinColumn(name = "fk_question"))
    private List<QuestionEntity> questions;

    @Builder
    ExamEntity(final LocalDateTime start, final LocalDateTime end, final List<QuestionEntity> questions, final String description) {
        this.start = start;
        this.end = end;
        this.questions = questions;
        this.status = ExamStatus.CREATED;
        this.description = description;
        studentScores = new ArrayList<>();
    }

    void addStudentScore(final StudentScoreEntity studentScore) {
        studentScores.add(studentScore);
    }
}


