package com.exam.feature.admin.exam.summary;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/exam")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class ExamSummaryEndpoint {

    private final ExamSummaryService examSummaryService;

    @GetMapping(value = "/{examId}/summary", produces = MediaType.APPLICATION_JSON_VALUE)
    ExamSummaryResponse getExamSummary(@PathVariable final UUID examId) {
        return examSummaryService.getExamSummary(examId);
    }
}
