package com.exam.feature.admin.exam.create;

import com.exam.commons.exception.custom.validate.NotEnoughQuestionsException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class CreateExamService {

    private final StudentGroupReadRepository studentGroupRepository;
    private final QuestionReadRepository questionRepository;
    private final ExamRepository examRepository;
    private final Random random;

    CreateExamDataResponse getCreateExamData() {
        return CreateExamDataResponse.builder()
                .studentGroupsData(getSubjects())
                .questionData(getSummaryQuestionData())
                .build();
    }

    @Transactional
    public void create(final CreateExamRequest createExamRequest) {
        final List<CreateExamRequest.Question> questionsData = createExamRequest.getQuestionData();
        final Map<String, List<QuestionEntity>> questions = findQuestions(questionsData);
        final List<QuestionEntity> selectedQuestions = selectQuestions(questionsData, questions);
        final List<UUID> studentGroupIds = createExamRequest.getStudentGroupIds();
        final List<StudentDetailsEntity> students = studentGroupRepository.findByIdIn(studentGroupIds).stream()
                .map(StudentGroupEntity::getStudentDetails)
                .flatMap(List::stream)
                .collect(Collectors.toList());

        final ExamEntity exam = createExam(createExamRequest, selectedQuestions);
        prepareStudentScore(students, exam);
        examRepository.save(exam);
        log.info("Exam has been created with id '{}'.", exam.getId());
    }

    private void prepareStudentScore(final List<StudentDetailsEntity> students, final ExamEntity exam) {
        for (final StudentDetailsEntity student: students) {
            final StudentScoreEntity studentScore = createStudentScore(exam, student);
            exam.addStudentScore(studentScore);
        }
    }

    private List<QuestionEntity> selectQuestions(final List<CreateExamRequest.Question> questionsData, final Map<String, List<QuestionEntity>> questions) {
        final List<QuestionEntity> result = new ArrayList<>();

        for (final CreateExamRequest.Question data : questionsData) {
            final String subjectName = data.getSubjectName();
            final Integer numberOfQuestions = data.getNumberOfQuestions();
            final List<QuestionEntity> questionEntities = questions.get(subjectName);

            while (questionEntities.size() > numberOfQuestions) {
                final int position = random.nextInt(questionEntities.size());
                questionEntities.remove(position);
            }

            result.addAll(questionEntities);
        }

        return result;
    }

    private Map<String, List<QuestionEntity>> findQuestions(final List<CreateExamRequest.Question> questionsData) {
        final Map<String, List<QuestionEntity>> result = new HashMap<>();
        for (final CreateExamRequest.Question questionData : questionsData) {
            final String subjectName = questionData.getSubjectName();
            final int requiredNumberOfQuestions = questionData.getNumberOfQuestions();
            final List<QuestionEntity> questions = questionRepository.findBySubjectName(subjectName);
            final int questionSize = questions.size();
            checkRequiredQuestionSize(subjectName, requiredNumberOfQuestions, questionSize);
            result.put(questionData.getSubjectName(), questions);
        }

        return result;
    }

    private void checkRequiredQuestionSize(final String subjectName, final int requiredNumberOfQuestions, final int questionSize) {
        if (questionSize < requiredNumberOfQuestions) {
            throw new NotEnoughQuestionsException(subjectName, requiredNumberOfQuestions, questionSize);
        }
    }

    private List<CreateExamDataResponse.QuestionData> getSummaryQuestionData() {
        return questionRepository.findCreateExamQuestionData().stream()
                .map(CreateExamDataResponse.QuestionData::of)
                .collect(Collectors.toList());
    }

    private List<CreateExamDataResponse.StudentGroupData> getSubjects() {
        final Sort sort = Sort.by(
                Sort.Order.desc("year"),
                Sort.Order.desc("name"),
                Sort.Order.desc("subgroup"));
        return studentGroupRepository.findAll(sort).stream()
                .map(CreateExamDataResponse.StudentGroupData::of)
                .collect(Collectors.toList());
    }

    private static StudentScoreEntity createStudentScore(final ExamEntity exam, final StudentDetailsEntity student) {
        return StudentScoreEntity.builder()
                .studentDetails(student)
                .exam(exam)
                .build();
    }

    private static ExamEntity createExam(final CreateExamRequest createExamRequest, final List<QuestionEntity> questions) {
        return ExamEntity.builder()
                .start(createExamRequest.getStart())
                .end(createExamRequest.getEnd())
                .questions(questions)
                .description(createExamRequest.getDescription())
                .build();
    }
}
