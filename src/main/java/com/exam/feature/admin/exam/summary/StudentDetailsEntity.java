package com.exam.feature.admin.exam.summary;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_details")
class StudentDetailsEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String albumNumber;

    @Column(insertable = false, updatable = false)
    private String name;

    @Column(insertable = false, updatable = false)
    private String surname;

    @JoinColumn(name = "fk_group")
    @ManyToOne
    private StudentGroupEntity studentGroup;

    ExamSummaryResponse.StudentResponse toStudentResponse() {
        return ExamSummaryResponse.StudentResponse.builder()
                .numberAlbum(albumNumber)
                .name(name)
                .surname(surname)
                .group(studentGroup.toGroupResponse())
                .build();
    }
}
