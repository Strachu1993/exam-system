package com.exam.feature.admin.exam.create;


import org.springframework.data.domain.Sort;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface StudentGroupReadRepository extends Repository<StudentGroupEntity, UUID> {

    List<StudentGroupEntity> findAll(Sort sort);

    List<StudentGroupEntity> findByIdIn(List<UUID> studentGroupIds);
}
