package com.exam.feature.admin.exam.find;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "${api.url.path.admin}/exam")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class FindExamEndpoint {

    private final FindExamService examService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<FindExamResponse> findExam() {
        return examService.findExam();
    }
}
