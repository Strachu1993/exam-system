package com.exam.feature.admin.exam.details;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.exception.custom.not_found.ExamNotFound;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class ExamDetailsService {

    private final ExamReadRepository examRepository;

    ExamDetailsResponse getExamDetails(final UUID examId) {
        final ExamEntity exam = findExamById(examId);

        final UUID id = exam.getId();
        final LocalDateTime start = exam.getStart();
        final LocalDateTime end = exam.getEnd();
        final ExamStatus status = exam.getStatus();
        final String description = exam.getDescription();
        final Set<ExamDetailsResponse.GroupResponse> groupsResponse = findExamStudentGroups(exam);
        final Set<ExamDetailsResponse.SubjectResponse> subjectsResponse = findExamSubjects(exam.getQuestions());

        return ExamDetailsResponse.builder()
                .examId(id)
                .start(start)
                .end(end)
                .status(status)
                .description(description)
                .groups(groupsResponse)
                .subjects(subjectsResponse)
                .build();
    }

    private Set<ExamDetailsResponse.SubjectResponse> findExamSubjects(final List<QuestionEntity> questions) {
        return questions.stream().collect(groupingBy(QuestionEntity::getSubject, counting())).entrySet().stream()
                .map((entrySet) -> ExamDetailsResponse.SubjectResponse.of(entrySet.getKey(), entrySet.getValue()))
                .collect(Collectors.toSet());
    }

    private Set<ExamDetailsResponse.GroupResponse> findExamStudentGroups(final ExamEntity exam) {
        return exam.getStudentScores().stream()
                .map(StudentScoreEntity::getStudentDetails)
                .map(StudentDetailsEntity::getStudentGroup)
                .map(ExamDetailsResponse.GroupResponse::of)
                .collect(Collectors.toSet());
    }

    private ExamEntity findExamById(final UUID examId) {
        return examRepository.findById(examId).orElseThrow(() -> new ExamNotFound(examId));
    }
}
