package com.exam.feature.admin.exam.find;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface ExamReadRepository extends Repository<ExamEntity, UUID> {

    List<ExamEntity> findAll(Sort sort);
}
