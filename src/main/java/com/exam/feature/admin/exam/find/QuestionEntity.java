package com.exam.feature.admin.exam.find;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "exams")
@ToString(exclude = "exams")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "question")
class QuestionEntity {

    @Id
    private UUID id;

    @JoinColumn(name = "fk_subject", insertable = false, updatable = false)
    @ManyToOne
    private SubjectEntity subject;

    @ManyToMany(mappedBy = "questions")
    private List<ExamEntity> exams;
}
