package com.exam.feature.admin.exam.create;

import com.exam.commons.Constraint;
import com.exam.commons.validator.annotation.ExpressionAssert;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@ExpressionAssert(value = "start.isBefore(end)")
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class CreateExamRequest {

    @FutureOrPresent
    private LocalDateTime start;

    @Future
    private LocalDateTime end;

    @Size(min = 1)
    private List<UUID> studentGroupIds;

    @Valid
    @Size(min = 1)
    private List<Question> questionData;

    @Size(max = Constraint.Exam.MAX_DESCRIPTION_LENGTH)
    private String description;

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class Question {

        @NotBlank
        private String subjectName;

        @Min(1)
        private Integer numberOfQuestions;
    }
}
