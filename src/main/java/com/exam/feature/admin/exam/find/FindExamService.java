package com.exam.feature.admin.exam.find;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class FindExamService {

    private final ExamReadRepository examRepository;

    List<FindExamResponse> findExam() {
        final Sort sort = Sort.by(
                Sort.Order.desc("status"),
                Sort.Order.desc("start"));
        return examRepository.findAll(sort).stream()
                .map(FindExamResponse::of)
                .collect(Collectors.toList());
    }
}
