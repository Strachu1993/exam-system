package com.exam.feature.admin.exam.find;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "subject")
class SubjectEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String name;
}