package com.exam.feature.admin.exam.create;

import com.exam.commons.converter.UuidByteConverter;
import com.exam.commons.enums.Subgroup;
import lombok.*;

import java.util.List;
import java.util.UUID;

@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class CreateExamDataResponse {

    private List<StudentGroupData> studentGroupsData;
    private List<QuestionData> questionData;

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class StudentGroupData {

        private UUID id;
        private String name;
        private String year;
        private Subgroup subgroup;

        static StudentGroupData of(final StudentGroupEntity group) {
            return StudentGroupData.builder()
                    .id(group.getId())
                    .name(group.getName())
                    .year(group.getYear())
                    .subgroup(group.getSubgroup())
                    .build();
        }
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class QuestionData {

        private UUID subjectId;
        private String name;
        private int numberOfQuestions;

        static QuestionData of(final QuestionReadRepository.CreateExamQuestionData createExamQuestionData) {
            final UUID questionId = UuidByteConverter.convertToUuid(createExamQuestionData.getId());
            return QuestionData.builder()
                    .subjectId(questionId)
                    .name(createExamQuestionData.getName())
                    .numberOfQuestions(createExamQuestionData.getNumberOfQuestions())
                    .build();
        }
    }
}
