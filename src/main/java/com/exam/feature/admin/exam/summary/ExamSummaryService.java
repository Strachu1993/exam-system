package com.exam.feature.admin.exam.summary;

import com.exam.commons.exception.custom.not_found.ExamNotFound;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class ExamSummaryService {

    private final ExamReadRepository examReadRepository;

    ExamSummaryResponse getExamSummary(final UUID examId) {
        return findExamById(examId).toExamSummaryResponse();
    }

    private ExamEntity findExamById(final UUID examId) {
        return examReadRepository.findById(examId).orElseThrow(() -> new ExamNotFound(examId));
    }
}
