package com.exam.feature.admin.exam.create;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface QuestionReadRepository extends Repository<QuestionEntity, UUID> {

    @Query(value = "SELECT s.id, s.name, COUNT(*) AS numberOfQuestions FROM subject AS s INNER JOIN question AS q ON q.fk_subject = s.id GROUP BY s.name, s.id", nativeQuery = true)
    List<CreateExamQuestionData> findCreateExamQuestionData();

    List<QuestionEntity> findBySubjectName(String subjectName);

    interface CreateExamQuestionData {

        byte[] getId();
        int getNumberOfQuestions();
        String getName();
    }
}
