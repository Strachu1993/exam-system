package com.exam.feature.admin.exam.create;

import com.exam.commons.enums.StudentScoreStatus;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.util.UUID;

@EqualsAndHashCode(exclude = {"studentDetails", "exam"})
@ToString(exclude = {"studentDetails", "exam"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_score")
class StudentScoreEntity {

    @Id
    @GeneratedValue
    @Setter(value = AccessLevel.NONE)
    private UUID id;

    @Enumerated(EnumType.STRING)
    private StudentScoreStatus status;

    @JoinColumn(name = "fk_exam")
    @ManyToOne
    private ExamEntity exam;

    @JoinColumn(name = "fk_student_details")
    @ManyToOne
    private StudentDetailsEntity studentDetails;

    @Builder
    StudentScoreEntity(final StudentDetailsEntity studentDetails, final ExamEntity exam) {
        this.studentDetails = studentDetails;
        this.exam = exam;
    }

    @PrePersist
    private void prePersist() {
        status = StudentScoreStatus.CREATED;
    }
}
