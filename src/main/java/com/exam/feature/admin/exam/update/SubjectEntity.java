package com.exam.feature.admin.exam.update;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = "questions")
@ToString(exclude = "questions")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "subject")
class SubjectEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String name;

    @OneToMany(mappedBy = "subject")
    private List<QuestionEntity> questions;
}