package com.exam.feature.admin.exam.update;

import com.exam.commons.Constraint;
import com.exam.commons.validator.annotation.ExpressionAssert;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@ExpressionAssert(value = "start.isBefore(end)")
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class UpdateExamRequest {

    @FutureOrPresent
    private LocalDateTime start;

    @Future
    private LocalDateTime end;

    @Size(max = Constraint.Exam.MAX_DESCRIPTION_LENGTH)
    private String description;
}
