package com.exam.feature.admin.exam.update;


import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface StudentGroupReadRepository extends Repository<StudentGroupEntity, UUID> {

    Optional<StudentGroupEntity> findById(UUID groupId);
}
