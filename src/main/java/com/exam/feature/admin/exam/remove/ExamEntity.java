package com.exam.feature.admin.exam.remove;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.exception.custom.validate.ExamDeleteException;
import com.exam.commons.exception.custom.validate.ExamModificationException;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(exclude = { "questions", "studentScores"})
@ToString(exclude = { "questions", "studentScores"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "exam")
class ExamEntity {

    @Id
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column
    private ExamStatus status;

    @OneToMany(mappedBy = "exam", cascade = { CascadeType.REMOVE, CascadeType.PERSIST }, orphanRemoval = true)
    private List<StudentScoreEntity> studentScores;

    @ManyToMany
    @JoinTable(name = "rel_question_exam",
            joinColumns = @JoinColumn(name = "fk_exam"),
            inverseJoinColumns = @JoinColumn(name = "fk_question"))
    private List<QuestionEntity> questions;

    void validateExamStatusForDelete() {
        if(status != ExamStatus.CREATED) {
            throw new ExamDeleteException(id, status);
        }
    }

    void validateExamStatusForModification() {
        if (status != ExamStatus.CREATED) {
            throw new ExamModificationException(id, status);
        }
    }

    void removeStudentScores(final List<StudentScoreEntity> studentScores) {
        this.studentScores.removeAll(studentScores);
    }

    void removeQuestions(final List<QuestionEntity> questions) {
        this.questions.removeAll(questions);
    }

    @PreRemove
    private void preRemove() {
        for (final QuestionEntity question: questions) {
            question.getExams().remove(this);
        }
    }
}
