package com.exam.feature.admin.exam.summary;

import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@Entity
@Table(name = "student_group")
class StudentGroupEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(insertable = false, updatable = false)
    private Subgroup subgroup;

    @Column(insertable = false, updatable = false)
    private String year;

    ExamSummaryResponse.GroupResponse toGroupResponse() {
        return ExamSummaryResponse.GroupResponse.builder()
                .id(id)
                .name(name)
                .subgroup(subgroup)
                .year(year)
                .build();
    }
}
