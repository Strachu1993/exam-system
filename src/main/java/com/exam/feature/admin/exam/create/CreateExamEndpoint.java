package com.exam.feature.admin.exam.create;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "${api.url.path.admin}/exam")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class CreateExamEndpoint {

    private final CreateExamService examService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/createData", produces = MediaType.APPLICATION_JSON_VALUE)
    CreateExamDataResponse createExamData() {
        return examService.getCreateExamData();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/create")
    void create(@RequestBody @Valid final CreateExamRequest createExamRequest) {
        examService.create(createExamRequest);
    }
}
