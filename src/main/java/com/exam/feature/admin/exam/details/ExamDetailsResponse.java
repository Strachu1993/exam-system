package com.exam.feature.admin.exam.details;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.enums.Subgroup;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Builder
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class ExamDetailsResponse {

    private UUID examId;
    private LocalDateTime start;
    private LocalDateTime end;
    private ExamStatus status;
    private String description;
    private Set<GroupResponse> groups;
    private Set<SubjectResponse> subjects;

    @Builder
    @Getter
    @EqualsAndHashCode(of = "id")
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class SubjectResponse {
        private UUID id;
        private String name;
        private long questionSize;

        static SubjectResponse of(final SubjectEntity subject, final long questionSize) {
            return SubjectResponse.builder()
                    .id(subject.getId())
                    .name(subject.getName())
                    .questionSize(questionSize)
                    .build();
        }
    }

    @Builder
    @Getter
    @EqualsAndHashCode(of = "id")
    @ToString
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @NoArgsConstructor(access = AccessLevel.PACKAGE)
    static class GroupResponse {
        private UUID id;
        private String name;
        private Subgroup subgroup;
        private String year;

        static GroupResponse of(final StudentGroupEntity studentGroup) {
            return GroupResponse.builder()
                    .id(studentGroup.getId())
                    .name(studentGroup.getName())
                    .subgroup(studentGroup.getSubgroup())
                    .year(studentGroup.getYear())
                    .build();
        }
    }
}
