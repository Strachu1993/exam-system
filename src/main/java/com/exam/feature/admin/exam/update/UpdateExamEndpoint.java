package com.exam.feature.admin.exam.update;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/exam/update")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class UpdateExamEndpoint {

    private final UpdateExamService updateExamService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(value = "/{examId}")
    void updateExam(@PathVariable final UUID examId, @RequestBody @Valid final UpdateExamRequest updateExamRequest) {
        updateExamService.updateExam(examId, updateExamRequest);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(value = "/{examId}/add/group/{groupId}")
    void addStudentsToExamByGroup(@PathVariable final UUID examId, @PathVariable final UUID groupId) {
        updateExamService.addStudentsToExamByGroup(examId, groupId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(value = "/{examId}/add/questions")
    void addStudentsToExamByGroup(@PathVariable final UUID examId, @RequestBody @Valid final UpdateExamQuestionsRequest updateExamRequest) {
        updateExamService.addQuestionsToExamBySubject(examId, updateExamRequest);
    }
}
