package com.exam.feature.admin.subject.update;

import com.exam.commons.Constraint;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@ToString
class SubjectRequest {

    @NotBlank
    @Size(min = Constraint.Subject.MIN_NAME_LENGTH, max = Constraint.Subject.MAX_NAME_LENGTH)
    private String name;
}
