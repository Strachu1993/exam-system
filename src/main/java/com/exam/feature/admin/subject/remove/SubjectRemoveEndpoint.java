package com.exam.feature.admin.subject.remove;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/subjects")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class SubjectRemoveEndpoint {

    private final SubjectRemoveService subjectRemoveService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{subjectId}")
    void remove(@PathVariable final UUID subjectId) {
        subjectRemoveService.delete(subjectId);
    }
}
