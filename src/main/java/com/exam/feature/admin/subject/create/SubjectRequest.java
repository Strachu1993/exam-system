package com.exam.feature.admin.subject.create;

import com.exam.commons.Constraint;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder(access = AccessLevel.PACKAGE)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
class SubjectRequest {

    @NotBlank
    @Size(min = Constraint.Subject.MIN_NAME_LENGTH, max = Constraint.Subject.MAX_NAME_LENGTH)
    private final String name;
}
