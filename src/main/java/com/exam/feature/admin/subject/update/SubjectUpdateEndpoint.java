package com.exam.feature.admin.subject.update;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/subjects")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class SubjectUpdateEndpoint {

    private final SubjectUpdateService subjectService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(value = "/{subjectId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    void update(@PathVariable @NotBlank final UUID subjectId, @RequestBody @Valid final SubjectRequest subjectRequest) {
        subjectService.update(subjectId, subjectRequest);
    }
}
