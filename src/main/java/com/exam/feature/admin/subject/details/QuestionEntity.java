package com.exam.feature.admin.subject.details;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@EqualsAndHashCode(exclude = "answers")
@ToString(exclude = "answers")
@Entity
@Table(name = "question")
class QuestionEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String question;

    @Column(insertable = false, updatable = false)
    private String code;

    @Column(insertable = false, updatable = false)
    private short correctAnswers;

    @JoinColumn(name = "fk_subject", insertable = false, updatable = false)
    @ManyToOne
    private SubjectEntity subject;

    @JoinColumn(name = "fk_question")
    @OneToMany(fetch = FetchType.EAGER)
    private List<AnswerEntity> answers;

    @ManyToMany(mappedBy = "questions")
    private List<ExamEntity> exams;
}
