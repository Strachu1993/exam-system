package com.exam.feature.admin.subject.list;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class SubjectListService {

    private final SubjectListReadRepository subjectRepository;

    List<SubjectResponse> findAll() {
        final Sort sort = Sort.by(
                Sort.Order.desc("name"));
        return subjectRepository.findAll(sort).stream()
                .map(SubjectResponse::of)
                .collect(Collectors.toList());
    }
}
