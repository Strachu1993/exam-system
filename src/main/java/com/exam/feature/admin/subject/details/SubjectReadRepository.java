package com.exam.feature.admin.subject.details;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface SubjectReadRepository extends Repository<SubjectEntity, UUID> {

    Optional<SubjectEntity> findById(UUID subjectId, Sort sort);
}
