package com.exam.feature.admin.subject.list;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Repository
interface SubjectListReadRepository extends Repository<SubjectEntity, UUID> {

    List<SubjectEntity> findAll(Sort sort);
}