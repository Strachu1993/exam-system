package com.exam.feature.admin.subject.create;

import com.exam.commons.exception.custom.validate.SubjectNameAlreadyExistException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class SubjectCreateService {

    private final SubjectRepository subjectRepository;

    @Transactional
    public void create(final SubjectRequest subjectRequest) {
        final String newName = subjectRequest.getName();
        validateNewName(newName);
        final SubjectEntity subjectEntity = SubjectEntity.builder()
                .name(newName)
                .build();
        subjectRepository.save(subjectEntity);
        log.info("Subject named '{}' has been added.", newName);
    }

    private void validateNewName(final String newName) {
        final int existNameCount = subjectRepository.countByName(newName);
        if (existNameCount > 0) {
            throw new SubjectNameAlreadyExistException(newName);
        }
    }
}
