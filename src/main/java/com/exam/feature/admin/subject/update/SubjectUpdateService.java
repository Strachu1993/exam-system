package com.exam.feature.admin.subject.update;

import com.exam.commons.exception.custom.not_found.SubjectNotFoundException;
import com.exam.commons.exception.custom.validate.SubjectNameAlreadyExistException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class SubjectUpdateService {

    private final SubjectRepository subjectRepository;

    @Transactional
    public void update(final UUID subjectId, final SubjectRequest subjectRequest) {
        final SubjectEntity subjectEntity = findSubjectEntity(subjectId);
        final String newName = subjectRequest.getName();
        validateNewName(newName);
        subjectEntity.setName(newName);
        subjectRepository.save(subjectEntity);
        log.info("Subjects with id '{}' has been updated.", subjectId);
    }

    private SubjectEntity findSubjectEntity(final UUID subjectId) {
        return subjectRepository.findById(subjectId)
                .orElseThrow(() -> new SubjectNotFoundException(subjectId));
    }

    private void validateNewName(final String newName) {
        final int newNameCount = subjectRepository.countByName(newName);
        if (newNameCount > 0) {
            throw new SubjectNameAlreadyExistException(newName);
        }
    }
}
