package com.exam.feature.admin.subject.details;

import com.exam.commons.exception.custom.not_found.SubjectNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class SubjectDetailsService {

    private final SubjectReadRepository subjectRepository;

    SubjectDetailsResponse getSubjectDetails(final UUID subjectId) {
        final SubjectEntity subject = findSubjectEntity(subjectId);
        final List<SubjectDetailsResponse.QuestionResponse> questionResponses = subject.getQuestions().stream()
                .map(SubjectDetailsResponse.QuestionResponse::of)
                .collect(Collectors.toList());

        return SubjectDetailsResponse.builder()
                .id(subject.getId())
                .name(subject.getName())
                .questions(questionResponses)
                .build();
    }

    private SubjectEntity findSubjectEntity(final UUID subjectId) {
        final Sort sort = Sort.by(
                Sort.Order.desc("name"));
        return subjectRepository.findById(subjectId, sort)
                .orElseThrow(() -> new SubjectNotFoundException(subjectId));
    }
}
