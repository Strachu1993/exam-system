package com.exam.feature.admin.subject.details;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "${api.url.path.admin}/subjects")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class SubjectDetailsEndpoint {

    private final SubjectDetailsService subjectDetailsService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{subjectId}", produces = MediaType.APPLICATION_JSON_VALUE)
    SubjectDetailsResponse getSubjectDetails(@PathVariable final UUID subjectId) {
        return subjectDetailsService.getSubjectDetails(subjectId);
    }
}
