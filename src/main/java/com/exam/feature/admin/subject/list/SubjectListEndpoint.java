package com.exam.feature.admin.subject.list;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "${api.url.path.admin}/subjects")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class SubjectListEndpoint {

    private final SubjectListService subjectService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<SubjectResponse> findAll() {
        return subjectService.findAll();
    }
}
