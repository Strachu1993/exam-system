package com.exam.feature.admin.subject.remove;

import com.exam.commons.exception.custom.general.SubjectException;
import com.exam.commons.exception.custom.not_found.SubjectNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@Service
class SubjectRemoveService {

    private final SubjectRepository subjectRepository;

    void delete(final UUID subjectId) {
        try {
            subjectRepository.deleteById(subjectId);
            log.info("Subject with id '{}' has been removed.", subjectId);
        } catch (final EmptyResultDataAccessException ex) {
            throw new SubjectNotFoundException(subjectId);
        } catch (final DataIntegrityViolationException ex) {
            throw SubjectException.ofReference(subjectId, ex);
        }
    }
}
