package com.exam.feature.admin.subject.details;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
class SubjectDetailsResponse {

    private final UUID id;
    private final String name;
    private final List<QuestionResponse> questions;

    @Builder(access = AccessLevel.PACKAGE)
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    @Getter
    @EqualsAndHashCode
    @ToString
    static class QuestionResponse {

        private final UUID id;
        private final String question;
        private final boolean hasCode;
        private final int totalAnswers;
        private final short correctAnswers;
        private final boolean canDelete;

        static QuestionResponse of(final QuestionEntity question) {
            final boolean hasCode = question.getCode() != null;
            final int totalAnswersCount = question.getAnswers().size();

            return QuestionResponse.builder()
                    .id(question.getId())
                    .question(question.getQuestion())
                    .hasCode(hasCode)
                    .totalAnswers(totalAnswersCount)
                    .correctAnswers(question.getCorrectAnswers())
                    .canDelete(question.getExams().isEmpty())
                    .build();
        }
    }
}
