package com.exam.feature.admin.subject.create;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping(value = "${api.url.path.admin}/subjects")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class SubjectCreateEndpoint {

    private final SubjectCreateService subjectService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void add(@RequestBody @Valid final SubjectRequest subjectRequest) {
        subjectService.create(subjectRequest);
    }
}
