package com.exam.feature.admin.subject.list;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
@ToString
class SubjectResponse {

    private final UUID id;
    private final String name;
    private final int questionSize;

    static SubjectResponse of(final SubjectEntity subject) {
        return SubjectResponse.builder()
                .id(subject.getId())
                .name(subject.getName())
                .questionSize(subject.getQuestions().size())
                .build();
    }
}
