package com.exam.feature.admin.subject.details;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter(value = AccessLevel.PACKAGE)
@EqualsAndHashCode(exclude = "questions")
@ToString(exclude = "questions")
@Entity
@Table(name = "subject")
class SubjectEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private String name;

    @OneToMany(mappedBy = "subject", fetch = FetchType.EAGER)
    private List<QuestionEntity> questions;
}
