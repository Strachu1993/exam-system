import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpParams, HttpErrorResponse} from '@angular/common/http';
import {addressAuth, storageAuth, storageRole, storageTokenType} from 'src/app/app.component';
import {LoginDataDto} from './loginRequest.dto';
import {LoginResponseDto} from './LoginResponse.dto';
import {Router} from '@angular/router';
import {Role} from 'src/app/commons/permission/role.enum';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient, private router: Router) {
  }

  async login(body: LoginDataDto) {
    let errorStatus = '';
    await this.http.post<LoginResponseDto>(addressAuth, body).toPromise()
      .then((auth: LoginResponseDto) => {
          this.setLocalStoreAuth(auth.tokenType, auth.accessToken, auth.roles);
          this.redirect();
        },
        (error: HttpErrorResponse) => {
          errorStatus = error.status.toString();
        });

    return errorStatus;
  }

  logout() {
    this.setLocalStoreAuth(null, null, null);
    this.router.navigate(['/login']);
  }

  private redirect() {
    const roles = localStorage.getItem(storageRole);
    if (roles.includes(Role.ROLE_ADMIN)) {
      this.router.navigate(['/admin']);
    } else if (roles.includes(Role.ROLE_STUDENT)) {
      this.router.navigate(['/student']);
    }
  }

  private setLocalStoreAuth(tokenType: string, accessToken: string, roles: string) {
    localStorage.setItem(storageTokenType, tokenType);
    localStorage.setItem(storageAuth, accessToken);
    localStorage.setItem(storageRole, roles);
  }
}
