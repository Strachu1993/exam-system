import {Component, OnInit} from '@angular/core';
import {LoginDataDto} from './loginRequest.dto';
import {LoginService} from './login.service';
import {ActivatedRoute} from '@angular/router';
import {LoginResponseDto} from './LoginResponse.dto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  private readonly defaultBorder = 'gray 2px solid';
  errorMessage: string;
  loginBorder = this.defaultBorder;
  passwordBorder = this.defaultBorder;

  loginData: LoginDataDto = ({
    login: '',
    password: ''
  });

  constructor(private loginService: LoginService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      const pageNotAvailableMessage = params['pageNotAvailable'];
      if (pageNotAvailableMessage) {
        this.errorMessage = 'Brak dostępu do ' + pageNotAvailableMessage;
      }
    });
  }

  async login() {
    this.loginBorder = this.getBorder(this.loginData.login);
    this.passwordBorder = this.getBorder(this.loginData.password);

    if (this.checkIsEqualsNone(this.loginBorder) && this.checkIsEqualsNone(this.passwordBorder)) {
      const error = await this.loginService.login(this.loginData);
      if (error) {
        this.errorMessage = 'Błąd autoryzacji';
      }
    }
  }

  private checkIsEqualsNone(value: string): boolean {
    return value === this.defaultBorder;
  }

  private getBorder(value: string): string {
    if (!value) {
      return 'red 2px solid';
    }
    return this.defaultBorder;
  }

}
