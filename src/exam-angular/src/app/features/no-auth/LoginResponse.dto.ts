export interface LoginResponseDto {
  accessToken: string;
  tokenType: string;
  roles: string;
}
