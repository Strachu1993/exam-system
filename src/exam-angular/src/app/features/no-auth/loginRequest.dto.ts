export interface LoginDataDto {
  login: string;
  password: string;
}
