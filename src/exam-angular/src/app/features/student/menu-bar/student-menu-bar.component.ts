import {Component} from '@angular/core';
import {LoginService} from '../../no-auth/login.service';

@Component({
  selector: 'app-student-menu-bar',
  templateUrl: './student-menu-bar.component.html',
  styleUrls: ['./student-menu-bar.component.css']
})
export class StudentMenuBarComponent {

  constructor(private loginService: LoginService) {
  }

  logout() {
    this.loginService.logout();
  }
}
