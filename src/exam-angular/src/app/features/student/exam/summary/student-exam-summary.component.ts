import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {StudentExamHttpService} from '../student-exam-http.service';
import {StudentScoreResponse} from '../shared/dto/student-score-response';

@Component({
  selector: 'app-student-exam-summary',
  templateUrl: './student-exam-summary.component.html',
  styleUrls: ['./student-exam-summary.component.css']
})
export class StudentExamSummaryComponent implements OnInit {

  studentScoreId: string;
  studentScoreResponse: StudentScoreResponse;

  constructor(private studentExamHttpService: StudentExamHttpService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadExamSummary();
  }

  loadExamSummary() {
    this.route.paramMap.subscribe((param: Params) => {
      this.studentScoreId = param.get('studentScoreId');
      this.studentExamHttpService.finishExam(this.studentScoreId).subscribe((studentScoreResponse: StudentScoreResponse) => {
        this.studentScoreResponse = studentScoreResponse;
      });
    });
  }
}
