import {Component, OnInit} from '@angular/core';
import {StudentExamHttpService} from '../student-exam-http.service';
import {StudentExamListResponse} from '../shared/dto/student-exam-list-response';
import {ScoreStatus} from '../../../../commons/enum/score-status.enum';
import {CyclicCallingService} from '../../../../commons/service/cyclic-calling.service';

@Component({
  selector: 'app-student-exam-list',
  templateUrl: './student-exam-list.component.html',
  styleUrls: ['./student-exam-list.component.css'],
  providers: [CyclicCallingService]
})
export class StudentExamListComponent implements OnInit {

  studentExamListResponse: Array<StudentExamListResponse>;

  constructor(private studentExamHttpService: StudentExamHttpService,
              private cyclicCallingService: CyclicCallingService) {
  }

  ngOnInit(): void {
    this.cyclicCallingService.initLoop(30, 1, () => this.getExams());
  }

  examForStudentIsAvailable(index: number): boolean {
    return this.studentExamListResponse[index].scoreStatus === ScoreStatus.IN_PROGRESS;
  }

  examScoreIsAvailable(index: number): boolean {
    return this.studentExamListResponse[index].scoreStatus === ScoreStatus.FINISH;
  }

  private getExams() {
    this.studentExamHttpService.getExams().subscribe((studentExamListResponse: Array<StudentExamListResponse>) => {
      this.studentExamListResponse = studentExamListResponse;
    });
  }
}
