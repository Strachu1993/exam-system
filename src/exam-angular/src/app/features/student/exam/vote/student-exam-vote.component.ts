import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {QuestionResponse, StudentExamVoteResponse} from '../shared/dto/student-exam-vote-response';
import {StudentExamHttpService} from '../student-exam-http.service';
import {AnswersVoteRequest, StudentExamVoteRequest} from '../shared/dto/student-exam-vote-request';
import {Page} from '../shared/dto/page';
import {TimerService} from '../../../../commons/service/timer.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-student-exam-vote',
  templateUrl: './student-exam-vote.component.html',
  styleUrls: ['./student-exam-vote.component.css'],
  providers: [TimerService]
})
export class StudentExamVoteComponent implements OnInit {

  private studentScoreId: string;

  paginationNumbers: Array<Page>;
  currentPageIndex: number;
  studentExamVoteResponse: StudentExamVoteResponse;
  restTime: string;

  question1: QuestionResponse;
  question2: QuestionResponse;
  image1: any;
  image2: any;
  studentVote1: StudentExamVoteRequest;
  studentVote2: StudentExamVoteRequest;

  constructor(private studentExamHttpService: StudentExamHttpService,
              private router: Router,
              private route: ActivatedRoute,
              private timerService: TimerService,
              private sanitizer: DomSanitizer) {
    this.currentPageIndex = 0;
  }

  getCurrentPage(): Page {
    return this.paginationNumbers[this.currentPageIndex];
  }

  ngOnInit(): void {
    this.loadExamDetails();
  }

  vote(successAction) {
    const body = [this.studentVote1];
    if (this.studentVote2) {
      body.push(this.studentVote2);
    }

    this.studentExamHttpService.voteAnswers(this.studentScoreId, body).subscribe(() => successAction());
  }

  goToPage(pageIndex: number) {
    this.vote(() => {});
    this.currentPageIndex = pageIndex;
    this.loadExamDetails();
    this.movePageToTheTop();
  }

  redirectToStudentExamSummary() {
    this.vote(() => this.router.navigate(['/student/exam/' + this.studentScoreId + '/summary']));
  }

  getActivePaginationClass(pageIndex: number): string {
    return this.currentPageIndex === pageIndex ? 'active' : 'cursor-pointer';
  }

  checkPreviousPaginationButton(): string {
    return this.currentPageIndex === 0 ? 'disabled' : 'cursor-pointer';
  }

  checkNextPaginationButton(): string {
    return this.currentPageIndex === this.paginationNumbers.length - 1 ? 'disabled' : 'cursor-pointer';
  }

  goToPreviousPage() {
    if (this.currentPageIndex !== 0) {
      this.goToPage(this.currentPageIndex - 1);
    }
  }

  goToNextPage() {
    if (this.currentPageIndex !== this.paginationNumbers.length - 1) {
      this.goToPage(this.currentPageIndex + 1);
    }
  }

  private movePageToTheTop() {
    window.scroll(0, 0);
  }

  private loadExamDetails() {
    this.route.paramMap.subscribe((param: Params) => {
      this.studentScoreId = param.get('studentScoreId');
      this.studentExamHttpService.getStudentExamDetails(this.studentScoreId).subscribe((studentExamVoteResponse: StudentExamVoteResponse) => {
        this.studentExamVoteResponse = studentExamVoteResponse;
        this.setTimer();
        this.paginationNumbers = this.getPaginationNumbers();
        this.initQuestions();

        this.studentVote1 = this.preparedInitStudentVote(this.question1);
        this.studentVote2 = this.preparedInitStudentVote(this.question2);
      });
    });
  }

  private setTimer(): void {
    this.timerService.setRestTimeInterval(this.studentExamVoteResponse?.end, 15, () => this.redirectToStudentExamSummary());
    this.timerService.changeTime.subscribe(restTime => this.restTime = restTime);
  }

  private preparedInitStudentVote(data: QuestionResponse): StudentExamVoteRequest {
    if (data) {
      const voteAnswers = Array<AnswersVoteRequest>();
      for (const answer of data.answers) {
        voteAnswers.push({
          studentAnswerId: answer.studentAnswerId,
          checked: answer.selected
        });
      }

      return {
        studentQuestionId: data.studentQuestionId,
        answers: voteAnswers
      };
    }

    return null;
  }

  private getPaginationNumbers(): Array<Page> {
    const pageNumbers = Array<Page>();
    const questionSize = this.studentExamVoteResponse.studentQuestionResponses.length;

    for (let i = 0; i < questionSize; i = i + 2) {
      if (i + 2 <= questionSize) {
        pageNumbers.push({first: i, second: i + 1});
      } else {
        pageNumbers.push({first: i, second: null});
      }
    }

    return pageNumbers;
  }

  private initQuestions() {
    this.question1 = null;
    this.question2 = null;

    const currentPage = this.paginationNumbers[this.currentPageIndex];
    if (currentPage.first !== null) {
      this.question1 = this.studentExamVoteResponse.studentQuestionResponses[currentPage.first];
    }
    if (currentPage.second !== null) {
      this.question2 = this.studentExamVoteResponse.studentQuestionResponses[currentPage.second];
    }

    this.image1 = this.prepareImage(this.question1);
    this.image2 = this.prepareImage(this.question2);
  }

  private prepareImage(questionResponse: QuestionResponse): any {
    if (questionResponse?.image) {
      const objectURL: string = 'data:image/png;base64,' + questionResponse.image;
      return this.sanitizer.bypassSecurityTrustUrl(objectURL);
    }
    return null;
  }
}
