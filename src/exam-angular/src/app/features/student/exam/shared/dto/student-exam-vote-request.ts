export interface StudentExamVoteRequest {

  studentQuestionId: string;
  answers: AnswersVoteRequest[];
}

export interface AnswersVoteRequest {

  studentAnswerId: string;
  checked: boolean;
}
