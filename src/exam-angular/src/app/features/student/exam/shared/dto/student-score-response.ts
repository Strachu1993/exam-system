import {Rating} from '../../../../../commons/enum/rating.enum';

export interface StudentScoreResponse {

  studentScoreId: string;
  correctQuestions: number;
  inCorrectQuestions: number;
  score: number;
  rating: Rating;
}
