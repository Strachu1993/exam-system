export interface StudentExamVoteResponse {
  start: Date;
  end: Date;
  studentScoreId: string;
  studentQuestionResponses: QuestionResponse[];
}

export interface QuestionResponse {
  studentQuestionId: string;
  question: string;
  code: string;
  codeSyntax: string;
  image: string;
  correctAnswers: number;
  answers: AnswerResponse[];
}

export interface AnswerResponse {
  studentAnswerId: string;
  answer: string;
  selected: boolean;
}
