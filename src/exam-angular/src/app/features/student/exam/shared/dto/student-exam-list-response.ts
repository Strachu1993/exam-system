import {ScoreStatus} from '../../../../../commons/enum/score-status.enum';
import {ExamStatus} from '../../../../../commons/enum/exam-status.enum';

export interface StudentExamListResponse {

  start: string;
  end: string;
  examStatus: ExamStatus;
  description: string;
  studentScoreId: string;
  scoreStatus: ScoreStatus;
}
