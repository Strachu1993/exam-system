import {Component} from '@angular/core';
import {StudentExamHttpService} from './student-exam-http.service';

@Component({
  selector: 'app-student-exam',
  template: `
    <div class="container">
      <router-outlet></router-outlet>
    </div>`,
  providers: [
    StudentExamHttpService
  ]
})
export class StudentExamComponent {
}
