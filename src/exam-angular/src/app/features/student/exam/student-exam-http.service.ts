import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {addressStudent} from '../../../app.component';
import {Observable} from 'rxjs';
import {StudentExamListResponse} from './shared/dto/student-exam-list-response';
import {StudentExamVoteResponse} from './shared/dto/student-exam-vote-response';
import {StudentExamVoteRequest} from './shared/dto/student-exam-vote-request';
import {StudentScoreResponse} from './shared/dto/student-score-response';

@Injectable({
  providedIn: 'root'
})
export class StudentExamHttpService {

  private studentExamsUrl = addressStudent + '/exams';
  private studentExamUrl = addressStudent + '/exam/';

  constructor(private http: HttpClient) { }

  getExams(): Observable<Array<StudentExamListResponse>> {
    return this.http.get<Array<StudentExamListResponse>>(this.studentExamsUrl);
  }

  getStudentExamDetails(studentScoreId: string): Observable<StudentExamVoteResponse> {
    return this.http.get<StudentExamVoteResponse>(this.studentExamUrl  + studentScoreId);
  }

  voteAnswers(studentScoreId: string, body: StudentExamVoteRequest[]): Observable<void> {
    return this.http.patch<void>(this.studentExamUrl + studentScoreId + '/vote', body);
  }

  finishExam(studentScoreId: string): Observable<StudentScoreResponse> {
    return this.http.patch<StudentScoreResponse>(this.studentExamUrl + studentScoreId + '/finish', null);
  }
}
