import {Component} from '@angular/core';

@Component({
  selector: 'app-student',
  template: `
    <app-student-menu-bar></app-student-menu-bar>
    <router-outlet></router-outlet>`
})
export class StudentComponent {
}
