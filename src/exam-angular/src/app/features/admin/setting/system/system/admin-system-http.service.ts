import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {addressAdmin} from '../../../../../app.component';
import {Observable} from 'rxjs';
import {SettingRatingResponse} from './shared/dto/setting-rating-response';
import {SettingRatingRequest} from './shared/dto/setting-rating-request';

@Injectable({
  providedIn: 'root'
})
export class AdminSystemHttpService {

  private adminSettingsRatingListGetUrl = addressAdmin + '/system/setting/ratings';
  private adminSettingsRatingUpdatePutUrl = addressAdmin + '/system/setting/ratings/update';

  constructor(private http: HttpClient) {
  }

  getSettingRatings(): Observable<Array<SettingRatingResponse>> {
    return this.http.get<Array<SettingRatingResponse>>(this.adminSettingsRatingListGetUrl);
  }

  editSettingRatings(body: Array<SettingRatingRequest>): Observable<void> {
    return this.http.put<void>(this.adminSettingsRatingUpdatePutUrl, body);
  }
}
