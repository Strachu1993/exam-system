import {Component} from '@angular/core';
import {AdminSystemHttpService} from './admin-system-http.service';

@Component({
  selector: 'app-admin-system',
  template: `
    <div class="container">
      <app-admin-rating></app-admin-rating>
    </div>`,
  providers: [
    AdminSystemHttpService
  ]
})
export class AdminSystemComponent {

}
