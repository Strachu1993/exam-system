import {Rating} from '../../../../../../../commons/enum/rating.enum';

export interface SettingRatingResponse {

  id: string;
  symbol: Rating;
  maxScope: number;
  minScope: number;
}
