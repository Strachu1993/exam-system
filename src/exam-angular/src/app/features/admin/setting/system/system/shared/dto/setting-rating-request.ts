import {Rating} from '../../../../../../../commons/enum/rating.enum';

export interface SettingRatingRequest {

  id: string;
  symbol: Rating;
  maxScope: number;
  minScope: number;
}
