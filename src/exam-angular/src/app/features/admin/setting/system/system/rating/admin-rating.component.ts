import { Component, OnInit } from '@angular/core';
import {AdminSystemHttpService} from '../admin-system-http.service';
import {StudentGroupListResponse} from '../../../../student-group/shared/dto/student-group-list-response';
import {SettingRatingResponse} from '../shared/dto/setting-rating-response';
import {SettingRatingRequest} from '../shared/dto/setting-rating-request';
import {Rating} from '../../../../../../commons/enum/rating.enum';

@Component({
  selector: 'app-admin-rating',
  templateUrl: './admin-rating.component.html',
  styleUrls: ['./admin-rating.component.css']
})
export class AdminRatingComponent implements OnInit {

  settingRatingResponse: Array<SettingRatingResponse>;
  settingRatingRequest: Array<SettingRatingRequest>;

  constructor(private adminSystemHttpService: AdminSystemHttpService) { }

  ngOnInit(): void {
    this.loadRating();
  }

  editSettingRatings() {
    this.adminSystemHttpService.editSettingRatings(this.settingRatingRequest).subscribe(() => {
      this.loadRating();
      alert('Zmieniono');
    });
  }

  private loadRating() {
    this.adminSystemHttpService.getSettingRatings().subscribe((settingRatingResponse: Array<SettingRatingResponse>) => {
      this.settingRatingResponse = settingRatingResponse;
      this.settingRatingRequest = [];

      this.settingRatingResponse.forEach((rating: SettingRatingResponse) => {
        const settingRatingRequest: SettingRatingRequest = {
          id: rating.id,
          symbol: rating.symbol,
          maxScope: rating.maxScope,
          minScope: rating.minScope
        };

        this.settingRatingRequest.push(settingRatingRequest);
      });
    });
  }
}
