import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ExamRouteService} from '../../../../commons/service/exam-route.service';
import {StudentGroupHttpService} from '../student-group-http.service';
import {StudentGroupDetailsResponse} from '../shared/dto/student-group-details-response';
import {saveAs} from 'file-saver';
import {StudentGroupRequest} from '../shared/dto/student-group-request';
import {StudentAccountRequest} from '../shared/dto/student-account-request';

@Component({
  selector: 'app-student-group-details',
  templateUrl: './student-group-details.component.html',
  styleUrls: ['./student-group-details.component.css']
})
export class StudentGroupDetailsComponent implements OnInit {

  studentGroupsDetails: StudentGroupDetailsResponse;
  groupId: string;

  constructor(private studentGroupHttpService: StudentGroupHttpService,
              private router: Router,
              private route: ActivatedRoute,
              private examRoute: ExamRouteService) {
  }

  ngOnInit(): void {
    this.loadExamDetails();
  }

  getStudentsPdfFromGroup() {
    this.studentGroupHttpService.getPdfByGroupId(this.groupId).subscribe((data) => {
        saveAs(new Blob([data], {type: 'application/pdf'}), this.getPdfFileName());
    });
  }

  addStudent(body: StudentAccountRequest) {
    body.groupId = this.groupId;
    this.studentGroupHttpService.addStudent(body).subscribe(() => this.examRoute.reloadPage());
  }

  editGroup(body: StudentGroupRequest) {
    this.studentGroupHttpService.editGroup(this.groupId, body).subscribe(() => this.examRoute.reloadPage());
  }

  deleteStudent(studentId: string) {
    this.studentGroupHttpService.deleteStudent(studentId).subscribe(() => this.examRoute.reloadPage());
  }

  activeStudentAccount(accountId: string) {
    this.studentGroupHttpService.activeStudentAccount(accountId).subscribe(() => this.examRoute.reloadPage());
  }

  private getPdfFileName() {
    return `${this.studentGroupsDetails.name}_${this.studentGroupsDetails.subgroup}_${this.studentGroupsDetails.year}.pdf`;
  }

  private loadExamDetails() {
    this.route.paramMap.subscribe((param: Params) => {
      this.groupId = param.get('id');
      this.studentGroupHttpService.getStudentGroupDetails(this.groupId).subscribe((studentGroupsDetails: StudentGroupDetailsResponse) => {
        this.studentGroupsDetails = studentGroupsDetails;
      });
    });
  }
}
