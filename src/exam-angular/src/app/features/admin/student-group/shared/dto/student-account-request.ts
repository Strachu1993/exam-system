export interface StudentAccountRequest {

  name: string;
  surname: string;
  groupId: string;
  albumNumber: string;
}
