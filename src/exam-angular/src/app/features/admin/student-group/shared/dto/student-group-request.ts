import {Subgroup} from '../../../../../commons/enum/subgroup.enum';

export interface StudentGroupRequest {

  name: string;
  year: string;
  subgroup: Subgroup;
}
