import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StudentGroupRequest} from '../../dto/student-group-request';
import {Subgroup} from '../../../../../../commons/enum/subgroup.enum';

@Component({
  selector: 'app-student-group-modal',
  templateUrl: './student-group-modal.component.html',
  styleUrls: ['./student-group-modal.component.css']
})
export class StudentGroupModalComponent implements OnInit {

  @Input()
  textTitle: string;

  @Output()
  studentGroupRequestEvent = new EventEmitter<StudentGroupRequest>();

  subgroups: string[];
  studentGroupRequest: StudentGroupRequest;

  ngOnInit(): void {
    this.prepareSubgroups();
    this.initStudentGroupRequest();
  }

  emitStudentGroup() {
    this.studentGroupRequestEvent.emit(this.studentGroupRequest);
  }

  cleanModal() {
    this.initStudentGroupRequest();
  }

  initStudentGroupRequest() {
    this.studentGroupRequest = {
      name: '',
      subgroup: null,
      year: ''
    };
  }

  private prepareSubgroups() {
    this.subgroups = Object.keys(Subgroup).filter((item) => isNaN(Number(item)));
  }
}
