import {Subgroup} from '../../../../../commons/enum/subgroup.enum';

export interface StudentGroupListResponse {

  id: string;
  name: string;
  year: string;
  subgroup: Subgroup;
  studentSize: number;
}
