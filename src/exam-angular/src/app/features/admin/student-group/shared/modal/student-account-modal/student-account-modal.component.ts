import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StudentAccountRequest} from '../../dto/student-account-request';

@Component({
  selector: 'app-student-account-modal',
  templateUrl: './student-account-modal.component.html',
  styleUrls: ['./student-account-modal.component.css']
})
export class StudentAccountModalComponent implements OnInit {

  @Input()
  textTitle: string;

  @Output()
  studentAccountRequestEvent = new EventEmitter<StudentAccountRequest>();

  studentAccountRequest: StudentAccountRequest;

  constructor() { }

  ngOnInit(): void {
    this.initStudentGroupRequest();
  }

  emitStudentAccountRequest() {
    this.studentAccountRequestEvent.emit(this.studentAccountRequest);
  }

  cleanModal() {
    this.initStudentGroupRequest();
  }

  initStudentGroupRequest() {
    this.studentAccountRequest = {
      name: '',
      surname: '',
      groupId: '',
      albumNumber: ''
    };
  }
}
