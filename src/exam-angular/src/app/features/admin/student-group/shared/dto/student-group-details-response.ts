import {Subgroup} from '../../../../../commons/enum/subgroup.enum';

export interface StudentGroupDetailsResponse {
  id: string;
  name: string;
  year: string;
  subgroup: Subgroup;
  students: Array<StudentDetailsResponse>;
}

export interface StudentDetailsResponse {
  studentScoreId: string;
  accountId: string;
  enabled: boolean;
  albumNumber: number;
  name: string;
  surname: string;
}
