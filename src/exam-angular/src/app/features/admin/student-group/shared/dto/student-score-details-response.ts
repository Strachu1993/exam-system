import {ExamStatus} from '../../../../../commons/enum/exam-status.enum';
import {ScoreStatus} from '../../../../../commons/enum/score-status.enum';
import {Rating} from '../../../../../commons/enum/rating.enum';

export interface StudentScoreDetailsResponse {

  examId: string;
  start: Date;
  end: Date;
  description: string;
  examStatus: ExamStatus;
  scoreId: string;
  correctQuestions: number;
  inCorrectQuestions: number;
  score: number;
  rating: Rating;
  scoreStatus: ScoreStatus;
}
