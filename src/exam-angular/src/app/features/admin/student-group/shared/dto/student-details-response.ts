import {Subgroup} from '../../../../../commons/enum/subgroup.enum';
import {StudentExamStatus} from '../../../../../commons/enum/student-exam-status.enum';
import {Rating} from '../../../../../commons/enum/rating.enum';

export interface StudentDetailsResponse {
  accountId: string;
  enabled: boolean;
  studentDetailsId: string;
  name: string;
  surname: string;
  albumNumber: string;
  group: StudentDetailsGroupResponse;
  exams: StudentDetailsExamResponse[];
}

export interface StudentDetailsGroupResponse {
  id: string;
  name: string;
  year: string;
  subgroup: Subgroup;
}

export interface StudentDetailsExamResponse {
  examId: string;
  start: Date;
  end: Date;
  description: string;
  scoreId: string;
  rating: Rating;
  scoreStatus: StudentExamStatus;
}
