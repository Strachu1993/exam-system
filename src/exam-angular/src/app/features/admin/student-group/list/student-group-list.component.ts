import {Component, OnInit} from '@angular/core';
import {ExamRouteService} from '../../../../commons/service/exam-route.service';
import {StudentGroupHttpService} from '../student-group-http.service';
import {StudentGroupListResponse} from '../shared/dto/student-group-list-response';
import {StudentGroupRequest} from '../shared/dto/student-group-request';

@Component({
  selector: 'app-student-group-list',
  templateUrl: './student-group-list.component.html',
  styleUrls: ['./student-group-list.component.css']
})
export class StudentGroupListComponent implements OnInit {

  studentGroupListResponse: Array<StudentGroupListResponse>;

  constructor(private studentGroupHttpService: StudentGroupHttpService,
              private examRouteService: ExamRouteService) {
  }

  ngOnInit(): void {
    this.loadAllStudentsGroups();
  }

  deleteStudentGroup(studentGroupId: string) {
    this.studentGroupHttpService.deleteStudentGroup(studentGroupId).subscribe(() => this.examRouteService.reloadPage());
  }

  addStudentGroup(body: StudentGroupRequest) {
    this.studentGroupHttpService.addStudentGroup(body).subscribe(() => this.examRouteService.reloadPage());
  }

  private loadAllStudentsGroups() {
    this.studentGroupHttpService.loadStudentGroups().subscribe((studentGroupListResponse: Array<StudentGroupListResponse>) => {
      this.studentGroupListResponse = studentGroupListResponse;
    });
  }
}
