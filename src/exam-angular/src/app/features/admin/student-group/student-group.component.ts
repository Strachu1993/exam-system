import {Component} from '@angular/core';
import {StudentGroupHttpService} from './student-group-http.service';

@Component({
  selector: 'app-student-group',
  template: `
    <div class="container">
      <router-outlet></router-outlet>
    </div>`,
  providers: [
    StudentGroupHttpService
  ]
})
export class StudentGroupComponent {
}
