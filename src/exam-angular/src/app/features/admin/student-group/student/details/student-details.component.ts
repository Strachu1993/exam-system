import {Component, OnInit} from '@angular/core';
import {StudentGroupHttpService} from '../../student-group-http.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ExamRouteService} from '../../../../../commons/service/exam-route.service';
import {StudentDetailsResponse} from '../../shared/dto/student-details-response';
import {StudentAccountRequest} from '../../shared/dto/student-account-request';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  private studentId: string;
  groupId: string;
  studentDetailsResponse: StudentDetailsResponse;

  constructor(private studentGroupHttpService: StudentGroupHttpService,
              private router: Router,
              private route: ActivatedRoute,
              private examRoute: ExamRouteService) {
  }

  ngOnInit(): void {
    this.loadStudentDetails();
  }

  editStudent(body: StudentAccountRequest) {
    body.groupId = this.groupId;
    this.studentGroupHttpService.editStudent(this.studentId, body).subscribe(() => this.examRoute.reloadPage());
  }

  getFullGroupName() {
    const group = this.studentDetailsResponse.group;
    return `${group.name} ${group.subgroup} ${group.year}`;
  }

  private loadStudentDetails() {
    this.route.paramMap.subscribe((param: Params) => {
      this.studentId = param.get('studentId');
      this.groupId = param.get('id');
      this.studentGroupHttpService.getStudentDetails(this.studentId).subscribe((studentDetailsResponse: StudentDetailsResponse) => {
        this.studentDetailsResponse = studentDetailsResponse;
      });
    });
  }
}
