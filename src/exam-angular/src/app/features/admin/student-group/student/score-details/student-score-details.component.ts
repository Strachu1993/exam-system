import { Component, OnInit } from '@angular/core';
import {StudentDetailsResponse} from '../../shared/dto/student-details-response';
import {StudentGroupHttpService} from '../../student-group-http.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ExamRouteService} from '../../../../../commons/service/exam-route.service';
import {StudentScoreDetailsResponse} from '../../shared/dto/student-score-details-response';

@Component({
  selector: 'app-student-score',
  templateUrl: './student-score-details.component.html',
  styleUrls: ['./student-score-details.component.css']
})
export class StudentScoreDetailsComponent implements OnInit {

  scoreId: string;
  studentId: string;
  groupId: string;
  studentScoreDetailsResponse: StudentScoreDetailsResponse;

  constructor(private studentGroupHttpService: StudentGroupHttpService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadStudentScoreDetails();
  }

  private loadStudentScoreDetails() {
    this.route.paramMap.subscribe((param: Params) => {
      this.scoreId = param.get('scoreId');
      this.studentId = param.get('studentId');
      this.groupId = param.get('id');

      this.studentGroupHttpService.getStudentScoreDetails(this.scoreId).subscribe((studentScoreDetailsResponse: StudentScoreDetailsResponse) => {
        this.studentScoreDetailsResponse = studentScoreDetailsResponse;
      });
    });
  }
}
