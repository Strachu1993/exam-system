import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {addressAdmin} from '../../../app.component';
import {StudentGroupListResponse} from './shared/dto/student-group-list-response';
import {Observable} from 'rxjs';
import {StudentGroupRequest} from './shared/dto/student-group-request';
import {StudentGroupDetailsResponse} from './shared/dto/student-group-details-response';
import {StudentAccountRequest} from './shared/dto/student-account-request';
import {StudentDetailsResponse} from './shared/dto/student-details-response';
import {StudentScoreDetailsResponse} from './shared/dto/student-score-details-response';

@Injectable({
  providedIn: 'root'
})
export class StudentGroupHttpService {

  private adminFindAllStudentGroupsUrl = addressAdmin + '/student-groups';
  private adminAddStudentGroupUrl = addressAdmin + '/student-groups';
  private adminDeleteStudentGroupUrl = addressAdmin + '/student-groups/{studentGroupId}';
  private adminStudentGroupDetailsUrl = addressAdmin + '/student-groups/{studentGroupId}';
  private adminUpdateStudentGroupUrl = addressAdmin + '/student-groups/{studentGroupId}';

  private adminGetPdfByGroupUrl = addressAdmin + '/pdf/group/students';

  private adminAddStudentAccountUrl = addressAdmin + '/students';
  private adminRemoveStudentUrl = addressAdmin + '/students/{studentAccountId}';
  private adminStudentDetailsUrl = addressAdmin + '/students/{studentAccountId}';
  private adminUpdateStudentUrl = addressAdmin + '/students/{studentAccountId}';
  private adminStudentScoreDetailsUrl = addressAdmin + '/students/{scoreId}/score';
  private adminActiveStudentAccountUrl = addressAdmin + '/students/{studentAccountId}/active';

  constructor(private http: HttpClient) {
  }

  loadStudentGroups(): Observable<Array<StudentGroupListResponse>> {
    return this.http.get<Array<StudentGroupListResponse>>(this.adminFindAllStudentGroupsUrl);
  }

  addStudentGroup(body: StudentGroupRequest): Observable<StudentGroupRequest> {
    return this.http.post<StudentGroupRequest>(this.adminAddStudentGroupUrl, body);
  }

  deleteStudentGroup(studentGroupId: string): Observable<void> {
    const url = this.adminDeleteStudentGroupUrl.replace('{studentGroupId}', studentGroupId);
    return this.http.delete<void>(url);
  }

  getStudentGroupDetails(studentGroupId: string): Observable<StudentGroupDetailsResponse> {
    const url = this.adminStudentGroupDetailsUrl.replace('{studentGroupId}', studentGroupId);
    return this.http.get<StudentGroupDetailsResponse>(url);
  }

  editGroup(studentGroupId: string, body: StudentGroupRequest): Observable<void> {
    const url = this.adminUpdateStudentGroupUrl.replace('{studentGroupId}', studentGroupId);
    return this.http.put<void>(url, body);
  }

  getPdfByGroupId(groupId: string) {
    const body = [
      groupId
    ];
    return this.http.post(this.adminGetPdfByGroupUrl, body, {
      responseType: 'arraybuffer'
    });
  }

  deleteStudent(studentId: string): Observable<void> {
    const url = this.adminRemoveStudentUrl.replace('{studentAccountId}', studentId);
    return this.http.delete<void>(url);
  }

  activeStudentAccount(studentId: string): Observable<void> {
    const url = this.adminActiveStudentAccountUrl.replace('{studentAccountId}', studentId);
    return this.http.patch<void>(url, null);
  }

  addStudent(body: StudentAccountRequest): Observable<void> {
    return this.http.post<void>(this.adminAddStudentAccountUrl, body);
  }

  getStudentDetails(studentId: string): Observable<StudentDetailsResponse> {
    const url = this.adminStudentDetailsUrl.replace('{studentAccountId}', studentId);
    return this.http.get<StudentDetailsResponse>(url);
  }

  editStudent(studentId: string, body: StudentAccountRequest) {
    const url = this.adminUpdateStudentUrl.replace('{studentAccountId}', studentId);
    return this.http.put<void>(url, body);
  }

  getStudentScoreDetails(scoreId: string): Observable<StudentScoreDetailsResponse> {
    const url = this.adminStudentScoreDetailsUrl.replace('{scoreId}', scoreId);
    return this.http.get<StudentScoreDetailsResponse>(url);
  }
}
