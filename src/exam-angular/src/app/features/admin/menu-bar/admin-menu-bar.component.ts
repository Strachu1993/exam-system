import {Component} from '@angular/core';
import {LoginService} from '../../no-auth/login.service';

@Component({
  selector: 'app-admin-menu-bar',
  templateUrl: './admin-menu-bar.component.html',
  styleUrls: ['./admin-menu-bar.component.css']
})
export class AdminMenuBarComponent {

  constructor(private loginService: LoginService) {
  }

  logout() {
    this.loginService.logout();
  }
}
