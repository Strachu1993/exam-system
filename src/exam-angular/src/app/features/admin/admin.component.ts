import {Component} from '@angular/core';

@Component({
  selector: 'app-admin',
  template: `
    <app-admin-menu-bar></app-admin-menu-bar>
    <router-outlet></router-outlet>`
})
export class AdminComponent {
}
