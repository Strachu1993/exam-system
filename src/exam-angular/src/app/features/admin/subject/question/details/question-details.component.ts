import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SubjectHttpService} from '../../subject-http.service';
import {ExamRouteService} from '../../../../../commons/service/exam-route.service';
import {QuestionDetailsResponse} from '../../shared/dto/question-details.response';
import {QuestionRequestDto} from '../../shared/dto/question-request.dto';
import {DomSanitizer} from '@angular/platform-browser';
import {QuestionModalEvent} from '../../shared/dto/question-modal-event';

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.css']
})
export class QuestionDetailsComponent implements OnInit {

  private questionId: string;
  subjectId: string;
  questionDetailsResponse: QuestionDetailsResponse;
  image: any;

  constructor(private subjectHttpService: SubjectHttpService,
              private router: Router,
              private route: ActivatedRoute,
              private examRoute: ExamRouteService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.getSubjectDetails();
  }

  editQuestion(event: QuestionModalEvent) {
    const image: File = event.image;
    const body: QuestionRequestDto = {
      subjectId: this.subjectId,
      question: event.question,
      code: event.code,
      codeSyntax: event.codeSyntax,
      answers: event.answers
    };
    this.subjectHttpService.updateQuestion(this.questionId, body, image).subscribe(() => this.examRoute.reloadPage());
  }

  private getSubjectDetails() {
    this.route.paramMap.subscribe((param: Params) => {
      this.questionId = param.get('questionId');
      this.subjectId = param.get('id');
      this.subjectHttpService.getQuestionDetails(this.questionId).subscribe((questionDetailsResponse: QuestionDetailsResponse) => {
        this.questionDetailsResponse = questionDetailsResponse;
        const objectURL: string = 'data:image/png;base64,' + this.questionDetailsResponse.image;
        this.image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      });
    });
  }
}
