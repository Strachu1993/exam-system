export interface SubjectDetailsResponse {

  id: string;
  name: string;
  questions: QuestionResponse[];
}

export interface QuestionResponse {

  id: string;
  question: string;
  hasCode: boolean;
  totalAnswers: number;
  correctAnswers: number;
  canDelete: boolean;
}
