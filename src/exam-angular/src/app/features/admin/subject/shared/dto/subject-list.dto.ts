export interface SubjectListDto {
  id: string;
  name: string;
  questionSize: number;
}
