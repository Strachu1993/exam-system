export interface QuestionModalEvent {
  subjectId: string;
  question: string;
  code: string;
  codeSyntax: string;
  image: File;
  answers: AnswerRequestEvent[];
}

export interface AnswerRequestEvent {
  answer: string;
  correct: boolean;
}
