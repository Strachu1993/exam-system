import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {QuestionModalEvent} from '../../dto/question-modal-event';

@Component({
  selector: 'app-question-modal',
  templateUrl: './question-modal.component.html',
  styleUrls: ['./question-modal.component.css']
})
export class QuestionModalComponent implements OnInit {

  @Input()
  subjectId: string;

  @Input()
  textTitle: string;

  @Output()
  questionModalEmitter = new EventEmitter<QuestionModalEvent>();

  questionModalEvent: QuestionModalEvent;
  previewUrl: any = null;
  fileUploadProgress: string = null;

  //https://highlightjs.org/static/demo/
  syntaxes: string[] = ['Plain text', 'Python', 'HTML', 'XML', 'Java', 'JavaScript', 'TypeScript', 'CoffeeScript', 'C++', 'C',
    'C#', 'Bash', 'CSS', 'SCSS', 'Less', 'Ruby', 'Go', 'HTTP', 'JSON', 'Kotlin', 'Perl', 'PHP', 'SQL', 'Swift', 'YAML'];

  ngOnInit(): void {
    this.initQuestionModalEvent();
  }

  emitQuestionModal() {
    console.log(this.questionModalEvent);
    this.questionModalEvent.code = this.questionModalEvent.code.length ? this.questionModalEvent.code : null;
    this.questionModalEmitter.emit(this.questionModalEvent);
  }

  cleanModal() {
    this.initQuestionModalEvent();
  }

  addAnswer() {
    this.questionModalEvent.answers.push({
      answer: '',
      correct: false
    });
  }

  removeAnswer(answerIndex: number) {
    this.questionModalEvent.answers.splice(answerIndex, 1);
  }

  codeBackgroundColor() {
    return this.questionModalEvent.code?.length ? '' : 'code-text-area-disable';
  }

  fileProgress(fileInput: any) {
    this.questionModalEvent.image = <File> fileInput.target.files[0];
    this.preview();
  }

  preview() {
    const mimeType = this.questionModalEvent.image.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.questionModalEvent.image);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    };
  }

  cleanImage(): void {
    this.questionModalEvent.image = null;
    this.previewUrl = null;
    this.fileUploadProgress = null;
  }

  canRemoveAnswer(): boolean {
    return this.questionModalEvent.answers.length < 3;
  }

  private initQuestionModalEvent() {
    this.questionModalEvent = {
      subjectId: this.subjectId,
      question: '',
      code: '',
      codeSyntax: '',
      image: null,
      answers: []
    };

    this.addAnswer();
    this.addAnswer();
  }
}
