import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-subject-name-modal',
  templateUrl: './subject-name-modal.component.html',
  styleUrls: ['./subject-name-modal.component.css']
})
export class SubjectNameModalComponent implements OnInit {

  @Input()
  textTitle: string;

  @Output()
  newSubjectNameEvent = new EventEmitter<string>();

  subjectName: string;

  constructor() { }

  ngOnInit(): void {
  }

  addSubject() {
    this.newSubjectNameEvent.emit(this.subjectName);
  }

  cleanModal() {
    this.subjectName = null;
  }
}
