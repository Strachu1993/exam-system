export interface QuestionDetailsResponse {
  id: string;
  subjectName: string;
  question: string;
  code: string;
  codeSyntax: string;
  totalAnswers: number;
  correctAnswers: number;
  image: string;
  answers: AnswerDetailsResponse[];
}

export interface AnswerDetailsResponse {
  answer: string;
  correct: boolean;
}
