export interface QuestionRequestDto {
  subjectId: string;
  question: string;
  code: string;
  codeSyntax: string;
  answers: AnswerRequestDto[];
}

export interface AnswerRequestDto {
  answer: string;
  correct: boolean;
}
