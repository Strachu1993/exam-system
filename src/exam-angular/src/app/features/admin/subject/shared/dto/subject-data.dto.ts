export interface SubjectDataDto {
  id: string;
  name: string;
}
