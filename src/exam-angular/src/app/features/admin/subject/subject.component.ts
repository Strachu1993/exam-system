import {Component} from '@angular/core';
import {SubjectHttpService} from './subject-http.service';

@Component({
  selector: 'app-subject',
  template: `
    <div class="container">
      <router-outlet></router-outlet>
    </div>`,
  providers: [
    SubjectHttpService
  ]
})
export class SubjectComponent {
}
