import { Component, OnInit } from '@angular/core';
import {SubjectHttpService} from '../subject-http.service';
import {SubjectListDto} from '../shared/dto/subject-list.dto';
import {ExamRouteService} from '../../../../commons/service/exam-route.service';

@Component({
  selector: 'app-subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.css']
})
export class SubjectListComponent implements OnInit {

  subjects: Array<SubjectListDto>;

  constructor(private subjectHttpService: SubjectHttpService,
              private examRouteService: ExamRouteService) { }

  ngOnInit(): void {
    this.loadAllSubjects();
  }

  addSubject(subjectName: string) {
    const body = {
      name: subjectName
    };

    this.subjectHttpService.addSubject(body).subscribe(() => {
      this.examRouteService.reloadPage();
    });
  }

  deleteSubject(subjectId: string) {
    this.subjectHttpService.deleteSubject(subjectId).subscribe(() => {
      this.examRouteService.reloadPage();
    });
  }

  private loadAllSubjects() {
    this.subjectHttpService.loadAllSubjects().subscribe((subjects: Array<SubjectListDto>) => {
      this.subjects = subjects;
    });
  }
}
