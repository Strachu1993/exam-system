import {Component, EventEmitter, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ExamRouteService} from '../../../../commons/service/exam-route.service';
import {SubjectHttpService} from '../subject-http.service';
import {SubjectDetailsResponse} from '../shared/dto/subject-details.response';
import {QuestionRequestDto} from '../shared/dto/question-request.dto';
import {QuestionModalEvent} from '../shared/dto/question-modal-event';

@Component({
  selector: 'app-subject-details',
  templateUrl: './subject-details.component.html',
  styleUrls: ['./subject-details.component.css']
})
export class SubjectDetailsComponent implements OnInit {

  subjectId: string;
  subjectDetailsResponse: SubjectDetailsResponse;

  constructor(private subjectHttpService: SubjectHttpService,
              private router: Router,
              private route: ActivatedRoute,
              private examRoute: ExamRouteService) {
  }

  ngOnInit(): void {
    this.getSubjectDetails();
  }

  addQuestion(event: QuestionModalEvent) {
    const image: File = event.image;
    const body: QuestionRequestDto = {
      subjectId: this.subjectId,
      question: event.question,
      code: event.code,
      codeSyntax: event.codeSyntax,
      answers: event.answers
    };
    this.subjectHttpService.addQuestion(body, image).subscribe(() => this.examRoute.reloadPage());
  }

  editSubject(subjectName: string) {
    const body = {
      name: subjectName
    };

    this.subjectHttpService.updateSubject(this.subjectId, body).subscribe(() => this.examRoute.reloadPage());
  }

  deleteQuestion(questionId: string) {
    this.subjectHttpService.deleteQuestion(questionId).subscribe(() => this.examRoute.reloadPage());
  }

  private getSubjectDetails() {
    this.route.paramMap.subscribe((param: Params) => {
      this.subjectId = param.get('id');
      this.subjectHttpService.getSubjectDetails(this.subjectId).subscribe((subjectDetailsResponse: SubjectDetailsResponse) => {
        this.subjectDetailsResponse = subjectDetailsResponse;
      });
    });
  }
}
