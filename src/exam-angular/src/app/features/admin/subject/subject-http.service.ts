import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {addressAdmin} from '../../../app.component';
import {Observable} from 'rxjs';
import {SubjectListDto} from './shared/dto/subject-list.dto';
import {SubjectDetailsResponse} from './shared/dto/subject-details.response';
import {QuestionRequestDto} from './shared/dto/question-request.dto';
import {QuestionDetailsResponse} from './shared/dto/question-details.response';

@Injectable({
  providedIn: 'root'
})
export class SubjectHttpService {

  private adminFindAllSubjectUrl = addressAdmin + '/subject/findAll';
  private adminDeleteSubjectUrl = addressAdmin + '/subject/delete';
  private adminDeleteAddUrl = addressAdmin + '/subject/add';
  private adminSubjectDetailsUrl = addressAdmin + '/subject';
  private adminSubjectUpdateUrl = addressAdmin + '/subject/{subjectId}/update';

  private adminQuestionDeleteUrl = addressAdmin + '/question/delete';
  private adminQuestionAddUrl = addressAdmin + '/question/add';

  private adminQuestionDetailsUrl = addressAdmin + '/question/details';
  private adminQuestionUpdateUrl = addressAdmin + '/question/update';

  constructor(private http: HttpClient) {
  }

  loadAllSubjects(): Observable<Array<SubjectListDto>> {
    return this.http.get<Array<SubjectListDto>>(this.adminFindAllSubjectUrl);
  }

  deleteSubject(subjectId: string): Observable<void> {
    return this.http.delete<void>(this.adminDeleteSubjectUrl + '/' + subjectId);
  }

  addSubject(body: any): Observable<void> {
    return this.http.post<void>(this.adminDeleteAddUrl, body);
  }

  getSubjectDetails(subjectId: string): Observable<SubjectDetailsResponse> {
    return this.http.get<SubjectDetailsResponse>(this.adminSubjectDetailsUrl + '/' + subjectId);
  }

  updateSubject(subjectId: string, body: any): Observable<void> {
    const url = this.adminSubjectUpdateUrl.replace('{subjectId}', subjectId);
    return this.http.put<void>(url, body);
  }

  deleteQuestion(questionId: string): Observable<void> {
    return this.http.delete<void>(this.adminQuestionDeleteUrl + '/' + questionId);
  }

  addQuestion(body: QuestionRequestDto, image: File): Observable<void> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };

    const formData = new FormData();
    formData.append('image', image);
    formData.append('questionRequest', new Blob([JSON.stringify(body)], {
      type: 'application/json',
    }));

    return this.http.post<void>(this.adminQuestionAddUrl, formData, httpOptions);
  }

  getQuestionDetails(questionId: string): Observable<QuestionDetailsResponse> {
    return this.http.get<QuestionDetailsResponse>(this.adminQuestionDetailsUrl + '/' + questionId);
  }

  updateQuestion(questionId: string, body: QuestionRequestDto, image: File): Observable<void> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };

    const formData = new FormData();
    formData.append('image', image);
    formData.append('questionRequest', new Blob([JSON.stringify(body)], {
      type: 'application/json',
    }));

    return this.http.put<void>(this.adminQuestionUpdateUrl + '/' + questionId, formData, httpOptions);
  }
}
