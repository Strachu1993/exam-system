import {Component, OnInit} from '@angular/core';
import {ExamHttpService} from '../exam-http.service';
import {ExamCreateDataDto} from '../shared/dto/exam-create-data.dto';
import {CreateExamRequest, QuestionDataRequest} from '../shared/dto/create-exam-request';
import {Router} from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-exam-add',
  templateUrl: './exam-add.component.html',
  styleUrls: ['./exam-add.component.css']
})
export class ExamAddComponent implements OnInit {

  timeMask = [/[0-2]/, /\d/, ':', /[0-5]/, /\d/];

  examCreateDataDto: ExamCreateDataDto;
  createExamRequest: CreateExamRequest;

  descriptionError = null;
  alertError = null;

  description: string = '';
  startDate: string;
  startTime: string;
  endTime: string;

  constructor(private examHttpService: ExamHttpService, private router: Router) {
  }

  ngOnInit(): void {
    this.initRequest();
    this.examHttpService.getCreateExamData().subscribe((x) => {
      this.examCreateDataDto = x;
    });
  }

  private initRequest() {
    this.createExamRequest = {
      description: '',
      end: '',
      questionData: [],
      start: '',
      studentGroupIds: []
    };
  }

  addQuestionRequest(event: QuestionDataRequest) {
    const index = this.createExamRequest.questionData.findIndex(x => x.subjectName === event.subjectName);

    if (index === -1) {
      this.createExamRequest.questionData.push(event);
    } else {
      this.createExamRequest.questionData[index] = event;
    }
  }

  removeQuestionRequest(index) {
    this.createExamRequest.questionData.splice(index, 1);
  }

  addStudentGroupIdRequest(event: string) {
    const index = this.createExamRequest.studentGroupIds.findIndex(x => x === event);

    if (index === -1) {
      this.createExamRequest.studentGroupIds.push(event);
    }
  }

  removeGroupRequest(index) {
    this.createExamRequest.studentGroupIds.splice(index, 1);
  }

  findGroupNameById(groupId: string) {
    const studentGroup = this.examCreateDataDto.studentGroupsData.find(x => x.id === groupId);
    return `Nazwa: ${studentGroup.name}, Podgrupa: ${studentGroup.subgroup}, Rok: ${studentGroup.year}`;
  }

  descriptionHasError() {
    return this.descriptionError ? 'error-input' : '';
  }

  createExam() {
    this.cleanErrors();

    const startDate = this.prepareDate(this.startDate, this.startTime);
    const endDate = this.prepareDate(this.startDate, this.endTime);

    this.createExamRequest.start = startDate;
    this.createExamRequest.end = endDate;
    this.createExamRequest.description = this.description;

    if (this.checkDescriptionLength() && this.checkQuestionSize() && this.checkGroupSize()) {
      this.examHttpService.createExam(this.createExamRequest).toPromise()
        .then(() => {
          this.router.navigate(['/admin/exam', {addedExam: 'Dodano egzamin'}]);
        }).catch(error => {
        const status = error.status;
        const message = error.message;
        this.alertError = `Status: ${status}, Message: ${message}`;
      });
    }
  }

  private checkDescriptionLength(): boolean {
    if (this.description.length > 200) {
      this.descriptionError = 'Maksymalna długość opisu wynosi 200 znaków';
      return false;
    }

    return true;
  }

  private checkQuestionSize(): boolean {
    if (this.createExamRequest.questionData.length < 1) {
      this.alertError = 'Musisz wybrac minimum jednen przemiot';
      return false;
    }

    return true;
  }

  private checkGroupSize(): boolean {
    if (this.createExamRequest.studentGroupIds.length < 1) {
      this.alertError = 'Musisz wybrac minimum jedną grupę';
      return false;
    }

    return true;
  }

  private prepareDate(date, time): string {
    return date + 'T' + time + ':00';
  }

  private cleanErrors() {
    this.descriptionError = null;
    this.alertError = null;
  }
}
