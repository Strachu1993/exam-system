import {Component, OnDestroy, OnInit} from '@angular/core';
import {ExamHttpService} from '../exam-http.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ExamSummaryResponse} from '../shared/dto/exam-summary-response';
import {CyclicCallingService} from '../../../../commons/service/cyclic-calling.service';
import {Subscription} from 'rxjs';
import {TimerService} from '../../../../commons/service/timer.service';
import {ExamStatus} from '../../../../commons/enum/exam-status.enum';

@Component({
  selector: 'app-exam-summary',
  templateUrl: './exam-summary.component.html',
  styleUrls: ['./exam-summary.component.css'],
  providers: [CyclicCallingService]
})
export class ExamSummaryComponent implements OnInit, OnDestroy {

  private timerSubscription: Subscription;

  examId: string;
  examSummaryResponse: ExamSummaryResponse;
  timer: number;
  restTime: string;

  constructor(private examHttpService: ExamHttpService,
              private router: Router,
              private route: ActivatedRoute,
              private cyclicCallingService: CyclicCallingService,
              private timerService: TimerService) {
  }

  ngOnInit(): void {
    this.cyclicCallingService.initLoop(5, 1, () => this.getExamSummary());
    this.timerSubscription = this.cyclicCallingService.changeTime.subscribe(value => {
      this.timer = value;
    });
  }

  ngOnDestroy() {
    this.timerSubscription.unsubscribe();
  }

  getFullGroupName(index: number): string {
    const group = this.examSummaryResponse?.scores[index]?.student?.group;
    return `${group.name} ${group.subgroup} ${group.year}`;
  }

  // TODO
  // getLinkToStudentDetails(index: number): string {
  //   const groupId = this.examSummaryResponse?.scores[index]?.student?.group?.id;
  //   const studentId = this.examSummaryResponse?.scores[index].id;
  //
  //   return `/admin/group/${groupId}/student/${studentId}`;
  // }

  private getExamSummary() {
    this.route.paramMap.subscribe((param: Params) => {
      this.examId = param.get('examId');
      this.examHttpService.getExamSummary(this.examId).subscribe((examSummaryResponse: ExamSummaryResponse) => {
        this.examSummaryResponse = examSummaryResponse;
        this.setTimer();
      });
    });
  }

  private setTimer(): void {
    if (this.examSummaryResponse.examStatus === ExamStatus.IN_PROGRESS) {
      this.timerService.setRestTimeInterval(this.examSummaryResponse?.end, 0, () => {});
      this.timerService.changeTime.subscribe(restTime => this.restTime = restTime);
    }
  }
}
