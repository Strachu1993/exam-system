import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ExamListDto} from './shared/dto/exam-list.dto';
import {addressAdmin} from '../../../app.component';
import {ExamCreateDataDto} from './shared/dto/exam-create-data.dto';
import {CreateExamRequest, QuestionDataRequest} from './shared/dto/create-exam-request';
import {ExamDetailsDto} from './shared/dto/exam-details.dto';
import {ExamSummaryResponse} from './shared/dto/exam-summary-response';

@Injectable({
  providedIn: 'root'
})
export class ExamHttpService {

  private adminExamFindGetUrl = addressAdmin + '/exam/find';
  private adminGetExamCreateDataGetUrl = addressAdmin + '/exam/createData';
  private adminExamExamPostUrl = addressAdmin + '/exam/create';
  private adminExamDetailsGetUrl = addressAdmin + '/exam/{examId}';
  private adminCreateStudentsPdfFromExamPostUrl = addressAdmin + '/pdf/exam/{examId}/students';
  private adminCreateExamScoresPdfFromExamPostUrl = addressAdmin + '/pdf/exam/{examId}/scores';
  private adminDeleteGroupFromExamPatchUrl = addressAdmin + '/exam/delete/{examId}/group/{groupId}';
  private adminDeleteSubjectFromExamPatchUrl = addressAdmin + '/exam/delete/{examId}/subject/{subjectId}';
  private adminDeleteFromExamDeleteUrl = addressAdmin + '/exam/delete/{examId}';
  private adminExamEditPatchUrl = addressAdmin + '/exam/update/{examId}';
  private adminExamEditAddQuestionPatchUrl = addressAdmin + '/exam/update/{examId}/add/questions';
  private adminExamEditAddGroupPatchUrl = addressAdmin + '/exam/update/{examId}/add/group/{groupId}';
  private adminExamSummaryGetUrl = addressAdmin + '/exam/{examId}/summary';

  constructor(private http: HttpClient) {
  }

  getExams(): Observable<Array<ExamListDto>> {
    return this.http.get<Array<ExamListDto>>(this.adminExamFindGetUrl);
  }

  getCreateExamData(): Observable<ExamCreateDataDto> {
    return this.http.get<ExamCreateDataDto>(this.adminGetExamCreateDataGetUrl);
  }

  createExam(createExamRequest: CreateExamRequest): Observable<void> {
    return this.http.post<void>(this.adminExamExamPostUrl, createExamRequest);
  }

  getExamDetails(examId: string): Observable<ExamDetailsDto> {
    const url = this.adminExamDetailsGetUrl.replace('{examId}', examId);
    return this.http.get<ExamDetailsDto>(url);
  }

  getStudentsPdfFromExam(examId: string) {
    const url = this.adminCreateStudentsPdfFromExamPostUrl.replace('{examId}', examId);
    return this.http.post(url, null, {
      responseType: 'arraybuffer'
    });
  }

  getExamScoresPdf(examId: string) {
    const url = this.adminCreateExamScoresPdfFromExamPostUrl.replace('{examId}', examId);
    return this.http.post(url, null, {
      responseType: 'arraybuffer'
    });
  }

  deleteGroup(examId: string, groupId: string): Observable<void> {
    const url = this.adminDeleteGroupFromExamPatchUrl.replace('{examId}', examId).replace('{groupId}', groupId);
    return this.http.patch<void>(url, null);
  }

  deleteSubject(examId: string, subjectId: string) {
    const url = this.adminDeleteSubjectFromExamPatchUrl.replace('{examId}', examId).replace('{subjectId}', subjectId);
    return this.http.patch<void>(url, null);
  }

  deleteExam(examId: string) {
    const url = this.adminDeleteFromExamDeleteUrl.replace('{examId}', examId);
    return this.http.delete(url);
  }

  editExam(examId: string, body: any): Observable<void> {
    const url = this.adminExamEditPatchUrl.replace('{examId}', examId);
    return this.http.patch<void>(url, body);
  }

  editExamAddQuestion(examId: string, body: QuestionDataRequest): Observable<void> {
    const url = this.adminExamEditAddQuestionPatchUrl.replace('{examId}', examId);
    return this.http.patch<void>(url, body);
  }

  editExamAddGroup(examId: string, groupId: string): Observable<void> {
    const url = this.adminExamEditAddGroupPatchUrl.replace('{examId}', examId).replace('{groupId}', groupId);
    return this.http.patch<void>(url, null);
  }

  getExamSummary(examId: string): Observable<ExamSummaryResponse> {
    const url = this.adminExamSummaryGetUrl.replace('{examId}', examId);
    return this.http.get<ExamSummaryResponse>(url);
  }
}
