import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ExamCreateQuestionDataDto} from '../../dto/exam-create-data.dto';
import {QuestionDataRequest} from '../../dto/create-exam-request';

declare const $: any;

@Component({
  selector: 'app-exam-add-question-modal',
  templateUrl: './exam-add-question-modal.component.html',
  styleUrls: ['./exam-add-question-modal.component.css']
})
export class ExamAddQuestionModalComponent {

  @Output()
  eventQuestionDataRequest = new EventEmitter<QuestionDataRequest>();

  @Input()
  examCreateQuestionDataDto: Array<ExamCreateQuestionDataDto>;

  questionSizeMask = [/\d/, /\d/, /\d/];

  selected: string = null;
  maxQuestionSize: number = null;
  questionSize: number = null;
  selectedQuestion: ExamCreateQuestionDataDto = null;

  questionErrorMessage: string = null;
  selectErrorMessage: string = null;

  constructor() {
  }

  selectOption() {
    this.selectedQuestion = this.examCreateQuestionDataDto
      .find(x => x.name === this.selected);

    this.maxQuestionSize = this.selectedQuestion.numberOfQuestions;
    this.cleanErrorsAndQuestionSize();
  }

  checkQuestionSize(): boolean {
    if (this.isQuestionSizeIsNotZero()) {
      this.questionErrorMessage = `Liczba pytań nie może wynosić 0`;
      return false;
    } else if (this.isQuestionsSizeTooBig()) {
      this.questionErrorMessage = `Maksymalna liczba pytań to ${this.maxQuestionSize} a podałeś ${this.questionSize}`;
      return false;
    }

    return true;
  }

  checkQuestionSelected(): boolean {
    if (!this.selectedQuestion) {
      this.selectErrorMessage = 'Wybierz kategorie pytań';
      return false;
    }

    return true;
  }

  questionSizeHasError() {
    return this.hasError(this.questionErrorMessage);
  }

  selectHasError() {
    return this.hasError(this.selectErrorMessage);
  }

  cleanModal() {
    this.selected = null;
    this.maxQuestionSize = null;
    this.selectedQuestion = null;
    this.cleanErrorsAndQuestionSize();
  }

  addQuestions() {
    this.cleanErrorMessages();

    if (this.checkQuestionSelected() && this.checkQuestionSize()) {
      this.emitQuestionDataRequest();
      this.closeModal();
      this.cleanModal();
    }
  }

  private hasError(value: string) {
    return value ? 'error-input' : '';
  }

  private emitQuestionDataRequest() {
    const questionDataRequest: QuestionDataRequest = {
      subjectName: this.selectedQuestion.name,
      numberOfQuestions: this.questionSize
    };

    this.eventQuestionDataRequest.emit(questionDataRequest);
  }

  private cleanErrorsAndQuestionSize() {
    this.questionSize = null;
    this.cleanErrorMessages();
  }

  private cleanErrorMessages() {
    this.questionErrorMessage = '';
    this.selectErrorMessage = '';
  }

  private closeModal() {
    $('#addQuestion').modal('toggle');
  }

  private isQuestionsSizeTooBig(): boolean {
    return +this.questionSize > this.maxQuestionSize;
  }

  private isQuestionSizeIsNotZero(): boolean {
    return +this.questionSize === 0;
  }
}
