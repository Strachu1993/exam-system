export interface CreateExamRequest {
  start: string;
  end: string;
  description: string;
  studentGroupIds: string[];
  questionData: QuestionDataRequest[];
}

export interface QuestionDataRequest {
  subjectName: string;
  numberOfQuestions: number;
}
