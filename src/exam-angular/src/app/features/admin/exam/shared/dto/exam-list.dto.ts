import {ExamStatus} from '../../../../../commons/enum/exam-status.enum';

export interface ExamListDto {
  examId?: string;
  start?: Date;
  end?: Date;
  examStatus?: ExamStatus;
  questionSize?: number;
  studentGroupsSize?: number;
  description?: string;
}
