import {Subgroup} from '../../../../../commons/enum/subgroup.enum';

export interface ExamCreateDataDto {
  studentGroupsData: StudentGroupData[];
  questionData: ExamCreateQuestionDataDto[];
}

export interface StudentGroupData {
    id: string;
    name: string;
    year: string;
    subgroup: Subgroup;
}

export interface ExamCreateQuestionDataDto {
  subjectId: string;
  name: string;
  numberOfQuestions: number;
}
