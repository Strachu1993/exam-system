import {Subgroup} from '../../../../../commons/enum/subgroup.enum';
import {ExamStatus} from '../../../../../commons/enum/exam-status.enum';

export interface ExamDetailsDto {
  examId: string;
  start: string;
  end: string;
  status: ExamStatus;
  description: string;
  groups: GroupData[];
  subjects: SubjectsData[];
}

export interface SubjectsData {
  id: string;
  name: string;
  questionSize: number;
}

export interface GroupData {
  id: string;
  name: string;
  subgroup: Subgroup;
  year: string;
}
