import {ExamStatus} from '../../../../../commons/enum/exam-status.enum';
import {Subgroup} from '../../../../../commons/enum/subgroup.enum';
import {Rating} from '../../../../../commons/enum/rating.enum';

export interface ExamSummaryResponse {
  id: string;
  start: Date;
  end: Date;
  description: string;
  examStatus: ExamStatus;
  scores: ScoreSummaryResponse[];
}

export interface ScoreSummaryResponse {
  id: string;
  scoreStatus: string;
  score: string;
  rating: Rating;
  student: StudentSummaryResponse;
}

export interface StudentSummaryResponse {
  numberAlbum: string;
  name: string;
  surname: string;
  group: GroupSummaryResponse;
}

export interface GroupSummaryResponse {
  id: string;
  name: string;
  subgroup: Subgroup;
  year: string;
}
