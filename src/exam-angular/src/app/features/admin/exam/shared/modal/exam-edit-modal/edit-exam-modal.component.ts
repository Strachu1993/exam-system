import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ExamHttpService} from '../../../exam-http.service';
import {HttpErrorResponse} from '@angular/common/http';
import {CustomError} from '../../../../../../commons/http/custom-error';

@Component({
  selector: 'app-edit-exam-modal',
  templateUrl: './edit-exam-modal.component.html',
  styleUrls: ['./edit-exam-modal.component.css']
})
export class EditExamModalComponent {

  timeMask = [/[0-2]/, /\d/, ':', /[0-5]/, /\d/];

  @Output()
  successEmit = new EventEmitter<void>();

  @Output()
  errorEmit = new EventEmitter<string>();

  @Input()
  examId: string;
  description: string = '';
  startDate: string;
  startTime: string;
  endTime: string;

  constructor(private examHttpService: ExamHttpService) {
  }

  editExam() {
    const body = {
      start: this.prepareDate(this.startDate, this.startTime),
      end: this.prepareDate(this.startDate, this.endTime),
      description: this.description
    };

    this.examHttpService.editExam(this.examId, body).subscribe(() => {
      this.successEmit.emit();
    },
      (error: HttpErrorResponse) => {
        this.errorEmit.emit('Wystąpił bład');
    });
  }

  cleanModal() {
    this.description = '';
    this.startDate = '';
    this.startTime = '';
    this.endTime = '';
  }

  private prepareDate(date, time): string {
    return date + 'T' + time + ':00';
  }
}
