import {Component, EventEmitter, Input, Output} from '@angular/core';
import {StudentGroupData} from '../../dto/exam-create-data.dto';

declare const $: any;

@Component({
  selector: 'app-exam-add-student-group-modal',
  templateUrl: './exam-add-student-group-modal.component.html',
  styleUrls: ['./exam-add-student-group-modal.component.css']
})
export class ExamAddStudentGroupModalComponent {

  @Output()
  eventStudentGroupId = new EventEmitter<string>();

  @Input()
  studentGroupData: Array<StudentGroupData>;

  selectErrorMessage: string = null;
  selectedGroupsId: string = null;

  constructor() {
  }

  selectHasError() {
    return this.selectErrorMessage ? 'error-input' : '';
  }

  cleanModal() {
    this.selectErrorMessage = null;
    this.selectedGroupsId = null;
  }

  addStudentGroup() {
    this.selectErrorMessage = null;

    if (this.checkQuestionSelected()) {
      this.eventStudentGroupId.emit(this.selectedGroupsId);
      this.closeModal();
      this.cleanModal();
    }
  }

  private checkQuestionSelected(): boolean {
    if (!this.selectedGroupsId) {
      this.selectErrorMessage = 'Wybierz grupę studencką';
      return false;
    }

    return true;
  }

  private closeModal() {
    $('#addStudentGroup').modal('toggle');
  }
}
