import {Component} from '@angular/core';
import {ExamHttpService} from './exam-http.service';

@Component({
  selector: 'app-exam',
  template: `
    <div class="container">
        <router-outlet></router-outlet>
    </div>`,
  providers: [
    ExamHttpService
  ]
})
export class ExamComponent {
}
