import {Component} from '@angular/core';
import {ExamListDto} from '../shared/dto/exam-list.dto';
import {ExamHttpService} from '../exam-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ExamRouteService} from '../../../../commons/service/exam-route.service';
import {ExamStatus} from '../../../../commons/enum/exam-status.enum';

@Component({
  selector: 'app-exam-list',
  templateUrl: './exam-list.component.html',
  styleUrls: ['./exam-list.component.css']
})
export class ExamListComponent {

  exams: Array<ExamListDto>;
  successAlert: string = null;

  constructor(private examHttpService: ExamHttpService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private examRoute: ExamRouteService) {
    this.activatedRoute.params.subscribe(params => {
      const addedExamParam = params['addedExam'];
          if (addedExamParam) {
            this.successAlert = addedExamParam;
          }
    });
    examHttpService.getExams().subscribe((x) => {
      this.exams = x;
    });
  }

  hasCreatedStatus(status: ExamStatus): boolean {
    return status.valueOf() !== ExamStatus.CREATED.valueOf();
  }

  deleteExam(examId: string) {
    this.examHttpService.deleteExam(examId).subscribe(() => {
      this.examRoute.reloadPage();
    });
  }
}
