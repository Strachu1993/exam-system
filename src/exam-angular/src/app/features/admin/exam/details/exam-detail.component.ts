import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ExamHttpService} from '../exam-http.service';
import {ExamDetailsDto} from '../shared/dto/exam-details.dto';
import {saveAs} from 'file-saver';
import {ExamStatus} from '../../../../commons/enum/exam-status.enum';
import {ExamCreateDataDto} from '../shared/dto/exam-create-data.dto';
import {QuestionDataRequest} from '../shared/dto/create-exam-request';

@Component({
  selector: 'app-exam-detail',
  templateUrl: './exam-detail.component.html',
  styleUrls: ['./exam-detail.component.css']
})
export class ExamDetailComponent implements OnInit {
  private examId: string;
  errorAlert: string = '';
  examDetailsDto: ExamDetailsDto;
  examCreateDataDto: ExamCreateDataDto;

  constructor(private examHttpService: ExamHttpService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.loadData();
  }

  editExamQuestion(body: QuestionDataRequest) {
    this.examHttpService.editExamAddQuestion(this.examId, body).subscribe(() => {
      this.getExamDetails();
    });
  }

  editExamStudentGroup(groupId: string) {
    this.examHttpService.editExamAddGroup(this.examId, groupId).subscribe(() => {
      this.getExamDetails();
    });
  }

  deleteGroup(groupId: string): void {
    this.examHttpService.deleteGroup(this.examId, groupId).subscribe(() => {
      this.getExamDetails();
    });
  }

  deleteSubject(subjectId: string): void {
    this.examHttpService.deleteSubject(this.examId, subjectId).subscribe(() => {
      this.getExamDetails();
    });
  }

  getStudentsPdfFromExam() {
    const fileName = 'StudentsFromExam_{examId}.pdf'.replace('{examId}', this.examId);
    this.examHttpService.getStudentsPdfFromExam(this.examId).subscribe(data => {
      saveAs(new Blob([data], {type: 'application/pdf'}), fileName);
    });
  }

  getExamScoresPdfFromExam() {
    const fileName = 'ExamScores_{examId}.pdf'.replace('{examId}', this.examId);
    this.examHttpService.getExamScoresPdf(this.examId).subscribe(data => {
      saveAs(new Blob([data], {type: 'application/pdf'}), fileName);
    });
  }

  examHasCreatedStatus(): boolean {
    return this.examDetailsDto.status.valueOf() !== ExamStatus.CREATED.valueOf();
  }

  examHasFinishedStatus(): boolean {
    return this.examDetailsDto.status.valueOf() !== ExamStatus.FINISH.valueOf();
  }

  showErrorAlert(errorMessage: string) {
    this.errorAlert = errorMessage;
  }

  loadData() {
    this.getExamDetails();
    this.getEditExamData();
  }

  private getEditExamData() {
    this.examHttpService.getCreateExamData().subscribe((examCreateDataDto: ExamCreateDataDto) => {
      this.examCreateDataDto = examCreateDataDto;
    });
  }

  private getExamDetails() {
    this.route.paramMap.subscribe((param: Params) => {
      this.examId = param.get('id');
      this.examHttpService.getExamDetails(this.examId).subscribe((examDetailsResponse: ExamDetailsDto) => {
        this.examDetailsDto = examDetailsResponse;
      });
    });
  }
}
