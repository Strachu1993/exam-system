import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './features/no-auth/login.component';
import {PageNotFoundComponent} from './features/page-not-found/page-not-found.component';
import {LoginService} from './features/no-auth/login.service';
import {AuthAdminGuardService} from './commons/permission/auth-admin-guard.service';
import {JwtTokenInterceptor} from './commons/http/JwtToken.interceptor';
import {AdminComponent} from './features/admin/admin.component';
import {AdminMenuBarComponent} from './features/admin/menu-bar/admin-menu-bar.component';
import {ExamComponent} from './features/admin/exam/exam.component';
import {ExamDetailComponent} from './features/admin/exam/details/exam-detail.component';
import {ExamAddComponent} from './features/admin/exam/create/exam-add.component';
import {ExamListComponent} from './features/admin/exam/list/exam-list.component';
import {AdminHomeComponent} from './features/admin/admin-home/admin-home.component';
import {TextMaskModule} from 'angular2-text-mask';
import {ExamAddQuestionModalComponent} from './features/admin/exam/shared/modal/question-add-modal/exam-add-question-modal.component';
import {ExamAddStudentGroupModalComponent} from './features/admin/exam/shared/modal/group-add-modal/exam-add-student-group-modal.component';
import {StudentGroupComponent} from './features/admin/student-group/student-group.component';
import {StudentGroupDetailsComponent} from './features/admin/student-group/details/student-group-details.component';
import {StudentGroupListComponent} from './features/admin/student-group/list/student-group-list.component';
import {SubjectComponent} from './features/admin/subject/subject.component';
import {SubjectDetailsComponent} from './features/admin/subject/details/subject-details.component';
import {SubjectListComponent} from './features/admin/subject/list/subject-list.component';
import {ExamRouteService} from './commons/service/exam-route.service';
import {EditExamModalComponent} from './features/admin/exam/shared/modal/exam-edit-modal/edit-exam-modal.component';
import {SubjectNameModalComponent} from './features/admin/subject/shared/modal/subject-name-modal/subject-name-modal.component';
import {QuestionModalComponent} from './features/admin/subject/shared/modal/question-modal/question-modal.component';
import {QuestionDetailsComponent} from './features/admin/subject/question/details/question-details.component';
import {StudentGroupModalComponent} from './features/admin/student-group/shared/modal/student-group-modal/student-group-modal.component';
import {StudentAccountModalComponent} from './features/admin/student-group/shared/modal/student-account-modal/student-account-modal.component';
import {StudentDetailsComponent} from './features/admin/student-group/student/details/student-details.component';
import {StudentScoreDetailsComponent} from './features/admin/student-group/student/score-details/student-score-details.component';
import {ExamSummaryComponent} from './features/admin/exam/summary/exam-summary.component';
import {StudentMenuBarComponent} from './features/student/menu-bar/student-menu-bar.component';
import {StudentComponent} from './features/student/student.component';
import {StudentExamListComponent} from './features/student/exam/list/student-exam-list.component';
import {StudentExamVoteComponent} from './features/student/exam/vote/student-exam-vote.component';
import {StudentExamSummaryComponent} from './features/student/exam/summary/student-exam-summary.component';
import {StudentExamComponent} from './features/student/exam/student-exam.component';
import { RatingComponent } from './commons/components/rating-text/rating.component';
import {RatingPipe} from './commons/components/rating-text/rating.pipe';
import { ExamStatusPipe } from './commons/components/exam-status/exam-status.pipe';
import { ExamStatusComponent } from './commons/components/exam-status/exam-status.component';
import {HighlightJsModule} from 'ngx-highlight-js';
import { AdminSystemComponent } from './features/admin/setting/system/system/admin-system.component';
import { AdminRatingComponent } from './features/admin/setting/system/system/rating/admin-rating.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    LoginComponent,
    AdminComponent,
    AdminMenuBarComponent,
    ExamComponent,
    ExamDetailComponent,
    ExamAddComponent,
    ExamListComponent,
    AdminHomeComponent,
    ExamAddQuestionModalComponent,
    ExamAddStudentGroupModalComponent,
    StudentGroupComponent,
    StudentGroupDetailsComponent,
    StudentGroupListComponent,
    SubjectComponent,
    SubjectDetailsComponent,
    SubjectListComponent,
    EditExamModalComponent,
    SubjectNameModalComponent,
    QuestionModalComponent,
    QuestionDetailsComponent,
    StudentGroupModalComponent,
    StudentAccountModalComponent,
    StudentDetailsComponent,
    StudentScoreDetailsComponent,
    ExamSummaryComponent,
    StudentMenuBarComponent,
    StudentComponent,
    StudentExamListComponent,
    StudentExamVoteComponent,
    StudentExamSummaryComponent,
    StudentExamComponent,
    RatingPipe,
    RatingComponent,
    ExamStatusPipe,
    ExamStatusComponent,
    AdminSystemComponent,
    AdminRatingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    TextMaskModule,
    HighlightJsModule
  ],
  providers: [
    LoginService,
    AuthAdminGuardService,
    ExamRouteService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtTokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
