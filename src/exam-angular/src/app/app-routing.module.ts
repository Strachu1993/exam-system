import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './features/no-auth/login.component';
import {PageNotFoundComponent} from './features/page-not-found/page-not-found.component';
import {AdminComponent} from './features/admin/admin.component';
import {ExamComponent} from './features/admin/exam/exam.component';
import {AuthAdminGuardService} from './commons/permission/auth-admin-guard.service';
import {ExamDetailComponent} from './features/admin/exam/details/exam-detail.component';
import {ExamAddComponent} from './features/admin/exam/create/exam-add.component';
import {ExamListComponent} from './features/admin/exam/list/exam-list.component';
import {AdminHomeComponent} from './features/admin/admin-home/admin-home.component';
import {StudentGroupComponent} from './features/admin/student-group/student-group.component';
import {StudentGroupListComponent} from './features/admin/student-group/list/student-group-list.component';
import {StudentGroupDetailsComponent} from './features/admin/student-group/details/student-group-details.component';
import {SubjectComponent} from './features/admin/subject/subject.component';
import {SubjectDetailsComponent} from './features/admin/subject/details/subject-details.component';
import {SubjectListComponent} from './features/admin/subject/list/subject-list.component';
import {QuestionDetailsComponent} from './features/admin/subject/question/details/question-details.component';
import {StudentDetailsComponent} from './features/admin/student-group/student/details/student-details.component';
import {StudentScoreDetailsComponent} from './features/admin/student-group/student/score-details/student-score-details.component';
import {ExamSummaryComponent} from './features/admin/exam/summary/exam-summary.component';
import {StudentComponent} from './features/student/student.component';
import {AuthStudentGuardService} from './commons/permission/auth-student-guard.service';
import {StudentExamListComponent} from './features/student/exam/list/student-exam-list.component';
import {StudentExamVoteComponent} from './features/student/exam/vote/student-exam-vote.component';
import {StudentExamSummaryComponent} from './features/student/exam/summary/student-exam-summary.component';
import {StudentExamComponent} from './features/student/exam/student-exam.component';
import {AdminSystemComponent} from './features/admin/setting/system/system/admin-system.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthAdminGuardService],
    children: [
      {
        path: '',
        component: AdminHomeComponent
      },
      {
        path: 'exam',
        component: ExamComponent,
        children: [
          {
            path: '',
            component: ExamListComponent
          },
          {
            path: 'add',
            component: ExamAddComponent
          },
          {
            path: ':id',
            component: ExamDetailComponent
          },
          {
            path: ':examId/summary',
            component: ExamSummaryComponent
          }
        ]
      },
      {
        path: 'group',
        component: StudentGroupComponent,
        children: [
          {
            path: '',
            component: StudentGroupListComponent
          },
          {
            path: ':id',
            component: StudentGroupDetailsComponent
          },
          {
            path: ':id/student/:studentId',
            component: StudentDetailsComponent
          },
          {
            path: ':id/student/:studentId/score-details/:scoreId',
            component: StudentScoreDetailsComponent
          }
        ]
      },
      {
        path: 'subject',
        component: SubjectComponent,
        children: [
          {
            path: '',
            component: SubjectListComponent
          },
          {
            path: ':id',
            component: SubjectDetailsComponent,
          },
          {
            path: ':id/question/:questionId',
            component: QuestionDetailsComponent
          }
        ]
      },
      {
        path: 'system',
        component: AdminSystemComponent
      },
    ]
  },
  {
    path: 'student',
    component: StudentComponent,
    canActivate: [AuthStudentGuardService],
    children: [
      {
        path: '',
        redirectTo: '/student/exam',
        pathMatch: 'full'
      },
      {
        path: 'exam',
        component: StudentExamComponent,
        children: [
          {
            path: '',
            component: StudentExamListComponent
          },
          {
            path: ':studentScoreId/vote',
            component: StudentExamVoteComponent,
          },
          {
            path: ':studentScoreId/summary',
            component: StudentExamSummaryComponent,
          }
        ]
      }
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
