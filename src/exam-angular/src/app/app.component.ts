import { Component } from '@angular/core';

export const authHeader = 'Authorization';
export const storageTokenType = 'tokenType';
export const storageAuth = 'token';
export const storageRole = 'role';

const backEndUrl = 'http://localhost:8080';
export const addressAuth = backEndUrl + '/api/no-auth/login';
export const addressAdmin = backEndUrl + '/api/admin';
export const addressStudent = backEndUrl + '/api/student';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'exam-angular';
}
