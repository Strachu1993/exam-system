import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {storageRole} from '../../app.component';
import {Role} from './role.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthLoginGuardService implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(route: import('@angular/router').ActivatedRouteSnapshot, state: import('@angular/router').RouterStateSnapshot): boolean {
    const userRole = localStorage.getItem(storageRole);

    if (userRole) {
      if (userRole === Role.ROLE_STUDENT) {
        this.router.navigate(['/student']);
      } else if (userRole === Role.ROLE_ADMIN) {
        this.router.navigate(['/admin']);
      }
    }

    return true;
  }
}
