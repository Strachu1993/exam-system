import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { storageRole } from '../../app.component';
import { Role } from './role.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthAdminGuardService implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: import('@angular/router').ActivatedRouteSnapshot, state: import('@angular/router').RouterStateSnapshot): boolean {
    const userRole = localStorage.getItem(storageRole);

    if (userRole && userRole === Role.ROLE_ADMIN) {
        return true;
    }

    this.router.navigate(['/login', { pageNotAvailable: route.component['name'] }]);
    return false;
  }
}
