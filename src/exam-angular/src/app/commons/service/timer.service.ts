import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService implements OnDestroy {

  private interval;
  changeTime: Subject<string> = new Subject<string>();

  ngOnDestroy() {
    this.clean();
  }

  setRestTimeInterval(finishDate: Date, minusSec, action) {
    this.clean();
    this.interval = setInterval(() => {
      this.calculateRestTime(finishDate, minusSec, action);
    }, 1000);
  }

  private calculateRestTime(finishDate: Date, minusSec, action) {
    const endDate = new Date(finishDate);
    endDate.setSeconds(endDate.getSeconds() - minusSec);

    const currentDate = new Date(Date.now());
    const diff = endDate.getTime() - currentDate.getTime();

    if (diff <= 0) {
      action();
      this.changeTime.next(null);
      this.clean();
    } else {
      const restSeconds = diff / 1000;
      const restMinutes = restSeconds / 60;
      const restHours = restMinutes / 60;

      const secLeft = this.fillByZero(Math.floor(restSeconds % 60));
      const minLeft = this.fillByZero(Math.floor(restMinutes % 60));
      const hourLeft = this.fillByZero(Math.floor(restHours));

      const newTime = `godziny: ${hourLeft}, minuty: ${minLeft}, sekundy: ${secLeft}`;
      this.changeTime.next(newTime);
    }
  }

  private clean(): void {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  private fillByZero(value: number): string {
    return value < 10 ? '0' + value : value.toString();
  }
}
