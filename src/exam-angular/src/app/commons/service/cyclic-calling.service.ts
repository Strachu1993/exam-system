import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CyclicCallingService implements OnDestroy {

  private timer: number;
  private delay: number;
  private interval;

  changeTime: Subject<number> = new Subject<number>();

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  initLoop(timeInSec: number, delayInSec: number, action): void {
    this.delay = delayInSec * 1000;
    this.timer = timeInSec;
    action();

    this.interval = setInterval(() => {
      this.changeTime.next(this.timer);
      if (this.timer === 0) {
        action();
        this.timer = timeInSec;
      } else {
        this.timer--;
      }
    }, this.delay);
  }
}
