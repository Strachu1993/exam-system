import { Pipe, PipeTransform } from '@angular/core';
import {Rating} from '../../enum/rating.enum';

@Pipe({
  name: 'ratingFormatter'
})
export class RatingPipe implements PipeTransform {

  transform(rating: Rating, ...args: unknown[]): string {
    switch (rating) {
      case Rating.DBD:
        return 'Bardzo dobry';
      case Rating.DB_PLUS:
        return 'Dobry plus';
      case Rating.DB:
        return 'Dobry';
      case Rating.DST_PLUS:
        return `Dostateczny plus`;
      case Rating.DST:
        return 'Dostateczny';
      case Rating.NDST:
        return 'Niedostateczny';
    }
    return null;
  }
}
