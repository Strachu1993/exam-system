import {Component, Input} from '@angular/core';
import {ExamStatus} from '../../enum/exam-status.enum';

@Component({
  selector: 'app-exam-status',
  templateUrl: './exam-status.component.html',
  styleUrls: ['./exam-status.component.css']
})
export class ExamStatusComponent {

  @Input()
  examStatus: ExamStatus;
}
