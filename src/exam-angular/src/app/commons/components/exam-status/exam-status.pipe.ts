import {Pipe, PipeTransform} from '@angular/core';
import {ExamStatus} from '../../enum/exam-status.enum';

@Pipe({
  name: 'examStatusFormatter'
})
export class ExamStatusPipe implements PipeTransform {

  transform(status: ExamStatus, ...args: unknown[]): string {
    switch (status) {
      case ExamStatus.FINISH:
        return 'Zakończony';
      case ExamStatus.IN_PROGRESS:
        return 'W trakcie';
      case ExamStatus.CREATED:
        return 'Nowy';
    }
    return null;
  }
}
