export interface CustomError {
  requestPath: string;
  exceptionType: string;
  httpStatus: string;
  message: string;
}
