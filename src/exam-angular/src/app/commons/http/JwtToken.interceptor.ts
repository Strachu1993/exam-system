import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { storageTokenType, storageAuth, authHeader } from 'src/app/app.component';

export class JwtTokenInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const tokenType: string = localStorage.getItem(storageTokenType);
    const token: string = localStorage.getItem(storageAuth);

    if (tokenType && token) {
      const tokenToSend = `${tokenType} ${token}`;
      const jwtHeader = new HttpHeaders().set(authHeader, tokenToSend);
      const reqCloned = req.clone({ headers: jwtHeader });

      return next.handle(reqCloned);
    }

    return next.handle(req);
  }
}
