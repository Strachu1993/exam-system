export enum StudentExamStatus {
  CREATE = 'CREATE',
  IN_PROGRESS = 'IN_PROGRESS',
  FINISH = 'FINISH'
}
