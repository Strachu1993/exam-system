export enum Rating {
  DBD = 'DBD',
  DB_PLUS = 'DB_PLUS',
  DB = 'DB',
  DST_PLUS = 'DST_PLUS',
  DST = 'DST',
  NDST = 'NDST'
}
