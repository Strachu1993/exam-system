package com.exam.commons.feature.character_generator;

import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertTrue;

class CharacterGeneratorTest {

    private static final int MAX_LENGTH = 6;
    private static final int MIN_LENGTH = 4;
    private static final char[] availableChars = {'a', 'b', 'c', 'd', 'e', '1', '2', '3', '4', '5'};

    private final Random random = new Random();
    private final CharacterGenerator characterGenerator = new CharacterGenerator(random);

    @Test
    void shouldGenerateTextWithLengthBetweenFourAndSixCharacters() {
        // Given
        ReflectionTestUtils.setField(characterGenerator, "availableChars", availableChars);

        // When
        final int generatedTextLength = characterGenerator.generateText().length();

        // Then
        assertTrue(generatedTextLength <= MAX_LENGTH, "Error, text random is too high");
        assertTrue(generatedTextLength >= MIN_LENGTH, "Error, text random is too low");
    }
}