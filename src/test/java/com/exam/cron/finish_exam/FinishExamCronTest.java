package com.exam.cron.finish_exam;

import org.junit.jupiter.api.Test;

import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.then;

class FinishExamCronTest {

    private final FinishExamService examEndService = mock(FinishExamService.class);

    @Test
    void shouldCallFinishSelectedExamsMethod() {
        // given
        final FinishExamCron cron = new FinishExamCron(examEndService);

        // when
        cron.checkInProgressExams();

        // then
        then(examEndService).should().finishSelectedExams();
    }
}