package com.exam.feature.admin.student_group.create;

import com.exam.commons.enums.Subgroup;
import com.exam.commons.exception.custom.validate.StudentGroupAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.http.HttpStatus;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

class StudentGroupCreateServiceTest {

    private static final String NAME = "New name";
    private static final String YEAR = "2004";
    private static final Subgroup SUBGROUP = Subgroup.A;

    private final StudentGroupRepository studentGroupRepository = mock(StudentGroupRepository.class);
    private StudentGroupCreateService studentGroupCreateService;

    @BeforeEach
    void beforeEach() {
        studentGroupCreateService = new StudentGroupCreateService(studentGroupRepository);
    }

    @Test
    void shouldCreateStudentGroup() {
        // given
        final StudentGroupCreateRequest studentGroupCreateRequest = prepareStudentGroupCreateRequest();
        final StudentGroupEntity studentGroupEntity = prepareStudentGroupEntity();
        given(studentGroupRepository.countByNameAndYearAndSubgroup(NAME, YEAR, SUBGROUP)).willReturn(0);
        given(studentGroupRepository.save(any(StudentGroupEntity.class))).willReturn(studentGroupEntity);

        // when
        studentGroupCreateService.create(studentGroupCreateRequest);

        // then
        then(studentGroupRepository).should().save(any());
    }

    @Test
    void shouldThrowStudentGroupAlreadyExistExceptionDuringCallCreate() {
        // given
        final StudentGroupCreateRequest studentGroupCreateRequest = prepareStudentGroupCreateRequest();
        given(studentGroupRepository.countByNameAndYearAndSubgroup(NAME, YEAR, SUBGROUP)).willReturn(1);

        // when
        final Executable executable = () -> studentGroupCreateService.create(studentGroupCreateRequest);

        // then
        final StudentGroupAlreadyExistException exception = assertThrows(StudentGroupAlreadyExistException.class, executable);
        assertEquals("Student group with data 'name = " + NAME + "', 'year = " + YEAR + "', 'subgroup = " + SUBGROUP + "' already exist.", exception.getMessage());
        assertEquals("student-group-already-exist", exception.getType());
        assertSame(HttpStatus.BAD_REQUEST, exception.getHttpStatus());
    }

    private static StudentGroupCreateRequest prepareStudentGroupCreateRequest() {
        return StudentGroupCreateRequest.builder()
                .name(NAME)
                .year(YEAR)
                .subgroup(SUBGROUP)
                .build();
    }

    private static StudentGroupEntity prepareStudentGroupEntity() {
        return StudentGroupEntity.builder()
                .id(UUID.fromString("212f40b0-bebc-4813-b371-8d99cc4d92dc"))
                .name(NAME)
                .year(YEAR)
                .subgroup(SUBGROUP)
                .build();
    }
}