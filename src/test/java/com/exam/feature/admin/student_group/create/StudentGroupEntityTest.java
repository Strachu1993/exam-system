package com.exam.feature.admin.student_group.create;

import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class StudentGroupEntityTest {

    @Test
    void shouldSetIdForStudentGroupEntityDuringCreate() {
        // given
        final StudentGroupEntity studentGroupEntity = StudentGroupEntity.builder().build();

        // when
        ReflectionTestUtils.invokeMethod(studentGroupEntity, "prePersist");

        // then
        assertNotNull(studentGroupEntity.getId());
    }
}