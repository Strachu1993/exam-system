package com.exam.feature.admin.student_group.update;

import com.exam.commons.enums.Subgroup;
import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import com.exam.commons.exception.custom.validate.StudentGroupAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpStatus;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.then;

class StudentGroupUpdateServiceTest {

    private static final String OLD_NAME = "Old student group name";
    private static final String OLD_YEAR = "2019";
    private static final Subgroup OLD_SUBGROUP = Subgroup.B;
    private static final String NEW_NAME = "New student group name";
    private static final String NEW_YEAR = "2020";
    private static final Subgroup NEW_SUBGROUP = Subgroup.A;
    private static final UUID STUDENT_GROUP_ID = UUID.fromString("7461d726-c74c-42ab-87f4-33017d09e460");
    private final StudentGroupRepository studentGroupRepository = mock(StudentGroupRepository.class);
    private StudentGroupUpdateService studentGroupUpdateService;

    @BeforeEach
    void beforeEach() {
        studentGroupUpdateService = new StudentGroupUpdateService(studentGroupRepository);
    }

    @Test
    void shouldUpdateStudentGroup() {
        // given
        final StudentGroupUpdateRequest studentGroupRequest = prepareStudentGroupUpdateRequest();
        final Optional<StudentGroupEntity> studentGroupEntityOpt = prepareStudentGroupEntity();
        given(studentGroupRepository.findById(STUDENT_GROUP_ID)).willReturn(studentGroupEntityOpt);
        given(studentGroupRepository.countByIdNotAndNameAndYearAndSubgroup(STUDENT_GROUP_ID, NEW_NAME, NEW_YEAR, NEW_SUBGROUP)).willReturn(0);

        // when
        studentGroupUpdateService.update(STUDENT_GROUP_ID, studentGroupRequest);

        // then
        final ArgumentCaptor<StudentGroupEntity> captor = ArgumentCaptor.forClass(StudentGroupEntity.class);
        then(studentGroupRepository).should().save(captor.capture());
        final StudentGroupEntity captorValue = captor.getValue();
        assertEquals(STUDENT_GROUP_ID, captorValue.getId());
        assertEquals(NEW_NAME, captorValue.getName());
        assertEquals(NEW_YEAR, captorValue.getYear());
        assertSame(NEW_SUBGROUP, captorValue.getSubgroup());
    }

    @Test
    void shouldThrowStudentGroupNotFoundExceptionDuringCallUpdate() {
        // given
        final StudentGroupUpdateRequest studentGroupRequest = prepareStudentGroupUpdateRequest();

        // when
        final Executable executable = () -> studentGroupUpdateService.update(STUDENT_GROUP_ID, studentGroupRequest);

        // then
        final StudentGroupNotFoundException exception = assertThrows(StudentGroupNotFoundException.class, executable);
        assertEquals("Cannot found student group with id '" + STUDENT_GROUP_ID + "'.", exception.getMessage());
        assertEquals("student-group-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
    }

    @Test
    void shouldThrowStudentGroupAlreadyExistExceptionDuringCallUpdate() {
        // given
        final StudentGroupUpdateRequest studentGroupRequest = prepareStudentGroupUpdateRequest();
        final Optional<StudentGroupEntity> studentGroupEntityOpt = prepareStudentGroupEntity();
        given(studentGroupRepository.findById(STUDENT_GROUP_ID)).willReturn(studentGroupEntityOpt);
        given(studentGroupRepository.countByIdNotAndNameAndYearAndSubgroup(STUDENT_GROUP_ID, NEW_NAME, NEW_YEAR, NEW_SUBGROUP)).willReturn(3);

        // when
        final Executable executable = () -> studentGroupUpdateService.update(STUDENT_GROUP_ID, studentGroupRequest);

        // then
        final StudentGroupAlreadyExistException exception = assertThrows(StudentGroupAlreadyExistException.class, executable);
        assertEquals("Student group with data 'id = " + STUDENT_GROUP_ID + "', 'name = New student group name', 'year = 2020', 'subgroup = A' already exist.", exception.getMessage());
        assertEquals("student-group-already-exist", exception.getType());
        assertSame(HttpStatus.BAD_REQUEST, exception.getHttpStatus());
    }

    private static Optional<StudentGroupEntity> prepareStudentGroupEntity() {
        final StudentGroupEntity studentGroupEntity = StudentGroupEntity.builder()
                .id(STUDENT_GROUP_ID)
                .name(OLD_NAME)
                .year(OLD_YEAR)
                .subgroup(OLD_SUBGROUP)
                .build();

        return Optional.of(studentGroupEntity);
    }

    private static StudentGroupUpdateRequest prepareStudentGroupUpdateRequest() {
        return StudentGroupUpdateRequest.builder()
                    .name(NEW_NAME)
                    .year(NEW_YEAR)
                    .subgroup(NEW_SUBGROUP)
                    .build();
    }
}