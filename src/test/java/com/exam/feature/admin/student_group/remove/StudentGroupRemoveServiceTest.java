package com.exam.feature.admin.student_group.remove;

import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.http.HttpStatus;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.then;

class StudentGroupRemoveServiceTest {

    private static final UUID STUDENT_GROUP_ID = UUID.fromString("7461d726-c74c-42ab-87f4-33017d09e460");
    private final StudentGroupRepository studentGroupRepository = mock(StudentGroupRepository.class);
    private StudentGroupRemoveService studentGroupRemoveService;

    @BeforeEach
    void beforeEach() {
        studentGroupRemoveService = new StudentGroupRemoveService(studentGroupRepository);
    }

    @Test
    void shouldRemoveStudentGroup() {
        // given
        final Optional<StudentGroupEntity> studentGroupEntityOpt = prepareStudentGroupEntity(0);
        given(studentGroupRepository.findById(STUDENT_GROUP_ID)).willReturn(studentGroupEntityOpt);

        // when
        studentGroupRemoveService.remove(STUDENT_GROUP_ID);

        // then
        then(studentGroupRepository).should().deleteById(STUDENT_GROUP_ID);
    }

    @Test
    void shouldThrowStudentGroupHasStudentExceptionDuringCallRemove() {
        // given
        final int studentDetailsSize = 3;
        final Optional<StudentGroupEntity> studentGroupEntityOpt = prepareStudentGroupEntity(studentDetailsSize);
        given(studentGroupRepository.findById(STUDENT_GROUP_ID)).willReturn(studentGroupEntityOpt);

        // when
        final Executable executable = () -> studentGroupRemoveService.remove(STUDENT_GROUP_ID);

        // then
        final StudentGroupHasStudentsException exception = assertThrows(StudentGroupHasStudentsException.class, executable);
        assertEquals("Student group with id '" + STUDENT_GROUP_ID + "' has '" + studentDetailsSize + "' students.", exception.getMessage());
        assertEquals("student-group-has-student", exception.getType());
        assertSame(HttpStatus.CONFLICT, exception.getHttpStatus());
    }

    @Test
    void shouldThrowStudentGroupNotFoundExceptionDuringCallRemove() {
        // given
        given(studentGroupRepository.findById(STUDENT_GROUP_ID)).willReturn(Optional.empty());

        // when
        final Executable executable = () -> studentGroupRemoveService.remove(STUDENT_GROUP_ID);

        // then
        final StudentGroupNotFoundException exception = assertThrows(StudentGroupNotFoundException.class, executable);
        assertEquals("Cannot found student group with id '" + STUDENT_GROUP_ID + "'.", exception.getMessage());
        assertEquals("student-group-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
    }

    private static Optional<StudentGroupEntity> prepareStudentGroupEntity(final int studentDetailsSize) {
        final Set<StudentDetailsEntity> studentDetailsEntities = new HashSet<>();
        fillStudentDetailsEntities(studentDetailsSize, studentDetailsEntities);
        final StudentGroupEntity studentGroupEntity = StudentGroupEntity.builder()
                .id(STUDENT_GROUP_ID)
                .studentDetails(studentDetailsEntities)
                .build();

        return Optional.of(studentGroupEntity);
    }

    private static StudentDetailsEntity prepareStudentDetailsEntity() {
        return StudentDetailsEntity.builder()
                .id(UUID.randomUUID())
                .build();
    }

    private static void fillStudentDetailsEntities(int studentDetailsSize, final Set<StudentDetailsEntity> studentDetailsEntities) {
        for (int i = 0; i < studentDetailsSize; i++) {
            studentDetailsEntities.add(prepareStudentDetailsEntity());
        }
    }
}