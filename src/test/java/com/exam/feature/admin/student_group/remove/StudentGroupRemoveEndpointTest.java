package com.exam.feature.admin.student_group.remove;

import com.exam.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

class StudentGroupRemoveEndpointTest {

    private static final String URL = "/api/admin/student-groups/44a4127e-a953-47fe-80ac-1353c0697afb";
    private static final StudentGroupRemoveService studentGroupRemoveService = mock(StudentGroupRemoveService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new StudentGroupRemoveEndpoint(studentGroupRemoveService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallRemoveStudentGroup() throws Exception {
        // given

        // when
        final ResultActions actions = mockMvc.perform(delete(URL));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.NO_CONTENT.value(),
                mvcResult.getResponse()
                        .getStatus());
    }
}