package com.exam.feature.admin.student_group.update;

import com.exam.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

class StudentGroupUpdateEndpointTest {

    private static final String URL = "/api/admin/student-groups/44a4127e-a953-47fe-80ac-1353c0697afb";
    private static final StudentGroupUpdateService studentGroupUpdateService = mock(StudentGroupUpdateService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new StudentGroupUpdateEndpoint(studentGroupUpdateService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallUpdateStudentGroup() throws Exception {
        // given
        final String requestJson = "{\n" +
                "  \"name\":\"New name\",\n" +
                "  \"year\":\"2004\",\n" +
                "  \"subgroup\":\"B\"\n" +
                "}";

        // when
        final ResultActions actions = mockMvc.perform(put(URL)
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.NO_CONTENT.value(),
                mvcResult.getResponse()
                        .getStatus());
    }
}