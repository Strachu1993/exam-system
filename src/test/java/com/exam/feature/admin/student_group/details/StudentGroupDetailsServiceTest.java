package com.exam.feature.admin.student_group.details;

import com.exam.commons.enums.Subgroup;
import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StudentGroupDetailsServiceTest {

    private static final UUID STUDENT_GROUP_ID = UUID.fromString("ce93f398-3c89-466c-9588-822bde92eb29");
    private static final String NAME = "Student group name";
    private static final String YEAR = "2017";
    private static final Subgroup SUBGROUP = Subgroup.A;
    private static final String ALBUM_NUMBER_1 = "123321";
    private static final String ALBUM_NUMBER_2 = "321123";
    private static final String ALBUM_NUMBER_3 = "522552";
    private static final String NAME_1 = "Jan";
    private static final String NAME_2 = "Samuel";
    private static final String NAME_3 = "Mikołaj";
    private static final String SURNAME_1 = "Kozietulski";
    private static final String SURNAME_2 = "Kmicic";
    private static final String SURNAME_3 = "Skrzetuski";

    private final StudentGroupRepository studentGroupRepository = mock(StudentGroupRepository.class);
    private StudentGroupDetailsService studentGroupDetailsService;

    @BeforeEach
    void beforeEach() {
        studentGroupDetailsService = new StudentGroupDetailsService(studentGroupRepository);
    }

    @Test
    void shouldReturnStudentGroupDetailsById() {
        // given
        final StudentGroupEntity studentGroupEntity = prepareStudentGroupEntity();
        when(studentGroupRepository.findById(STUDENT_GROUP_ID)).thenReturn(Optional.of(studentGroupEntity));

        // when
        final StudentGroupDetailsResponse studentGroupDetails = studentGroupDetailsService.getStudentGroupDetails(STUDENT_GROUP_ID);

        // then
        assertEquals(STUDENT_GROUP_ID, studentGroupDetails.getId());
        assertEquals(NAME, studentGroupDetails.getName());
        assertEquals(YEAR, studentGroupDetails.getYear());
        assertSame(SUBGROUP, studentGroupDetails.getSubgroup());
        final StudentGroupDetailsResponse.StudentDetailsResponse studentDetailsResponse1 = studentGroupDetails.getStudents().get(0);
        final StudentGroupDetailsResponse.StudentDetailsResponse studentDetailsResponse2 = studentGroupDetails.getStudents().get(1);
        final StudentGroupDetailsResponse.StudentDetailsResponse studentDetailsResponse3 = studentGroupDetails.getStudents().get(2);
        assertStudentDetails(studentDetailsResponse1, ALBUM_NUMBER_1, NAME_1, SURNAME_1);
        assertStudentDetails(studentDetailsResponse2, ALBUM_NUMBER_2, NAME_2, SURNAME_2);
        assertStudentDetails(studentDetailsResponse3, ALBUM_NUMBER_3, NAME_3, SURNAME_3);
    }

    @Test
    void shouldReturnStudentGroupDetailsByNameAndYearAndSubgroup() {
        // given
        final StudentGroupEntity studentGroupEntity = prepareStudentGroupEntity();
        when(studentGroupRepository.findByNameAndYearAndSubgroup(NAME, YEAR, SUBGROUP)).thenReturn(Optional.of(studentGroupEntity));

        // when
        final UUID studentGroupId = studentGroupDetailsService.getStudentGroupDetails(NAME, YEAR, SUBGROUP);

        // then
        assertEquals(STUDENT_GROUP_ID, studentGroupId);
    }

    @Test
    void shouldThrowStudentGroupNotFoundException() {
        // given

        // when
        final Executable executable = () -> studentGroupDetailsService.getStudentGroupDetails(STUDENT_GROUP_ID);

        // then
        final StudentGroupNotFoundException exception = assertThrows(StudentGroupNotFoundException.class, executable);
        assertEquals("Cannot found student group with id '" + STUDENT_GROUP_ID + "'.", exception.getMessage());
        assertEquals("student-group-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
    }

    private static void assertStudentDetails(final StudentGroupDetailsResponse.StudentDetailsResponse studentDetailsResponse, final String albumNumber, final String name, final String surname) {
        assertEquals(albumNumber, studentDetailsResponse.getAlbumNumber());
        assertEquals(name, studentDetailsResponse.getName());
        assertEquals(surname, studentDetailsResponse.getSurname());
        assertNotNull(studentDetailsResponse.getAccountId());
        assertNotNull(studentDetailsResponse.getStudentDetailsId());
    }

    private static StudentGroupEntity prepareStudentGroupEntity() {
        final StudentDetailsEntity studentDetailsEntity1 = createStudentDetailsEntity(ALBUM_NUMBER_1, NAME_1, SURNAME_1);
        final StudentDetailsEntity studentDetailsEntity2 = createStudentDetailsEntity(ALBUM_NUMBER_2, NAME_2, SURNAME_2);
        final StudentDetailsEntity studentDetailsEntity3 = createStudentDetailsEntity(ALBUM_NUMBER_3, NAME_3, SURNAME_3);
        final List<StudentDetailsEntity> studentsDetails = List.of(studentDetailsEntity1, studentDetailsEntity2, studentDetailsEntity3);

        return StudentGroupEntity.builder()
                .id(STUDENT_GROUP_ID)
                .name(NAME)
                .subgroup(SUBGROUP)
                .year(YEAR)
                .studentDetails(studentsDetails)
                .build();
    }

    private static StudentDetailsEntity createStudentDetailsEntity(final String albumNumber, final String name, final String surname) {
        return StudentDetailsEntity.builder()
                .id(UUID.randomUUID())
                .albumNumber(albumNumber)
                .name(name)
                .surname(surname)
                .account(createAccountEntity())
                .build();
    }

    private static AccountEntity createAccountEntity() {
        return AccountEntity.builder()
                .id(UUID.randomUUID())
                .enabled(true)
                .build();
    }
}