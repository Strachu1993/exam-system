package com.exam.feature.admin.student_group.list;

import com.exam.commons.enums.Subgroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.mock;

class StudentGroupListServiceTest {

    private static final String NAME = "Student group name";
    private static final String YEAR = "2019";
    private static final Subgroup SUBGROUP = Subgroup.B;
    private static final int STUDENT_SIZE = 10;
    private final StudentGroupRepository studentGroupRepository = mock(StudentGroupRepository.class);
    private StudentGroupListService studentGroupListService;

    @BeforeEach
    void beforeEach() {
        studentGroupListService = new StudentGroupListService(studentGroupRepository);
    }

    @Test
    void shouldGetStudentGroupList() {
        // given
        final StudentGroupEntity studentGroupEntity1 = prepareStudentGroupEntity();
        final StudentGroupEntity studentGroupEntity2 = prepareStudentGroupEntity();
        final StudentGroupEntity studentGroupEntity3 = prepareStudentGroupEntity();
        final StudentGroupEntity studentGroupEntity4 = prepareStudentGroupEntity();
        given(studentGroupRepository.findAll(any())).willReturn(List.of(studentGroupEntity1, studentGroupEntity2, studentGroupEntity3, studentGroupEntity4));

        // when
        final List<StudentGroupResponse> studentGroups = studentGroupListService.findAll();

        // then
        assertStudentGroup(studentGroups.get(0));
        assertStudentGroup(studentGroups.get(1));
        assertStudentGroup(studentGroups.get(2));
        assertStudentGroup(studentGroups.get(3));
    }

    private static void assertStudentGroup(final StudentGroupResponse studentGroupResponse) {
        assertNotNull(studentGroupResponse.getId());
        assertEquals(NAME, studentGroupResponse.getName());
        assertEquals(YEAR, studentGroupResponse.getYear());
        assertSame(SUBGROUP, studentGroupResponse.getSubgroup());
        assertSame(STUDENT_SIZE, studentGroupResponse.getStudentSize());
    }

    private static StudentGroupEntity prepareStudentGroupEntity() {
        final Set<StudentDetailsEntity> studentDetailsEntities = new HashSet<>();
        fillStudentDetailsEntities(studentDetailsEntities);

        return StudentGroupEntity.builder()
                .id(UUID.randomUUID())
                .name(NAME)
                .year(YEAR)
                .subgroup(SUBGROUP)
                .studentDetails(studentDetailsEntities)
                .build();
    }

    private static StudentDetailsEntity prepareStudentDetailsEntity() {
        return StudentDetailsEntity.builder()
                .id(UUID.randomUUID())
                .build();
    }

    private static void fillStudentDetailsEntities(final Set<StudentDetailsEntity> studentDetailsEntities) {
        for (int i = 0; i < STUDENT_SIZE; i++) {
            studentDetailsEntities.add(prepareStudentDetailsEntity());
        }
    }
}