package com.exam.feature.admin.subject.remove;

import com.exam.commons.exception.custom.general.SubjectException;
import com.exam.commons.exception.custom.not_found.SubjectNotFoundException;
import com.exam.helper.ReflectionUtilsHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.slf4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.never;

class SubjectRemoveServiceTest {

    private static final UUID SUBJECT_ID = UUID.fromString("f67b2643-4705-4650-bb8e-0af0b6022a69");

    private final Logger log = mock(Logger.class);
    private final SubjectRepository subjectRepository = mock(SubjectRepository.class);
    private SubjectRemoveService subjectRemoveService;

    @BeforeEach
    private void beforeEach() {
        subjectRemoveService = new SubjectRemoveService(subjectRepository);
        ReflectionUtilsHelper.setFinalStatic(subjectRemoveService, "log", log);
    }

    @Test
    void shouldRemoveSubjectById() {
        // given

        // when
        subjectRemoveService.delete(SUBJECT_ID);

        // then
        then(subjectRepository).should().deleteById(SUBJECT_ID);
        then(log).should().info(anyString(), eq(SUBJECT_ID));
    }

    @Test
    void shouldThrowSubjectNotFoundExceptionDuringDeleteSubject() {
        // given
        willThrow(new EmptyResultDataAccessException(1)).given(subjectRepository).deleteById(SUBJECT_ID);

        // when
        final Executable executed = () -> subjectRemoveService.delete(SUBJECT_ID);

        // then
        final SubjectNotFoundException exception = assertThrows(SubjectNotFoundException.class, executed);
        assertEquals("Cannot found subject with id '" + SUBJECT_ID + "'.", exception.getMessage());
        assertEquals("subject-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        then(subjectRepository).should().deleteById(SUBJECT_ID);
        then(log).should(never()).info(anyString(), eq(SUBJECT_ID));
    }

    @Test
    void shouldThrowSubjectExceptionDuringDeleteSubjectWhenEntityHasRelations() {
        // given
        willThrow(new DataIntegrityViolationException("Example error message")).given(subjectRepository).deleteById(SUBJECT_ID);

        // when
        final Executable executed = () -> subjectRemoveService.delete(SUBJECT_ID);

        // then
        final SubjectException exception = assertThrows(SubjectException.class, executed);
        assertEquals("The subject with id '" + SUBJECT_ID + "' has references to other tables.", exception.getMessage());
        assertEquals("subject-exception", exception.getType());
        assertSame(HttpStatus.CONFLICT, exception.getHttpStatus());
        then(subjectRepository).should().deleteById(SUBJECT_ID);
        then(log).should(never()).info(anyString(), eq(SUBJECT_ID));
    }
}