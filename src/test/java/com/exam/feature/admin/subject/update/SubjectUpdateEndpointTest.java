package com.exam.feature.admin.subject.update;

import com.exam.helper.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

class SubjectUpdateEndpointTest {

    private static final UUID SUBJECT_ID = UUID.fromString("476b41e9-71b6-44a8-b343-fcfa49f3e83c");
    private static final String URL = "/api/admin/subjects/" + SUBJECT_ID;
    private static final SubjectUpdateService subjectUpdateService = mock(SubjectUpdateService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new SubjectUpdateEndpoint(subjectUpdateService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallUpdateSubject() throws Exception {
        // given
        final String requestJson = "{\n" +
                "  \"name\":\"New name\"\n" +
                "}";

        // when
        final ResultActions actions = mockMvc.perform(put(URL)
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.NO_CONTENT.value(),
                mvcResult.getResponse()
                        .getStatus());
        then(subjectUpdateService).should().update(eq(SUBJECT_ID), any(SubjectRequest.class));
    }
}