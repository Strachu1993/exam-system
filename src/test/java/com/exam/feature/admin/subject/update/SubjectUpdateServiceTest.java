package com.exam.feature.admin.subject.update;

import com.exam.commons.exception.custom.not_found.SubjectNotFoundException;
import com.exam.commons.exception.custom.validate.SubjectNameAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpStatus;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

class SubjectUpdateServiceTest {

    private final SubjectRepository subjectRepository = mock(SubjectRepository.class);
    private SubjectUpdateService subjectUpdateService;

    @BeforeEach
    private void beforeEach() {
        subjectUpdateService = new SubjectUpdateService(subjectRepository);
    }

    @Test
    void shouldUpdateSubject() {
        // given
        final String subjectName = "Data base";
        final UUID subjectId = UUID.fromString("cf9890d9-e03c-48e2-bcdc-d555724fc2fb");
        final SubjectRequest subjectRequest = SubjectRequest.builder()
                .name(subjectName)
                .build();

        final SubjectEntity subjectEntity = prepareSubjectName(subjectId);
        given(subjectRepository.findById(subjectId)).willReturn(Optional.of(subjectEntity));
        given(subjectRepository.countByName(subjectName)).willReturn(0);

        // when
        subjectUpdateService.update(subjectId, subjectRequest);

        // then
        final ArgumentCaptor<SubjectEntity> captor = ArgumentCaptor.forClass(SubjectEntity.class);
        then(subjectRepository).should().save(captor.capture());
        final SubjectEntity captorValue = captor.getValue();
        assertEquals(subjectId, captorValue.getId());
        assertEquals(subjectName, captorValue.getName());
    }

    @Test
    void shouldThrowSubjectNotFoundExceptionDuringSubjectUpdate() {
        // given
        final String subjectName = "Data base";
        final UUID subjectId = UUID.fromString("cf9890d9-e03c-48e2-bcdc-d555724fc2fb");
        final SubjectRequest subjectRequest = SubjectRequest.builder()
                .name(subjectName)
                .build();
        given(subjectRepository.findById(subjectId)).willReturn(Optional.empty());

        // when
        final Executable executed = () -> subjectUpdateService.update(subjectId, subjectRequest);

        // then
        final SubjectNotFoundException exception = assertThrows(SubjectNotFoundException.class, executed);
        assertEquals("Cannot found subject with id '" + subjectId + "'.", exception.getMessage());
        assertEquals("subject-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
    }

    @Test
    void shouldThrowSubjectNameAlreadyExistExceptionDuringSubjectUpdate() {
        // given
        final String subjectName = "Data base";
        final UUID subjectId = UUID.fromString("cf9890d9-e03c-48e2-bcdc-d555724fc2fb");
        final SubjectRequest subjectRequest = SubjectRequest.builder()
                .name(subjectName)
                .build();
        final SubjectEntity subjectEntity = prepareSubjectName(subjectId);
        given(subjectRepository.findById(subjectId)).willReturn(Optional.of(subjectEntity));
        given(subjectRepository.countByName(subjectName)).willReturn(1);

        // when
        final Executable executed = () -> subjectUpdateService.update(subjectId, subjectRequest);

        // then
        final SubjectNameAlreadyExistException exception = assertThrows(SubjectNameAlreadyExistException.class, executed);
        assertEquals("Name '" + subjectName + "' already exist in subject table.", exception.getMessage());
        assertEquals("subject-name-already-exist", exception.getType());
        assertSame(HttpStatus.CONFLICT, exception.getHttpStatus());
    }

    private static SubjectEntity prepareSubjectName(final UUID subjectId) {
        return SubjectEntity.builder()
                    .id(subjectId)
                    .name("Programing")
                    .build();
    }
}