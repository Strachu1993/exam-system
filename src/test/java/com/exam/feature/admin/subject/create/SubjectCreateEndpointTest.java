package com.exam.feature.admin.subject.create;

import com.exam.helper.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

class SubjectCreateEndpointTest {

    private static final String URL = "/api/admin/subjects";
    private static final SubjectCreateService subjectCreateService = mock(SubjectCreateService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new SubjectCreateEndpoint(subjectCreateService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallCreateSubject() throws Exception {
        // given
        final String requestJson = "{\n" +
                "  \"name\":\"New name\"\n" +
                "}";

        // when
        final ResultActions actions = mockMvc.perform(post(URL)
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.CREATED.value(),
                mvcResult.getResponse()
                        .getStatus());
    }
}