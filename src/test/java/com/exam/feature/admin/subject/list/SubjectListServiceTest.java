package com.exam.feature.admin.subject.list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class SubjectListServiceTest {

    private final SubjectListReadRepository subjectRepository = mock(SubjectListReadRepository.class);
    private SubjectListService service;

    @BeforeEach
    private void beforeEach() {
        service = new SubjectListService(subjectRepository);
    }

    @Test
    void shouldGetSubjectList() {
        // given
        final int questionSize1 = 2;
        final int questionSize2 = 4;
        final String subjectName1 = "Software testing";
        final String subjectName2 = "Data base";
        final SubjectEntity subjectEntity1 = prepareSubjectEntity(questionSize1, subjectName1);
        final SubjectEntity subjectEntity2 = prepareSubjectEntity(questionSize2, subjectName2);

        final Sort sort = Sort.by(Sort.Order.desc("name"));
        final List<SubjectEntity> subjectEntities = List.of(subjectEntity1, subjectEntity2);
        given(subjectRepository.findAll(sort)).willReturn(subjectEntities);

        // when
        final List<SubjectResponse> subjects = service.findAll();

        // then
        assertSame(subjectEntities.size(), subjects.size());
        assertResponse(subjects.get(0), questionSize1, subjectName1);
        assertResponse(subjects.get(1), questionSize2, subjectName2);
    }

    private void assertResponse(final SubjectResponse response, final int questionSize, final String subjectName) {
        assertNotNull(response.getId());
        assertSame(questionSize, response.getQuestionSize());
        assertEquals(subjectName, response.getName());
    }

    private static SubjectEntity prepareSubjectEntity(final int questionSize, final String subjectName) {
        final List<QuestionEntity> questionEntities = new ArrayList<>();
        for (int i = 0; i < questionSize; i++) {
            questionEntities.add(prepareQuestionEntity());
        }

        return SubjectEntity.builder()
                .id(UUID.randomUUID())
                .name(subjectName)
                .questions(questionEntities)
                .build();
    }

    private static QuestionEntity prepareQuestionEntity() {
        return QuestionEntity.builder()
                .id(UUID.randomUUID())
                .build();
    }
}