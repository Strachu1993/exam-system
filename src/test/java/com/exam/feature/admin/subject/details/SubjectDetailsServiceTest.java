package com.exam.feature.admin.subject.details;

import com.exam.commons.exception.custom.not_found.SubjectNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SubjectDetailsServiceTest {

    private static final UUID SUBJECT_ID = UUID.fromString("c30cbc82-5d10-4b1e-915c-5066fa282d36");
    private final SubjectReadRepository subjectRepository = mock(SubjectReadRepository.class);
    private final SubjectDetailsService subjectDetailsService = new SubjectDetailsService(subjectRepository);

    @Test
    void shouldReturnSubjectDetails() {
        // given
        final String subjectName = "Software testing";
        final SubjectEntity subjectEntity = SubjectEntity.builder()
                .id(UUID.randomUUID())
                .name(subjectName)
                .questions(new ArrayList<>())
                .build();

        final Optional<SubjectEntity> subjectEntityOpt = Optional.of(subjectEntity);
        when(subjectRepository.findById(eq(SUBJECT_ID), any())).thenReturn(subjectEntityOpt);

        // when
        final SubjectDetailsResponse subjectDetails = subjectDetailsService.getSubjectDetails(SUBJECT_ID);

        // then
        assertAll(
                () -> assertNotNull(subjectDetails.getId()),
                () -> assertEquals(subjectName, subjectDetails.getName()),
                () -> assertSame(0, subjectDetails.getQuestions().size()),
                () -> verify(subjectRepository).findById(eq(SUBJECT_ID), any())
        );
    }

    @Test
    void shouldThrowSubjectNotFoundException() {
        // given
        doThrow(new SubjectNotFoundException(SUBJECT_ID)).when(subjectRepository).findById(eq(SUBJECT_ID), any());

        // when
        final Executable executed = () -> subjectDetailsService.getSubjectDetails(SUBJECT_ID);

        //then
        final SubjectNotFoundException exception = assertThrows(SubjectNotFoundException.class, executed);
        assertAll(
                () -> assertEquals("Cannot found subject with id '" + SUBJECT_ID + "'.", exception.getMessage()),
                () -> assertEquals("subject-not-found", exception.getType()),
                () -> assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus()),
                () -> verify(subjectRepository).findById(eq(SUBJECT_ID), any())
        );
    }
}