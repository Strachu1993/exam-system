package com.exam.feature.admin.subject.details;

import com.exam.helper.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

class SubjectDetailsEndpointTest {

    private static final UUID SUBJECT_ID = UUID.fromString("476b41e9-71b6-44a8-b343-fcfa49f3e83c");
    private static final String URL = "/api/admin/subjects/" + SUBJECT_ID;
    private static final SubjectDetailsService subjectDetailsService = mock(SubjectDetailsService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new SubjectDetailsEndpoint(subjectDetailsService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallGetStudentGroupDetails() throws Exception {
        // given

        // when
        final ResultActions actions = mockMvc.perform(get(URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.OK.value(),
                mvcResult.getResponse()
                        .getStatus());
        then(subjectDetailsService).should().getSubjectDetails(SUBJECT_ID);
    }
}