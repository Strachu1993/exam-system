package com.exam.feature.admin.subject.details;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SubjectDetailsResponseTest {

    private final UUID ID = UUID.fromString("599c567d-827b-44b9-b9b0-5bfd7719c2b0");
    private final String QUESTION = "Some question";
    private final short CORRECT_ANSWERS = 2;
    private final SubjectEntity SUBJECT = prepareSubjectEntity();

    @Test
    void shouldCreateSubjectDetailsResponseWithCodeAndCanDelete() {
        // given
        final String code = "System.out.println(\"Some stdout\");";
        final List<AnswerEntity> answers = List.of(prepareAnswerEntity(), prepareAnswerEntity(), prepareAnswerEntity());
        final List<ExamEntity> exams = new ArrayList<>();
        final QuestionEntity questionEntity = QuestionEntity.builder()
                .id(ID)
                .question(QUESTION)
                .code(code)
                .correctAnswers(CORRECT_ANSWERS)
                .answers(answers)
                .exams(exams)
                .subject(SUBJECT)
                .build();

        // when
        final SubjectDetailsResponse.QuestionResponse result = SubjectDetailsResponse.QuestionResponse.of(questionEntity);

        // then
        assertEquals(ID, result.getId());
        assertEquals(QUESTION, result.getQuestion());
        assertSame(answers.size(), result.getTotalAnswers());
        assertSame(CORRECT_ANSWERS, result.getCorrectAnswers());
        assertTrue(result.isHasCode());
        assertTrue(result.isCanDelete());
    }

    @Test
    void shouldCreateSubjectDetailsResponseWithoutCodeAndCannotDelete() {
        // given
        final List<AnswerEntity> answers = List.of(prepareAnswerEntity(), prepareAnswerEntity(), prepareAnswerEntity());
        final List<ExamEntity> exams = List.of(prepareExamEntity());
        final QuestionEntity questionEntity = QuestionEntity.builder()
                .id(ID)
                .question(QUESTION)
                .correctAnswers(CORRECT_ANSWERS)
                .answers(answers)
                .exams(exams)
                .subject(SUBJECT)
                .build();

        // when
        final SubjectDetailsResponse.QuestionResponse result = SubjectDetailsResponse.QuestionResponse.of(questionEntity);

        // then
        assertEquals(ID, result.getId());
        assertEquals(QUESTION, result.getQuestion());
        assertSame(answers.size(), result.getTotalAnswers());
        assertSame(CORRECT_ANSWERS, result.getCorrectAnswers());
        assertFalse(result.isHasCode());
        assertFalse(result.isCanDelete());
    }

    private static ExamEntity prepareExamEntity() {
        return ExamEntity.builder()
                .id(UUID.randomUUID())
                .build();
    }

    private static SubjectEntity prepareSubjectEntity() {
        return SubjectEntity.builder()
                .id(UUID.fromString("a6ef4290-3fed-4ecb-adbe-b76e0ce729d3"))
                .name("Testowanie oprogramowania")
                .questions(List.of())
                .build();
    }

    private static AnswerEntity prepareAnswerEntity() {
        return AnswerEntity.builder()
                .id(UUID.randomUUID())
                .build();
    }
}
