package com.exam.feature.admin.subject.list;

import com.exam.helper.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

class SubjectListEndpointTest {

    private static final String URL = "/api/admin/subjects";
    private static final SubjectListService subjectListService = mock(SubjectListService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new SubjectListEndpoint(subjectListService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallGetSubject() throws Exception {
        // given

        // when
        final ResultActions actions = mockMvc.perform(get(URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.OK.value(),
                mvcResult.getResponse()
                        .getStatus());
    }
}