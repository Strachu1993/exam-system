package com.exam.feature.admin.subject.list;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

class SubjectResponseTest {

    private static final UUID SUBJECT_ENTITY_ID = UUID.fromString("2700502d-676f-45b9-a68f-8155a0555230");
    private static final String NAME = "Testowanie oprogramowania";

    @Test
    void shouldCreateSubjectResponseFromSubjectResponse() {
        // given
        final SubjectEntity subjectEntity = prepareSubjectEntity();

        // when
        final SubjectResponse result = SubjectResponse.of(subjectEntity);

        // then
        assertEquals(SUBJECT_ENTITY_ID, result.getId());
        assertEquals(NAME, result.getName());
        assertSame(2, result.getQuestionSize());
    }

    private static SubjectEntity prepareSubjectEntity() {
        return SubjectEntity.builder()
                .id(SUBJECT_ENTITY_ID)
                .name(NAME)
                .questions(List.of(prepareQuestionEntity(), prepareQuestionEntity()))
                .build();
    }

    private static QuestionEntity prepareQuestionEntity() {
        return QuestionEntity.builder()
                .id(UUID.randomUUID())
                .build();
    }
}