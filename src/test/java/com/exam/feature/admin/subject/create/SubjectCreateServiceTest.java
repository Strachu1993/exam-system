package com.exam.feature.admin.subject.create;

import com.exam.commons.exception.custom.validate.SubjectNameAlreadyExistException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SubjectCreateServiceTest {

    private static final String NEW_SUBJECT_NAME = "New Subject name";

    private final SubjectRepository subjectRepository = mock(SubjectRepository.class);
    private final SubjectCreateService subjectCreateService = new SubjectCreateService(subjectRepository);

    @Test
    void shouldCreateNewSubject() {
        // given
        final SubjectRequest subjectRequest = SubjectRequest.builder()
                .name(NEW_SUBJECT_NAME)
                .build();
        when(subjectRepository.countByName(NEW_SUBJECT_NAME)).thenReturn(0);

        // when
        subjectCreateService.create(subjectRequest);

        // then
        final ArgumentCaptor<SubjectEntity> subjectCapture = ArgumentCaptor.forClass(SubjectEntity.class);
        verify(subjectRepository).save(subjectCapture.capture());
        final SubjectEntity savedSubject = subjectCapture.getValue();
        assertAll(
                () -> assertEquals(NEW_SUBJECT_NAME, savedSubject.getName()),
                () -> verify(subjectRepository).countByName(NEW_SUBJECT_NAME),
                () -> verify(subjectRepository).save(any())
        );
    }

    @Test
    void shouldThrowSubjectNameAlreadyExistExceptionDuringCreate() {
        // given
        final SubjectRequest subjectRequest = SubjectRequest.builder()
                .name(NEW_SUBJECT_NAME)
                .build();
        when(subjectRepository.countByName(NEW_SUBJECT_NAME)).thenReturn(1);

        // when
        final Executable executed = () -> subjectCreateService.create(subjectRequest);

        // then
        final SubjectNameAlreadyExistException exception = assertThrows(SubjectNameAlreadyExistException.class, executed);
        assertAll(
                () -> assertEquals("Name '" + NEW_SUBJECT_NAME + "' already exist in subject table.", exception.getMessage()),
                () -> assertEquals("subject-name-already-exist", exception.getType()),
                () -> assertSame(HttpStatus.CONFLICT, exception.getHttpStatus()),
                () -> verify(subjectRepository).countByName(NEW_SUBJECT_NAME),
                () -> verify(subjectRepository, never()).save(any())
        );
    }
}
