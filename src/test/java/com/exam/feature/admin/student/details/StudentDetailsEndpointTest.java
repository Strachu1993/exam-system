package com.exam.feature.admin.student.details;

import com.exam.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

class StudentDetailsEndpointTest {

    private static final String URL = "/api/admin/students/44a4127e-a953-47fe-80ac-1353c0697afb";
    private static final StudentDetailsService studentDetailsService = mock(StudentDetailsService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new StudentDetailsEndpoint(studentDetailsService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallGetStudentDetails() throws Exception {
        // given

        // when
        final ResultActions actions = mockMvc.perform(get(URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.OK.value(),
                mvcResult.getResponse()
                        .getStatus());
    }
}