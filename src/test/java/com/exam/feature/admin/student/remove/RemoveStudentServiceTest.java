package com.exam.feature.admin.student.remove;

import com.exam.commons.enums.RoleName;
import com.exam.commons.enums.StudentScoreStatus;
import com.exam.commons.exception.custom.general.StudentException;
import com.exam.commons.exception.custom.not_found.AccountNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;

class RemoveStudentServiceTest {

    private static final UUID SCORE_ID = UUID.fromString("f67b2643-4705-4650-bb8e-0af0b6022a69");
    private static final UUID ACCOUNT_ID = UUID.fromString("0e35b39e-0370-48d1-b766-fb1e5fb662f9");
    private static final UUID STUDENT_DETAILS_ID = UUID.fromString("9a719eb3-d7b2-4b9a-af88-9e60690a85ee");

    private final AccountRepository accountRepository = mock(AccountRepository.class);
    private final RemoveStudentService removeStudentService = new RemoveStudentService(accountRepository);

    @ParameterizedTest
    @EnumSource(value = StudentScoreStatus.class, names = {"CREATED", "FINISH"})
    void shouldRemoveStudentAccount(final StudentScoreStatus studentScoreStatus) {
        // given
        final AccountEntity accountEntity = createAccountEntity(studentScoreStatus);
        given(accountRepository.findByIdAndRolesName(ACCOUNT_ID, RoleName.ROLE_STUDENT)).willReturn(Optional.of(accountEntity));

        // when
        removeStudentService.removeStudent(ACCOUNT_ID);

        // then
        then(accountRepository).should().delete(accountEntity);
    }

    @Test
    void shouldThrowAccountNotFoundExceptionDuringRemoveStudent() {
        // given
        given(accountRepository.findByIdAndRolesName(ACCOUNT_ID, RoleName.ROLE_STUDENT)).willReturn(Optional.empty());

        // when
        final Executable executed = () -> removeStudentService.removeStudent(ACCOUNT_ID);

        // then
        final AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, executed);
        assertEquals("Cannot found account with id '" + ACCOUNT_ID + "'. With role 'ROLE_STUDENT'.", exception.getMessage());
        assertEquals("account-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        then(accountRepository).should(never()).delete(any());
    }

    @Test
    void shouldThrowStudentHasStartedExamExceptionDuringRemoveStudentAccount() {
        // given
        final AccountEntity accountEntity = createAccountEntity(StudentScoreStatus.IN_PROGRESS);
        given(accountRepository.findByIdAndRolesName(ACCOUNT_ID, RoleName.ROLE_STUDENT)).willReturn(Optional.of(accountEntity));

        // when
        final Executable executed = () -> removeStudentService.removeStudent(ACCOUNT_ID);

        // then
        final StudentHasStartedExamException exception = assertThrows(StudentHasStartedExamException.class, executed);
        assertEquals("Student with id '" + ACCOUNT_ID + "' has at least one exam started.", exception.getMessage());
        assertEquals("student-has-least-one-exam-started", exception.getType());
        assertSame(HttpStatus.CONFLICT, exception.getHttpStatus());
        then(accountRepository).should(never()).delete(any());
    }

    @Test
    void shouldThrowStudentExceptionDuringRemoveStudentAccount() {
        // given
        final AccountEntity accountEntity = createAccountEntity(StudentScoreStatus.CREATED);
        given(accountRepository.findByIdAndRolesName(ACCOUNT_ID, RoleName.ROLE_STUDENT)).willReturn(Optional.of(accountEntity));
        doThrow(new NullPointerException()).when(accountRepository).delete(accountEntity);

        // when
        final Executable executed = () -> removeStudentService.removeStudent(ACCOUNT_ID);

        // then
        final StudentException exception = assertThrows(StudentException.class, executed);
        assertEquals("Something was wrong during delete student account with id '" + ACCOUNT_ID + "'.", exception.getMessage());
        assertEquals("student-exception", exception.getType());
        assertSame(HttpStatus.CONFLICT, exception.getHttpStatus());
        then(accountRepository).should().delete(any());
    }

    private static AccountEntity createAccountEntity(final StudentScoreStatus studentScoreStatus) {
        final RoleEntity studentRole = createStudentRole();
        final StudentDetailsEntity studentDetails = createStudentDetails(studentScoreStatus);
        return AccountEntity.builder()
                .id(ACCOUNT_ID)
                .roles(Set.of(studentRole))
                .studentDetails(studentDetails)
                .build();
    }

    private static StudentDetailsEntity createStudentDetails(final StudentScoreStatus studentScoreStatus) {
        final StudentScoreEntity studentScoreEntity = createStudentScoreEntity(studentScoreStatus);
        return StudentDetailsEntity.builder()
                .id(STUDENT_DETAILS_ID)
                .studentScores(List.of(studentScoreEntity))
                .build();
    }

    private static StudentScoreEntity createStudentScoreEntity(final StudentScoreStatus studentScoreStatus) {
        return StudentScoreEntity.builder()
                .id(SCORE_ID)
                .status(studentScoreStatus)
                .build();
    }

    private static RoleEntity createStudentRole() {
        final UUID roleId = UUID.fromString("56649964-3a53-4694-acc3-9353a53a697c");
        return RoleEntity.builder()
                .id(roleId)
                .name(RoleName.ROLE_STUDENT)
                .build();
    }
}