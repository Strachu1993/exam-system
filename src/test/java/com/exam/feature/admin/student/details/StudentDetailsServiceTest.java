package com.exam.feature.admin.student.details;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.enums.Rating;
import com.exam.commons.enums.RoleName;
import com.exam.commons.enums.StudentScoreStatus;
import com.exam.commons.enums.Subgroup;
import com.exam.commons.exception.custom.not_found.AccountNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

class StudentDetailsServiceTest {

    private static final UUID EXAM_ID = UUID.fromString("23d2395a-a70b-4461-a4b5-39cecb1d5aa8");
    private static final LocalDateTime START = LocalDateTime.now();
    private static final LocalDateTime END = LocalDateTime.now().plusDays(1L);
    private static final String DESCRIPTION = "descriptions";
    private static final ExamStatus EXAM_STATUS = ExamStatus.IN_PROGRESS;
    private static final UUID SCORE_ID = UUID.fromString("f67b2643-4705-4650-bb8e-0af0b6022a69");
    private static final Integer SCORE = 75;
    private static final Rating RATING = Rating.DB;
    private static final StudentScoreStatus SCORE_STATUS = StudentScoreStatus.IN_PROGRESS;
    private static final UUID ACCOUNT_ID = UUID.fromString("0e35b39e-0370-48d1-b766-fb1e5fb662f9");
    private static final boolean ENABLED = true;
    private static final UUID STUDENT_DETAILS_ID = UUID.fromString("9a719eb3-d7b2-4b9a-af88-9e60690a85ee");
    private static final String NAME = "Jan";
    private static final String SURNAME = "Skrzetuski";
    private static final String ALBUM_NUMBER = "754623";
    private static final UUID GROUP_ID = UUID.fromString("b0909d05-7838-47b2-ad02-ceaf985b8665");
    private static final String GROUP_NAME = "IDZ-213";
    private static final Subgroup GROUP_SUBGROUP = Subgroup.A;
    private static final String GROUP_YEAR = "2020";

    private final AccountReadRepository accountReadRepository = mock(AccountReadRepository.class);
    private final StudentDetailsService studentDetailsService = new StudentDetailsService(accountReadRepository);

    @Test
    void shouldGetStudentDetailsResponse() {
        // given
        final AccountEntity accountEntity = createAccountEntity();
        given(accountReadRepository.findByIdAndRolesName(ACCOUNT_ID, RoleName.ROLE_STUDENT)).willReturn(Optional.of(accountEntity));

        // when
        final StudentDetailsResponse result = studentDetailsService.getStudentDetails(ACCOUNT_ID);

        // then
        final StudentDetailsResponse.StudentGroupResponse resultGroup = result.getGroup();
        assertEquals(ACCOUNT_ID, result.getAccountId());
        assertEquals(ALBUM_NUMBER, result.getAlbumNumber());
        assertEquals(NAME, result.getName());
        assertEquals(STUDENT_DETAILS_ID, result.getStudentDetailsId());
        assertTrue(result.isEnabled());
        assertSame(1, result.getExams().size());
        assertEquals(GROUP_ID, resultGroup.getId());
        assertEquals(GROUP_NAME, resultGroup.getName());
        assertSame(GROUP_SUBGROUP, resultGroup.getSubgroup());
        assertEquals(GROUP_YEAR, resultGroup.getYear());
        then(accountReadRepository).should().findByIdAndRolesName(ACCOUNT_ID, RoleName.ROLE_STUDENT);
    }

    @Test
    void shouldThrowAccountNotFoundExceptionDuringGetStudentDetailsResponse() {
        // given
        given(accountReadRepository.findByIdAndRolesName(ACCOUNT_ID, RoleName.ROLE_STUDENT)).willReturn(Optional.empty());

        // when
        final Executable executed = () -> studentDetailsService.getStudentDetails(ACCOUNT_ID);

        // then
        final AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, executed);
        assertEquals("Cannot found account with id '" + ACCOUNT_ID + "'. With role 'ROLE_STUDENT'.", exception.getMessage());
        assertEquals("account-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        then(accountReadRepository).should().findByIdAndRolesName(ACCOUNT_ID, RoleName.ROLE_STUDENT);
    }

    private static AccountEntity createAccountEntity() {
        final Set<RoleEntity> studentRoles = Set.of(createStudentRole());
        final StudentDetailsEntity studentDetails = createStudentDetails();

        return AccountEntity.builder()
                .id(ACCOUNT_ID)
                .enabled(ENABLED)
                .roles(studentRoles)
                .studentDetails(studentDetails)
                .build();
    }

    private static StudentDetailsEntity createStudentDetails() {
        final StudentGroupEntity group = createStudentGroupEntity();
        final List<StudentScoreEntity> studentScores = List.of(createStudentScoreEntity());

        return StudentDetailsEntity.builder()
                .id(STUDENT_DETAILS_ID)
                .albumNumber(ALBUM_NUMBER)
                .name(NAME)
                .surname(SURNAME)
                .studentGroup(group)
                .studentScores(studentScores)
                .build();
    }

    private static StudentScoreEntity createStudentScoreEntity() {
        final ExamEntity exam = createExamEntity();
        return StudentScoreEntity.builder()
                .id(SCORE_ID)
                .rating(RATING)
                .score(SCORE)
                .status(SCORE_STATUS)
                .exam(exam)
                .build();
    }

    private static RoleEntity createStudentRole() {
        final UUID roleId = UUID.fromString("56649964-3a53-4694-acc3-9353a53a697c");
        return RoleEntity.builder()
                .id(roleId)
                .name(RoleName.ROLE_STUDENT)
                .build();
    }

    private static StudentGroupEntity createStudentGroupEntity() {
        return StudentGroupEntity.builder()
                .id(GROUP_ID)
                .name(GROUP_NAME)
                .subgroup(GROUP_SUBGROUP)
                .year(GROUP_YEAR)
                .build();
    }

    private static ExamEntity createExamEntity() {
        return ExamEntity.builder()
                .id(EXAM_ID)
                .start(START)
                .end(END)
                .description(DESCRIPTION)
                .status(EXAM_STATUS)
                .build();
    }
}