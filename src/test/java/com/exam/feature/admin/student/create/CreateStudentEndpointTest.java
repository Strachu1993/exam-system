package com.exam.feature.admin.student.create;

import com.exam.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

class CreateStudentEndpointTest {

    private static final String URL = "/api/admin/students";
    private static final CreateStudentService createStudentService = mock(CreateStudentService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new CreateStudentEndpoint(createStudentService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallCreateStudent() throws Exception {
        // given
        final String requestJson = "{\n" +
                "  \"name\":\"New name\",\n" +
                "  \"subgroup\":\"B\",\n" +
                "  \"groupId\":\"3374147e-fef5-457d-b980-c6323c62bc0a\",\n" +
                "  \"albumNumber\":\"123123\"\n" +
                "}";

        // when
        final ResultActions actions = mockMvc.perform(post(URL)
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.CREATED.value(),
                mvcResult.getResponse()
                        .getStatus());
    }
}