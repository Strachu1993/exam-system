package com.exam.feature.admin.student;

import com.exam.commons.enums.RoleName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StudentAccountServiceTest {

    private static final RoleName ROLE_STUDENT = RoleName.ROLE_STUDENT;

    private final StudentGroupRepository studentGroupRepository = mock(StudentGroupRepository.class);
    private final RoleReadRepository roleRepository = mock(RoleReadRepository.class);
    private final AccountRepository accountRepository = mock(AccountRepository.class);
    private final CharacterGenerator characterGenerator = mock(CharacterGenerator.class);

    private final StudentAccountService studentAccountService = new StudentAccountService(studentGroupRepository, roleRepository, accountRepository, characterGenerator);

    @Test
    void shouldFindAllStudents() {
        // Given
        final AccountEntity studentAccount1 = prepareAccountEntity();
        final AccountEntity studentAccount2 = prepareAccountEntity();
        final List<AccountEntity> studentAccounts = List.of(studentAccount1, studentAccount2);
        when(accountRepository.findByRolesName(ROLE_STUDENT)).thenReturn(studentAccounts);

        // When
        final List<StudentAccountResponse> students = studentAccountService.findAllStudents();

        // Then
        assertSame(2, students.size());
    }

    private AccountEntity prepareAccountEntity() {
        final Set<RoleEntity> studentRole = Set.of(createRoleEntity());

        return AccountEntity.builder()
                .id(UUID.randomUUID())
                .enabled(true)
                .login("dgsyw7")
                .password("psjds")
                .roles(studentRole)
                .studentDetails(prepareStudentDetailsEntity())
                .build();
    }

    private StudentDetailsEntity prepareStudentDetailsEntity() {
        return StudentDetailsEntity.builder()
                .id(UUID.randomUUID())
                .albumNumber("2387123")
                .name("Jimi")
                .surname("Kowadełko")
                .studentGroup(createStudentGroupEntity())
                .build();
    }

    private RoleEntity createRoleEntity() {
        return RoleEntity.builder()
                .id(UUID.randomUUID())
                .name(ROLE_STUDENT)
                .build();
    }

    private StudentGroupEntity createStudentGroupEntity() {
        return StudentGroupEntity.builder()
                .id(UUID.randomUUID())
                .name("104-IDZ")
                .year("2020")
                .build();
    }
}