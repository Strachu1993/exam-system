package com.exam.feature.admin.student.active_account;

import com.exam.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;

class ActiveStudentAccountEndpointTest {

    private static final String URL = "/api/admin/students/2b8b98c3-74e2-486e-aff3-0ffe874277b5/active";
    private static final ActiveStudentAccountService activeStudentAccountService = mock(ActiveStudentAccountService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new ActiveStudentAccountEndpoint(activeStudentAccountService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallActiveStudentAccount() throws Exception {
        // given

        // when
        final ResultActions actions = mockMvc.perform(patch(URL));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.NO_CONTENT.value(),
                mvcResult.getResponse()
                        .getStatus());
    }
}