package com.exam.feature.admin.student.create;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.custom.not_found.RoleNotFoundException;
import com.exam.commons.exception.custom.not_found.StudentGroupNotFoundException;
import com.exam.commons.feature.character_generator.CharacterGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;

class CreateStudentServiceTest {

    private static final String PASSWORD_ENCRYPTION = "$2a$10$dSMHGPkOtzqcKu1fzKq96OcQtr85sCCJ2LzkCJyN1B9NG74At.eIW";
    private static final String PASSWORD = "+-B:e.";
    private static final String PASSWORD_BASE64 = "+-B:e.";
    private static final String LOGIN = "n]pO";
    private static final UUID STUDENT_ROLE_ID = UUID.fromString("ce93f398-3c89-466c-9588-822bde92eb29");
    private static final UUID GROUP_ID = UUID.fromString("62eea210-210a-494a-b1ef-46ed41bf8afb");
    private static final RoleName STUDENT_ROLE = RoleName.ROLE_STUDENT;
    private static final String ALBUM_NUMBER = "2387123";
    private static final String NAME = "Jan";
    private static final String SURNAME = "Skrzetuski";
    private static final String GROUP_NAME = "104-IDZ";
    private static final String GROUP_YEAR = "2020";

    private final StudentGroupReadRepository studentGroupRepository = mock(StudentGroupReadRepository.class);
    private final RoleReadRepository roleRepository = mock(RoleReadRepository.class);
    private final AccountRepository accountRepository = mock(AccountRepository.class);
    private final PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);
    private final CharacterGenerator characterGenerator = mock(CharacterGenerator.class);

    private final CreateStudentService createStudentService = new CreateStudentService(studentGroupRepository, roleRepository, accountRepository, passwordEncoder, characterGenerator);

    @Test
    void shouldCreateStudentAccount() {
        // given
        given(accountRepository.existsByStudentDetailsAlbumNumber(ALBUM_NUMBER)).willReturn(false);

        final CreateStudentRequest createStudentRequest = prepareCreateStudentAccountRequest();
        given(characterGenerator.generateText()).willReturn(LOGIN).willReturn(PASSWORD);
        given(passwordEncoder.encode(PASSWORD)).willReturn(PASSWORD_ENCRYPTION);

        final RoleEntity studentRole = prepareRoleEntity();
        final Optional<RoleEntity> studentRoleOpt = Optional.of(studentRole);
        given(roleRepository.findByName(STUDENT_ROLE)).willReturn(studentRoleOpt);

        final StudentGroupEntity studentGroup = prepareStudentGroupEntity();
        final Optional<StudentGroupEntity> studentGroupOpt = Optional.of(studentGroup);
        given(studentGroupRepository.findById(GROUP_ID)).willReturn(studentGroupOpt);

        // when
        createStudentService.createStudent(createStudentRequest);

        // then
        final ArgumentCaptor<AccountEntity> capture = ArgumentCaptor.forClass(AccountEntity.class);
        then(accountRepository).should().save(capture.capture());
        final AccountEntity accountEntityCapture = capture.getValue();
        final StudentDetailsEntity studentDetailsCapture = accountEntityCapture.getStudentDetails();
        final StudentGroupEntity studentGroupCapture = studentDetailsCapture.getStudentGroup();

        assertAccount(accountEntityCapture);
        assertStudentDetails(studentDetailsCapture);
        assertStudentGroup(studentGroupCapture);
    }

    @Test
    void shouldThrowStudentGroupNotFoundExceptionDuringCreateStudentAccount() {
        // given
        given(accountRepository.existsByStudentDetailsAlbumNumber(ALBUM_NUMBER)).willReturn(false);

        final CreateStudentRequest createStudentRequest = prepareCreateStudentAccountRequest();
        given(characterGenerator.generateText()).willReturn(LOGIN).willReturn(PASSWORD);
        given(passwordEncoder.encode(PASSWORD)).willReturn(PASSWORD_ENCRYPTION);

        final RoleEntity studentRole = prepareRoleEntity();
        final Optional<RoleEntity> studentRoleOpt = Optional.of(studentRole);
        given(roleRepository.findByName(STUDENT_ROLE)).willReturn(studentRoleOpt);
        given(studentGroupRepository.findById(GROUP_ID)).willReturn(Optional.empty());

        // when
        final Executable executed = () -> createStudentService.createStudent(createStudentRequest);

        // then
        final StudentGroupNotFoundException exception = assertThrows(StudentGroupNotFoundException.class, executed);
        assertEquals("Cannot found student group with id '" + GROUP_ID + "'.", exception.getMessage());
        assertEquals("student-group-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        then(accountRepository).should(never()).save(any());
    }

    @Test
    void shouldThrowRoleNotFoundExceptionDuringCreateStudentAccount() {
        // given
        given(accountRepository.existsByStudentDetailsAlbumNumber(ALBUM_NUMBER)).willReturn(false);

        final CreateStudentRequest createStudentRequest = prepareCreateStudentAccountRequest();
        given(characterGenerator.generateText()).willReturn(LOGIN).willReturn(PASSWORD);
        given(passwordEncoder.encode(PASSWORD)).willReturn(PASSWORD_ENCRYPTION);
        given(roleRepository.findByName(STUDENT_ROLE)).willReturn(Optional.empty());

        // when
        final Executable executed = () -> createStudentService.createStudent(createStudentRequest);

        // then
        final RoleNotFoundException exception = assertThrows(RoleNotFoundException.class, executed);
        assertEquals("Cannot found role with name '" + STUDENT_ROLE + "'.", exception.getMessage());
        assertEquals("role-not-found", exception.getType());
        assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus());
        then(accountRepository).should(never()).save(any());
    }

    @Test
    void shouldThrowAlbumNumberAlreadyExistsExceptionDuringCreateStudentAccount() {
        // given
        given(accountRepository.existsByStudentDetailsAlbumNumber(ALBUM_NUMBER)).willReturn(true);
        final CreateStudentRequest createStudentRequest = prepareCreateStudentAccountRequest();

        // when
        final Executable executed = () -> createStudentService.createStudent(createStudentRequest);

        // then
        final AlbumNumberAlreadyExistsException exception = assertThrows(AlbumNumberAlreadyExistsException.class, executed);
        assertEquals("Album number '" + ALBUM_NUMBER + "' already exists.", exception.getMessage());
        assertEquals("album-number-already-exists", exception.getType());
        assertSame(HttpStatus.BAD_REQUEST, exception.getHttpStatus());
        then(accountRepository).should(never()).save(any());
    }

    private static void assertStudentGroup(final StudentGroupEntity studentGroupCapture) {
        assertEquals(GROUP_NAME, studentGroupCapture.getName());
        assertEquals(GROUP_YEAR, studentGroupCapture.getYear());
    }

    private static void assertStudentDetails(final StudentDetailsEntity studentDetailsCapture) {
        assertEquals(ALBUM_NUMBER, studentDetailsCapture.getAlbumNumber());
        assertEquals(NAME, studentDetailsCapture.getName());
        assertEquals(SURNAME, studentDetailsCapture.getSurname());
        assertEquals(PASSWORD_BASE64, studentDetailsCapture.getPassword());
    }

    private static void assertAccount(final AccountEntity accountEntityCapture) {
        assertTrue(accountEntityCapture.isEnabled());
        assertEquals(LOGIN, accountEntityCapture.getLogin());
        assertEquals(PASSWORD_ENCRYPTION, accountEntityCapture.getPassword());
        assertSame(STUDENT_ROLE, accountEntityCapture.getRoles().iterator().next().getName());
    }

    private static StudentGroupEntity prepareStudentGroupEntity() {
        return StudentGroupEntity.builder()
                .id(GROUP_ID)
                .name(GROUP_NAME)
                .year(GROUP_YEAR)
                .build();
    }

    private static RoleEntity prepareRoleEntity() {
        return RoleEntity.builder()
                .id(STUDENT_ROLE_ID)
                .name(STUDENT_ROLE)
                .build();
    }

    private static CreateStudentRequest prepareCreateStudentAccountRequest() {
        return CreateStudentRequest.builder()
                .albumNumber(ALBUM_NUMBER)
                .name(NAME)
                .surname(SURNAME)
                .groupId(GROUP_ID)
                .build();
    }
}
