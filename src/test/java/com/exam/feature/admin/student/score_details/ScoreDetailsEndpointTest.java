package com.exam.feature.admin.student.score_details;

import com.exam.PrefixPathHelper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

class ScoreDetailsEndpointTest {

    private static final String URL = "/api/admin/students/2b8b98c3-74e2-486e-aff3-0ffe874277b5/score";
    private static final ScoreDetailsService scoreDetailsService = mock(ScoreDetailsService.class);
    private static final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new ScoreDetailsEndpoint(scoreDetailsService))
            .addPlaceholderValue(PrefixPathHelper.ADMIN.getPlaceholder(), PrefixPathHelper.ADMIN.getValue())
            .build();

    @Test
    void shouldCallGetStudentScoreDetails() throws Exception {
        // given

        // when
        final ResultActions actions = mockMvc.perform(get(URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE));

        // then
        final MvcResult mvcResult = actions.andReturn();
        assertEquals(HttpStatus.OK.value(),
                mvcResult.getResponse()
                        .getStatus());
    }
}