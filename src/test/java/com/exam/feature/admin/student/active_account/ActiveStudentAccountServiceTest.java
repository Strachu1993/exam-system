package com.exam.feature.admin.student.active_account;

import com.exam.commons.enums.RoleName;
import com.exam.commons.exception.custom.not_found.AccountNotFoundException;
import com.exam.commons.exception.custom.validate.AccountAlreadyIsEnabledException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpStatus;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;

class ActiveStudentAccountServiceTest {

    private static final UUID STUDENT_ACCOUNT_ID = UUID.fromString("2b8b98c3-74e2-486e-aff3-0ffe874277b5");
    private static final RoleName STUDENT_ROLE_NAME = RoleName.ROLE_STUDENT;
    private final AccountRepository accountRepository = mock(AccountRepository.class);
    private ActiveStudentAccountService activeStudentAccountService;

    @BeforeEach
    void beforeEach() {
        activeStudentAccountService = new ActiveStudentAccountService(accountRepository);
    }

    @Test
    void shouldActiveStudentAccount() {
        // given
        final AccountEntity accountEntity = prepareAccountEntity(false);
        given(accountRepository.findByIdAndRolesName(STUDENT_ACCOUNT_ID, STUDENT_ROLE_NAME)).willReturn(Optional.of(accountEntity));

        // when
        activeStudentAccountService.activeStudentAccount(STUDENT_ACCOUNT_ID);

        // then
        final ArgumentCaptor<AccountEntity> captor = ArgumentCaptor.forClass(AccountEntity.class);
        then(accountRepository).should().save(captor.capture());
        final AccountEntity captorValue = captor.getValue();
        assertTrue(captorValue.isEnabled());
    }

    @Test
    void shouldThrowAccountNotFoundExceptionDuringActiveStudentAccount() {
        // given
        given(accountRepository.findByIdAndRolesName(STUDENT_ACCOUNT_ID, STUDENT_ROLE_NAME)).willReturn(Optional.empty());

        // when
        final Executable executed = () -> activeStudentAccountService.activeStudentAccount(STUDENT_ACCOUNT_ID);

        // then
        final AccountNotFoundException exception = assertThrows(AccountNotFoundException.class, executed);
        assertAll(
                () -> assertEquals("Cannot found account with id '" + STUDENT_ACCOUNT_ID + "'. With role '" + STUDENT_ROLE_NAME + "'.", exception.getMessage()),
                () -> assertEquals("account-not-found", exception.getType()),
                () -> assertSame(HttpStatus.NOT_FOUND, exception.getHttpStatus()),
                () -> then(accountRepository).should(never()).save(any(AccountEntity.class))
        );
    }

    @Test
    void shouldThrowAccountAlreadyIsEnabledExceptionDuringActiveStudentAccount() {
        // given
        final AccountEntity accountEntity = prepareAccountEntity(true);
        given(accountRepository.findByIdAndRolesName(STUDENT_ACCOUNT_ID, STUDENT_ROLE_NAME)).willReturn(Optional.of(accountEntity));

        // when
        final Executable executed = () -> activeStudentAccountService.activeStudentAccount(STUDENT_ACCOUNT_ID);

        // then
        final AccountAlreadyIsEnabledException exception = assertThrows(AccountAlreadyIsEnabledException.class, executed);
        assertAll(
                () -> assertEquals("Account with id '" + STUDENT_ACCOUNT_ID + "' already is enabled.", exception.getMessage()),
                () -> assertEquals("account-is-already-enabled", exception.getType()),
                () -> assertSame(HttpStatus.CONFLICT, exception.getHttpStatus()),
                () -> then(accountRepository).should(never()).save(any(AccountEntity.class))
        );
    }

    private static AccountEntity prepareAccountEntity(final boolean isEnabled) {
        return AccountEntity.builder()
                .id(STUDENT_ACCOUNT_ID)
                .enabled(isEnabled)
                .roles(Set.of(prepareRoleEntity()))
                .build();
    }

    private static RoleEntity prepareRoleEntity() {
        return RoleEntity.builder()
                .id(UUID.fromString("38b5ee0d-d7fe-4452-a9d7-7fae42692299"))
                .name(STUDENT_ROLE_NAME)
                .build();
    }
}