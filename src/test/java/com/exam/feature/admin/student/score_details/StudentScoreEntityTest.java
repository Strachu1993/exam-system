package com.exam.feature.admin.student.score_details;

import com.exam.commons.enums.ExamStatus;
import com.exam.commons.enums.Rating;
import com.exam.commons.enums.StudentScoreStatus;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StudentScoreEntityTest {

    private static final UUID EXAM_ID = UUID.fromString("23d2395a-a70b-4461-a4b5-39cecb1d5aa8");
    private static final LocalDateTime START = LocalDateTime.now();
    private static final LocalDateTime END = LocalDateTime.now().plusDays(1L);
    private static final String DESCRIPTION = "descriptions";
    private static final ExamStatus EXAM_STATUS = ExamStatus.IN_PROGRESS;
    private static final UUID SCORE_ID = UUID.fromString("f67b2643-4705-4650-bb8e-0af0b6022a69");
    private static final Integer CORRECT_QUESTIONS = 4;
    private static final Integer IN_CORRECT_QUESTIONS = 7;
    private static final Integer SCORE = 75;
    private static final Rating RATING = Rating.DB;
    private static final StudentScoreStatus SCORE_STATUS = StudentScoreStatus.IN_PROGRESS;

    @Test
    void shouldMapStudentScoreEntityToScoreDetailsResponse() {
        // given
        final StudentScoreEntity studentScore = createStudentScoreEntity();

        // when
        final ScoreDetailsResponse result = studentScore.toScoreDetailsResponse();

        // then
        assertAll(
                () -> assertEquals(EXAM_ID, result.getExamId()),
                () -> assertEquals(START, result.getStart()),
                () -> assertEquals(END, result.getEnd()),
                () -> assertEquals(DESCRIPTION, result.getDescription()),
                () -> assertEquals(EXAM_STATUS, result.getExamStatus()),
                () -> assertEquals(SCORE_ID, result.getScoreId()),
                () -> assertEquals(CORRECT_QUESTIONS, result.getCorrectQuestions()),
                () -> assertEquals(IN_CORRECT_QUESTIONS, result.getInCorrectQuestions()),
                () -> assertEquals(SCORE, result.getScore()),
                () -> assertEquals(RATING, result.getRating()),
                () -> assertEquals(SCORE_STATUS, result.getScoreStatus())
        );
    }

    private static StudentScoreEntity createStudentScoreEntity() {
        final ExamEntity exam = createExamEntity();
        return StudentScoreEntity.builder()
                .id(SCORE_ID)
                .correctQuestions(CORRECT_QUESTIONS)
                .inCorrectQuestions(IN_CORRECT_QUESTIONS)
                .rating(RATING)
                .score(SCORE)
                .status(SCORE_STATUS)
                .exam(exam)
                .build();
    }

    private static ExamEntity createExamEntity() {
        return ExamEntity.builder()
                    .id(EXAM_ID)
                    .start(START)
                    .end(END)
                    .description(DESCRIPTION)
                    .status(EXAM_STATUS)
                    .build();
    }
}