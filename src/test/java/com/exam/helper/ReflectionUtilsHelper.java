package com.exam.helper;

import lombok.experimental.UtilityClass;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

@UtilityClass
public class ReflectionUtilsHelper {

    public void setFinalStatic(final Object target, final String fieldName, final Object newValue) {
        try {
            final Field field = target.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            final Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
            field.set(null, newValue);
        } catch (final IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException ex) {
            throw ReflectionUtilsHelperException.ofStaticFinalField(fieldName, ex);
        }
    }
}
