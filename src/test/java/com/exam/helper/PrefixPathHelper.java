package com.exam.helper;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum PrefixPathHelper {

    ADMIN("api.url.path.admin", "/api/admin"),
    STUDENT("api.url.path.student", "/api/student"),
    NO_AUTH("api.url.path.anonymous", "/api/no-auth");

    private final String placeholder;
    private final String value;
}
