package com.exam.helper;

public class ReflectionUtilsHelperException extends RuntimeException {

    private ReflectionUtilsHelperException(final String message, final Exception ex) {
        super(message, ex);
    }

    public static ReflectionUtilsHelperException ofStaticFinalField(final String fieldName, final Exception ex) {
        return new ReflectionUtilsHelperException("Error during set static final field with name '" + fieldName + "'.", ex);
    }
}
