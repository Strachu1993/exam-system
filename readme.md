# Exam system

## Technologys
   - Spring Boot 2(v.2.2.0.M3)
   - Spring Data JPA(v.2.2.0.M3)
   - Spring Security(v.2.2.0.M3)
   - Spring web(v.2.2.0.M3)
   - Java 1.11
   - Gradle(v.5.4.1)
   - PostgreSQL(v.42.2.9)
   - H2(v.1.4.200)
   - Liquibase(v.3.6.3)
   - Lombok(v.1.18.6)
   - Junit 5
   - Mockito 2
   - Pit
   - Node.js(v.12.16.3 LTS)
   - Npm(v.6.14.4)
   - JWT(JSON Web Tokens) [Official jwt website](https://jwt.io/)
   - Html(v.5)
   - Css(v.3)
   - Java Script/Type Script(ecma 6 or greater)
   - Bootstrap(v.4.4.1)

## Install
   - Install ```Java 1.11``` [Official java website](https://www.java.com/pl/)
   - Set ```JAVA_HOME```
   - install ```node js``` [Official node js website](https://nodejs.org/en/)
   - install ```npm```
   - install ```Angular CLI``` command: ```npm install -g @angular/cli```
   - Install ```Docker``` [Official Docker website](https://www.docker.com/)
   - Docker command to create docker image with data base: ```docker run -p 5432:5432 --name Exam-system-db -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_DB=exam-db -d postgres```
   
## How run in IntelIJ IDEA
1. Go to ```/src/main/resources/application.properties``` and set property ```spring.profiles.active``` using values ```local``` or ```prod```
   - When set ```prod``` you should also set properties in ```application-prod.properties``` for postgreSQL
     - spring.datasource.url
     - spring.datasource.username
     - spring.datasource.password
     - Run Docker command to start postgreSQL database ```docker container start image_id```
     - Run Gradle command ```liquibase -> update```
   - When set ```local``` the database(H2) in memory will be started with fake data
   
2. Set Lombok settings
    - ctrl+alt+s -> Plugins -> search lombok -> install ```Lombok-Plugin```
    - ctrl+alt+s -> Build. Execution, Deployment -> Compiler -> Annotation Processors -> select ```Enable annotation processing```
    
3. Run back-end application
    - Go to ```{projectDirectory}```
    - run ```./gradlew clean bootRun```
4. Run front-end application
    - Go to ```{projectDirectory}/src/exam-angular```
    - Run command(only first time or when dependencies has been changed) ```npm i``` or ```sudo npm i```
    - To run
      - ```ng serve``` for use
      - ```sudo ng serve -o --live-reload``` for develop
    - Go to ```localhost:4200```
    
## How run PIT tests
1. Go to ```{projectDirectory}```
2. Run pit task from gradle tree ```pitest -> cleanPitest```
   or run command ```./gradlew clean pitest```
3. After success PIT reports should be available in ```{projectDirectory}/build/reports/pitest```

## Run Sonarqube Analysis
1. Install Sonarqube [Official Sonar website](https://www.sonarqube.org/) or using command ```Docker docker run -d --name sonarqube -p 9000:9000 sonarqube:7.5-community```
2. Run Sonar using command ```./sonar.sh start```
3. Run Sonar analysis task from gradle tree ```sonarqube -> sonarAnalysis```
   or go to ```{projectDirectory}``` and run command```./gradlew sonarAnalysis```
4. Go to localhost:9000
